    //{
    //    "ret"
    //:
    //    [
    //        {"name": "Folder", "type": "folder"},
    //        {"name": "Folder2", "type": "folder"},
    //        {"name": "FileX", "type": "file"},
    //        {"name": "FileY", "type": "file"}
    //    ]
    //}


    //var action = parse("action");
    if (action == undefined)
        document.write(JSON.stringify({error: "noAction"}));
    else if (action == "getFolderContent")
        handleGetFolderContent();
    else
        document.write(JSON.stringify({error: "notHandled"}));

    function handleGetFolderContent() {
        var tree = {content: [{type: "folder", name: "human", content: []}]};
        var c = tree.content, name = parse("folder");
        if (name == undefined || name=="/" || name=="")
            return returnTree(tree);
        name = name.split("/");
        for (var i = 0; i < name.length; i++)
            if (name[i] in c) {
                c = c[name[i]];
            } else {
                document.write(JSON.stringify({error: "notFound", "folder": name[i]}));
                return false;
            }
        returnTree(c);
    }

    function returnTree(tree) {
        var o = {"ret": []};
        console.log(tree);
        for (var i = 0; i < tree.content.length ; i++)
            if (tree.content[i].type == "folder")
                o.ret.push({name: tree.content[i].name, type: "folder"});
            else
                o.ret.push(tree.content[i]);
        document.write(JSON.stringify(o));
    }


    function parse(val) {
        var result = undefined,
                tmp = [];
        location.search
            //.replace ( "?", "" )
            // this is better, there might be a question mark inside
                .substr(1)
                .split("&")
                .forEach(function (item) {
                    tmp = item.split("=");
                    if (tmp[0] === val) result = decodeURIComponent(tmp[1]);
                });
        return result;
    }
