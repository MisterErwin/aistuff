/**
 * Created by L�pges on 15.06.2015.
 */

var cFrame = 1;

var frames = {};


function ls_store(){
    localStorage.setItem('ani', JSON.stringify(frames));
}

function ls_get(){
    frames = JSON.parse(localStorage.getItem('ani'));
}

$(function () {

    var fS = $("#frameScroll");
    fS.scroll(function (e) {
        console.log($(this).scrollLeft() / $(this).width());
        //console.log( $(this).scrollLeft() +"/"+ ($(this).children(":first").width() +"-"+ $(this).width()));
        var scrollPercent = 100 * $(this).scrollLeft() / ($(this).children(":first").width() - $(this).width());
        //console.log(scrollPercent);
        if ($(this).scrollLeft() / $(this).width() > .5) {
            $("#frameScroll").append($("<button/>", {
                class: 'btn-xs btn-default keybutton',
                title: 'Keyframe',
                text: ($("#frameScroll button").length + 1),
                'data-FID': ($("#frameScroll button").length + 1)
            }));

        }
        //console.log($(this).scrollLeft());
    });
    fS.append($("<button/>", {class: 'btn-xs btn-info keybutton', title: 'Keyframe', text: '1', 'data-fID': 1}));
    for (var i = 2; i < 50; i++) {
        fS.append($("<button/>", {class: 'btn-xs btn-default keybutton', title: 'Keyframe', text: i, 'data-fID': i}));

    }


    $("[data-frame]").click(function (e) {
        e.preventDefault();
        switch ($(this).attr("data-frame")) {
            case '-':
                selectFrame(cFrame - 1);
                break;
            case '+':
                selectFrame(cFrame + 1);
                break;
            case 'l':

                selectFrame(getLastKeyFrame());
                break;
            default:
                selectFrame(parseInt($(this).attr("data-frame")));
                break;
        }

    });

    $("#frameScroll").on("click", "[data-fID]", function () {
        selectFrame($(this).attr("data-fID"));
    });

    $("#optionsKF input").on("input", function () {
        handleInput();
    });
    $("#optionsKF :checkbox").on("change", function () {
        handleInput();
    });

    //Handle rotating with mouse
    $("#gl")
        .mousedown(function (event) {
            mouseDownX = event.pageX;
            mouseDownY = event.pageY;
        })
        .mousemove(function (event) {
            mouseMoveX = event.pageX;
            mouseMoveY = event.pageY;
        })
        .mouseup(function (event) {
            rotY += getMouseDeltaX();
            rotX += getMouseDeltaY();
            mouseDownX = null;
            mouseDownY = null;
        });

    setup();
    render();
    createKeyFrame();

    $("#playSpeed").change(function(){
        playSpeed = $(this).val();
    })
});


function selectFrame(frame) {
    if (frame == cFrame || frame < 1)return;
    var e = $("#frameScroll button").eq(cFrame - 1);
    if (cFrame in frames)
        e.addClass("btn-warning");
    e.removeClass("btn-info");
    cFrame = frame;
    e = $("#frameScroll button").eq(cFrame - 1);
    e.removeClass("btn-warning").addClass("btn-info");
    e.parent().scrollLeft(e.parent().scrollLeft() + e.offset().left - 200);
    setOptions();
    loadFrom();
}

function setOptions() {
    if (cFrame in frames) {
        $("#optionsKF").show();
        $("#optionsF").hide();
        $("#btn_delKeyFrame").prop("disabled", cFrame == 1);


    } else {
        $("#optionsF").show();
        $("#optionsKF").hide();
    }
}

function createKeyFrame() {
    console.log("Creating", cFrame);
    if (cFrame in frames)
        throw  new Error("Already a keyframe", cFrame);
    frames[cFrame] = {};
    copyInto();
    setOptions();
}

function delKeyFrame() {
    if (!(cFrame in frames))
        throw  new Error("Not a keyframe", cFrame);
    delete frames[cFrame];
    setOptions();
}

function resetKeyFrame() {
    if (!(cFrame in frames))
        throw  new Error("Not a keyframe", cFrame);
    $("#bodyX").val(0);
    $("#bodyY").val(0);
    $("#bodyZ").val(0);
    $("#headX").val(0);
    $("#headY").val(0);
    $("#headZ").val(0);
    $("#leftLegX").val(0);
    $("#leftLegY").val(0);
    $("#leftLegZ").val(0);
    $("#rightLegX").val(0);
    $("#rightLegY").val(0);
    $("#rightLegZ").val(0);
    $("#leftArmX").val(0);
    $("#leftArmY").val(0);
    $("#leftArmZ").val(0);
    $("#rightArmX").val(0);
    $("#rightArmY").val(0);
    $("#rightArmZ").val(0);

    $("#rotation").val(0);

    handleInput();
}

function copyInto() {
    if (!( cFrame in frames)) //Interpolated frame
        return;
    var o = frames[cFrame];
    o.bodyX = $("#bodyX").val();
    o.bodyY = $("#bodyY").val();
    o.bodyZ = $("#bodyZ").val();
    o.headX = $("#headX").val();
    o.headY = $("#headY").val();
    o.headZ = $("#headZ").val();
    o.leftLegX = $("#leftLegX").val();
    o.leftLegY = $("#leftLegY").val();
    o.leftLegZ = $("#leftLegZ").val();
    o.rightLegX = $("#rightLegX").val();
    o.rightLegY = $("#rightLegY").val();
    o.rightLegZ = $("#rightLegZ").val();
    o.leftArmX = $("#leftArmX").val();
    o.leftArmY = $("#leftArmY").val();
    o.leftArmZ = $("#leftArmZ").val();
    o.rightArmX = $("#rightArmX").val();
    o.rightArmY = $("#rightArmY").val();
    o.rightArmZ = $("#rightArmZ").val();

    o.rotation = $("#rotation").val();
}

function loadFrom() {
    if (!(cFrame in frames)) {
        return loadFromInterpol();
    }
    var o = frames[cFrame];

    $("#bodyX").val(o.bodyX);
    $("#bodyY").val(o.bodyY);
    $("#bodyZ").val(o.bodyZ);
    $("#headX").val(o.headX);
    $("#headY").val(o.headY);
    $("#headZ").val(o.headZ);
    $("#leftLegX").val(o.leftLegX);
    $("#leftLegY").val(o.leftLegY);
    $("#leftLegZ").val(o.leftLegZ);
    $("#rightLegX").val(o.rightLegX);
    $("#rightLegY").val(o.rightLegY);
    $("#rightLegZ").val(o.rightLegZ);
    $("#leftArmX").val(o.leftArmX);
    $("#leftArmY").val(o.leftArmY);
    $("#leftArmZ").val(o.leftArmZ);
    $("#rightArmX").val(o.rightArmX);
    $("#rightArmY").val(o.rightArmY);
    $("#rightArmZ").val(o.rightArmZ);

    $("#rotation").val(o.rotation);

    handleInput();
}


function loadFromInterpol() {
    var bI = cFrame;
    for (var x; bI > 0 && !(bI in frames); bI--) {
    }
    if (bI == 0)
        throw new Error("Can't go down!");
    console.log(bI);
    var lastKeyFrame = getLastKeyFrame();
    if (cFrame > lastKeyFrame)
        return;
    var nI = cFrame;

    for (var x; nI < lastKeyFrame && !(nI in frames); nI++) {
    }
    console.log(nI);
    var a = (cFrame-bI) / (nI - bI);
    console.log("Steps:" + a);
    var b = frames[bI];
    var n = frames[nI];
    setVal("bodyX",b,n,a);
    setVal("bodyY",b,n,a);
    setVal("bodyZ",b,n,a);
    setVal("headX",b,n,a);
    setVal("headY",b,n,a);
    setVal("headZ",b,n,a);
    setVal("leftLegX",b,n,a);
    setVal("leftLegY",b,n,a);
    setVal("leftLegZ",b,n,a);
    setVal("rightLegX",b,n,a);
    setVal("rightLegY",b,n,a);
    setVal("rightLegZ",b,n,a);
    setVal("leftArmX",b,n,a);
    setVal("leftArmY",b,n,a);
    setVal("leftArmZ",b,n,a);
    setVal("rightArmX",b,n,a);
    setVal("rightArmY",b,n,a);
    setVal("rightArmZ",b,n,a);
    setVal("rotation",b,n,a);

    //$("#bodyX").val(shortestAngle(n.bodyX, b.bodyX) * a);
    //$("#bodyY").val(shortestAngle(n.bodyY, b.bodyY) * a);
    //$("#bodyZ").val(shortestAngle(n.bodyZ, b.bodyZ) * a);
    //$("#headX").val(shortestAngle(n.headX, b.headX) * a);
    //$("#headY").val(shortestAngle(n.headY, b.headY) * a);
    //$("#headZ").val(shortestAngle(n.headZ, b.headZ) * a);
    //$("#leftLegX").val(shortestAngle(n.leftLegX, b.leftLegX) * a);
    //$("#leftLegY").val(shortestAngle(n.leftLegY, b.leftLegY) * a);
    //$("#leftLegZ").val(shortestAngle(n.leftLegZ, b.leftLegZ) * a);
    //$("#rightLegX").val(shortestAngle(n.rightLegX, b.rightLegX) * a);
    //$("#rightLegY").val(shortestAngle(n.rightLegY, b.rightLegY) * a);
    //$("#rightLegZ").val(shortestAngle(n.rightLegZ, b.rightLegZ) * a);
    //$("#leftArmX").val(shortestAngle(n.leftArmX, b.leftArmX) * a);
    //$("#leftArmY").val(shortestAngle(n.leftArmY, b.leftArmY) * a);
    //$("#leftArmZ").val(shortestAngle(n.leftArmZ, b.leftArmZ) * a);
    //$("#rightArmX").val(shortestAngle(n.rightArmX, b.rightArmX) * a);
    //$("#rightArmY").val(shortestAngle(n.rightArmY, b.rightArmY) * a);
    //$("#rightArmZ").val(shortestAngle(n.rightArmZ, b.rightArmZ) * a);
    //
    //$("#rotation").val(b.rotation+shortestAngle(n.rotation, b.rotation) * a);
    handleInput();
}

function setVal(part, b,n,a){
    //console.log("------",part,"---------");
    //console.log("B:" + b[part] + "// N:" + n[part] + "// a:" + a );
    //var s = shortestAngle(n[part], b[part]);
    //console.log("s:" + s, " ==" + (parseInt(b[part]) + s*a) + "=>>>" +(b[part] + s*a)%360 );
    $("#"+part).val((parseInt(b[part]) + shortestAngle(n[part], b[part])*a)%360);
}

function shortestAngle(end, start) {
    return ((((end - start) % 360) + 540) % 360) - 180;
}


function getLastKeyFrame() {
    var h = 1;
    $.each(frames, function (key, o) {
        if (parseInt(key) > h)
            h = parseInt(key);
    });
    return h;
}

jQuery.fn.random = function () {
    var randomIndex = Math.floor(Math.random() * this.length);
    return jQuery(this[randomIndex]);
};


/****
 *  RENDER
 */

//Stuff for rendering
var DEG2RAD = Math.PI / 180;
var width, height, renderer, scene, camera;
var clock = new THREE.Clock;
var rotY = 0, rotX = 0;
var matWood = new THREE.MeshLambertMaterial({color: 0x826841});
//var matWood = new THREE.MeshLambertMaterial({ color: 0x826841, transparent: true, opacity: 0.5 }); //For testing the mesh alignment
var matStone = new THREE.MeshLambertMaterial({color: 0xadadad});
var viewCenter = new THREE.Vector3(0, 0, 0);
// Meshes
// The ones marked with //* are not real meshes, but contain a child (or more) which gets rendered.
// This is done, so these can easily be rotated around an accurate pivot point.
//var mBasePlate;
var mBody; //*
var mHead; //*
var mLegLeft; //*
var mLegRight; //*
var mArmLeft; //*
var mArmRight; //*
var armorstand, armorstandWrapper; //Group all the other elements

var mRightHand, rightHandRenderer;

//DATA
var small = false;
//The rotation values are all in degrees.
var head = new THREE.Vector3(0, 0, 0);
var body = new THREE.Vector3(0, 0, 0);
var leftLeg = new THREE.Vector3(0, 0, 0);
var rightLeg = new THREE.Vector3(0, 0, 0);
var leftArm = new THREE.Vector3(0, 0, 0);
var rightArm = new THREE.Vector3(0, 0, 0);
var rotation = 0;

//Stuff for mouse movements
var mouseDownX;
var mouseDownY;
var mouseMoveX;
var mouseMoveY;
var mouseRotationMultiplier = 0.008;
//A point class will help us manage the mouse movements.
Point = {
    x: null,
    y: null
};

function setup() {
    width = $("#gl").width();
    height = $("#gl").height();

    renderer = new THREE.WebGLRenderer({antialias: true, alpha: true});
    renderer.setSize(width, height);
    $("#gl").append(renderer.domElement);


    scene = new THREE.Scene();
    armorstand = new THREE.Object3D();
    //Add an armorstandWrapper to the scene, so the armorstand can be rotated naturally.
    armorstandWrapper = new THREE.Object3D();
    armorstand.position.set(0, -0.5, 0);
    armorstandWrapper.add(armorstand);


    //BasePlate
    //mBasePlate = new THREE.Mesh(
    //    new THREE.BoxGeometry(12/16, 1/16, 12/16),
    //    matStone);
    //mBasePlate.position.y = - (1/32 - armorstand.position.y);
    //armorstandWrapper.add(mBasePlate);
    //Add a little dot, so the user knows which way is forward
    var mmBaseDot = new THREE.Mesh(
        new THREE.BoxGeometry(2 / 16, 1 / 16, 4 / 16),
        matStone);
    mmBaseDot.position.set(0, -(1 / 32 - armorstand.position.y), 10 / 16);
    armorstandWrapper.add(mmBaseDot);

    // To Generate the other body parts, we will use a mesh to display,
    // and add it as a child to the object that serves as a pivot.

    //Left Leg
    var mmLegLeft = new THREE.Mesh(
        new THREE.BoxGeometry(2 / 16, 11 / 16, 2 / 16),
        matWood);
    mmLegLeft.position.set(0, -5.5 / 16, 0);
    mLegLeft = new THREE.Object3D();
    mLegLeft.position.set(2 / 16, 11 / 16, 0); //Pivot Point
    mLegLeft.add(mmLegLeft);
    armorstand.add(mLegLeft);

    //Right Leg
    var mmLegRight = new THREE.Mesh(
        new THREE.BoxGeometry(2 / 16, 11 / 16, 2 / 16),
        matWood);
    mmLegRight.position.set(0, -5.5 / 16, 0);
    mLegRight = new THREE.Object3D();
    mLegRight.position.set(-2 / 16, 11 / 16, 0); //Pivot Point
    mLegRight.add(mmLegRight);
    armorstand.add(mLegRight);

    //Left Arm
    var mmArmLeft = new THREE.Mesh(
        new THREE.BoxGeometry(2 / 16, 12 / 16, 2 / 16),
        matWood);
    mmArmLeft.position.set(0, -4 / 16, 0);
    mArmLeft = new THREE.Object3D();
    mArmLeft.position.set(6 / 16, 21 / 16, 0); //Pivot Point
    mArmLeft.add(mmArmLeft);
    armorstand.add(mArmLeft);

    //Right Arm
    var mmArmRight = new THREE.Mesh(
        new THREE.BoxGeometry(2 / 16, 12 / 16, 2 / 16),
        matWood);
    mmArmRight.position.set(0, -4 / 16, 0);
    mArmRight = new THREE.Object3D();
    mArmRight.position.set(-6 / 16, 21 / 16, 0); //Pivot Point
    mArmRight.add(mmArmRight);

    mRightHand = new THREE.Object3D();
    mRightHand.rotation.set(90 * DEG2RAD, 0,0);

    mRightHand.position.set(-5.7/16,2/16,2/16);

    mmArmRight.add(mRightHand);

    armorstand.add(mArmRight);


    rightHandRenderer = new ItemRenderer(undefined, mRightHand, 1/20);

    //Body (consists of four parts)
    var mmHip = new THREE.Mesh(
        new THREE.BoxGeometry(8 / 16, 2 / 16, 2 / 16),
        matWood);
    mmHip.position.set(0, -11 / 16, 0);
    var mmBodyLeft = new THREE.Mesh(
        new THREE.BoxGeometry(2 / 16, 7 / 16, 2 / 16),
        matWood);
    mmBodyLeft.position.set(2 / 16, -6.5 / 16, 0);
    var mmBodyRight = new THREE.Mesh(
        new THREE.BoxGeometry(2 / 16, 7 / 16, 2 / 16),
        matWood);
    mmBodyRight.position.set(-2 / 16, -6.5 / 16, 0);
    var mmShoulders = new THREE.Mesh(
        new THREE.BoxGeometry(12 / 16, 3 / 16, 3 / 16),
        matWood);
    mmShoulders.position.set(0, -1.5 / 16, 0);
    mBody = new THREE.Object3D();
    mBody.position.set(0, 23 / 16, 0); //Pivot Point
    mBody.add(mmHip);
    mBody.add(mmBodyLeft);
    mBody.add(mmBodyRight);
    mBody.add(mmShoulders);
    armorstand.add(mBody);

    //Head (neck and skull)
    var mmNeck = new THREE.Mesh(
        new THREE.BoxGeometry(2 / 16, 7 / 16, 2 / 16),
        matWood);
    mmNeck.position.set(0, 3.5 / 16, 0);
    /*
     We do not want the head, as it obstructs a whole lot of the view. We can implement this,
     once we've got an editor option to toggle it on and off.
     var mmSkull = new THREE.Mesh(
     new THREE.BoxGeometry(10/16, 10/16, 10/16),
     matWood);
     mmSkull.position.set(0,5/16,0);*/
    mHead = new THREE.Object3D();
    mHead.position.set(0, 22 / 16, 0); //Pivot Point
    mHead.add(mmNeck);
    armorstand.add(mHead);


    scene.add(armorstandWrapper);

    camera = new THREE.PerspectiveCamera(45, width / height, 0.1, 1000);
    camera.position.y = 2;
    camera.position.z = 4;
    camera.lookAt(viewCenter);
    scene.add(camera);

    var pointLight = new THREE.PointLight(0xffffff);
    pointLight.position.set(0, 300, 200);

    scene.add(pointLight);
}

function updateUI() {


    // Rotate 3D Stuff
    // y and z rotation needs to be inverted
    mBody.rotation.set(body.x * DEG2RAD, -body.y * DEG2RAD, -body.z * DEG2RAD);
    mHead.rotation.set(head.x * DEG2RAD, -head.y * DEG2RAD, -head.z * DEG2RAD);
    mLegLeft.rotation.set(leftLeg.x * DEG2RAD, -leftLeg.y * DEG2RAD, -leftLeg.z * DEG2RAD);
    mLegRight.rotation.set(rightLeg.x * DEG2RAD, -rightLeg.y * DEG2RAD, -rightLeg.z * DEG2RAD);
    mArmLeft.rotation.set(leftArm.x * DEG2RAD, -leftArm.y * DEG2RAD, -leftArm.z * DEG2RAD);
    mArmRight.rotation.set(rightArm.x * DEG2RAD, -rightArm.y * DEG2RAD, -rightArm.z * DEG2RAD);
    armorstand.rotation.y = -rotation * DEG2RAD;

    // Scale model, depending on small variable
    if (small)
        armorstand.scale.set(0.6, 0.6, 0.6);
    else
        armorstand.scale.set(1, 1, 1);

    //Set Visibility
    //mArmRight.visible = mArmLeft.visible = showArms;
    //mBasePlate.visible = !noBasePlate;
}

function isZero(vector) {
    return vector.x == 0 && vector.y == 0 && vector.z == 0;
}

function getMouseDeltaX() {
    var mouseDeltaX = 0;
    if (mouseDownX != null && mouseMoveX != null) {
        mouseDeltaX = mouseMoveX - mouseDownX;
    }
    return mouseDeltaX * mouseRotationMultiplier;
}
function getMouseDeltaY() {
    var mouseDeltaY = 0;
    if (mouseDownY != null && mouseMoveY != null) {
        mouseDeltaY = mouseMoveY - mouseDownY;
    }
    return mouseDeltaY * mouseRotationMultiplier;
}

function render() {
    renderer.render(scene, camera);

    var deltaTime = clock.getDelta();

    armorstandWrapper.rotation.y = rotY + getMouseDeltaX();
    armorstandWrapper.rotation.x = rotX + getMouseDeltaY();

    requestAnimationFrame(render);
}

function handleInput() {
    small = $("#small").prop("checked");
    body.set($("#bodyX").val(), $("#bodyY").val(), $("#bodyZ").val());
    head.set($("#headX").val(), $("#headY").val(), $("#headZ").val());
    leftLeg.set($("#leftLegX").val(), $("#leftLegY").val(), $("#leftLegZ").val());
    rightLeg.set($("#rightLegX").val(), $("#rightLegY").val(), $("#rightLegZ").val());
    leftArm.set($("#leftArmX").val(), $("#leftArmY").val(), $("#leftArmZ").val());
    rightArm.set($("#rightArmX").val(), $("#rightArmY").val(), $("#rightArmZ").val());

    rotation = $("#rotation").val();
    copyInto();
    updateUI();
}


///// Playing
var isPlaying = false;
var playSpeed = 500;
function playNext(){
    if (!isPlaying)
    return;
    if (cFrame +1> getLastKeyFrame())
        selectFrame(1)
    else
        selectFrame(cFrame+1);
    window.setTimeout(playNext, playSpeed);
}

function startPlaying(){
    $("#btn_f_play").addClass("btn-success").prop("disabled",true);
    $("#btn_f_pause").removeClass("btn-primary").prop("disabled",false);
    isPlaying = true;
    playNext();
}

function stopPlaying(){
    $("#btn_f_play").removeClass("btn-success").prop("disabled",false);
    $("#btn_f_pause").addClass("btn-primary").prop("disabled",true);
    isPlaying = false;
}


function loadFromModal(){
    frames = JSON.parse($("#ta_loadC").val());
    $("#loadModal").modal("hide");
}

function showModal(t){
    if (t !== undefined){
        $("#ta_loadC").val(JSON.stringify(frames)).prop("disabled",true);
        $("#btn_loadmodal").hide();

    }else {
        $("#ta_loadC").val("").prop("disabled", false);
        $("#btn_loadmodal").show();
    }

    $("#loadModal").modal("show");
}

function resetHand(hand) {
    if (hand == "right") {
        rightHandRenderer.reset();
    }
}
function setHandFromFile(hand, file){
    if (hand == "right"){
        rightHandRenderer.loadFromFile(file, function(){
            console.log("okay");
        },
        function(e){
            console.log(e);
        });
    }
}