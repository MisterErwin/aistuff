$(document).ready(function () {

    makeModal();

    $("#fileModal .subfolder").hide();

    $("#fileModal:not([loading])").on("click", "[data-name][data-folder]", function () {
        $("#fileModal .openbtn").prop("disabled", true);
    });
    $("#fileModal:not([loading])").on("click", "[data-name]:not([data-folder])", function () {
        $("#fileModal .openbtn").prop("disabled", false);
        $("#fileModal output").val($(this).attr("data-name"));
    });

    $("#fileModal:not([loading])").on("click", "[data-name][data-folder='open']", function () {
        $(this).find("span").removeClass("glyphicon-triangle-bottom").addClass("glyphicon-triangle-right");
        $(this).next(".subfolder").hide();
        $(this).attr("data-folder", "closed");
    });
    $("#fileModal:not([loading])").on("click", "[data-name][data-folder]:not([data-folder='open'],[data-folder='loading'])", function () {
        if (!$(this).next().hasClass("subfolder")) {
            $(this).find("span").addClass("glyphicon-refresh spin").removeClass("glyphicon-triangle-right");
            $(this).attr("data-folder", "loading");
            $("#fileModal").attr("loading", "true");
            var x = $(this);
            var f = $("<div/>", {class: "subfolder btn-group-vertical btn-block text-left"}).insertAfter(x);
            loadFolderContent(x.attr("data-name"), f, x);

        } else {
            $(this).attr("data-folder", "open");
            $(this).next(".subfolder").show();
            $(this).find("span").addClass("glyphicon-triangle-bottom").removeClass("glyphicon-triangle-right");
        }
    });

    $("#fileModal:not([loading])[write-access]").on("contextmenu", "[data-name]", function (event) {
        event.preventDefault();
        event.stopPropagation();

        hideContext();
        $("<div class='custom-menu'><div class='btn-group-vertical'>" +
        "<button class='btn btn-default'>Rename</button>" +
        "<button class='btn btn-default'>Remove</button>" +
        "<button class='btn btn-default'>Create folder</button>" +

        "</div></div>")
            .appendTo("body")
            .css({top: event.pageY + "px", left: event.pageX + "px"});
    });
    $(document).bind("click", function (event) {
        hideContext();
    });

    loadFolderContent("/", $("#fileModal .modal-body > .btn-group-vertical"));

});

function loadFolderContent(folder, f, span) {
    //$.get("fileserver.js", {
    //    action: "getFolderContent",
    //    folder: folder
    //}, function (data, textStatus) {
    var data = handleGetFolderContent(folder), textStatus = "success";
    if (textStatus == "success" && !("error" in data)) {
        $("#fileModal").removeAttr("loading");
        $.each(data["ret"], function (k, v) {
            if (v.type == "folder") {
                f.append($("<button/>", {
                    type: "button",
                    class: "btn btn-default",
                    "data-name": v.fname,
                    "data-folder": true

                }).append($("<span/>", {
                    class: "glyphicon glyphicon-folder-open",
                    "aria-hidden": "true"
                })).append(" ").append(v.name))
            } else
                f.append($("<button/>", {
                    type: "button",
                    class: "btn btn-default",
                    "data-name": v.fname
                }).append($("<span/>", {
                    class: "glyphicon glyphicon-file",
                    "aria-hidden": "true"
                })).append(" ").append(v.name))
        });
        if (typeof span !== "undefined")
            span.attr("data-folder", "open").find("span").addClass("glyphicon-triangle-bottom").removeClass("glyphicon-refresh spin");

    } else {
        $("#fileModal").removeAttr("loading");
        console.log(textStatus, data);
        if (typeof span !== "undefined")
            span.find("span").addClass("glyphicon-flash").removeClass("glyphicon-refresh spin");
    }
    //}, "script").fail(function(e){
    //    console.log(e);
    //});

}

function hideContext() {
    $("div.custom-menu").remove();
}

function makeModal() {
    $("body").append('<div class="modal fade" id="fileModal">    <div class="modal-dialog">        <div class="modal-content">        <div class="modal-header">        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span    aria-hidden="true">&times;</span></button>    <h4 class="modal-title">File Browser</h4>    </div>    <div class="modal-body">        <div class="btn-group-vertical btn-block text-left" role="group" aria-label="...">    </div>        </div>        <div class="modal-footer">        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>        <button type="button" class="btn btn-primary openbtn" onclick="modalSuccess(fileModalRet.value)">Open        </button>        </div>        </div>            <!-- /.modal-content -->        </div>            <!-- /.modal-dialog -->        <output class="hidden" id="fileModalRet"></output>        </div>            <!-- /.modal -->        ');
}


function handleGetFolderContent(name) {
    var tree = {
        content: {
            human: {
                type: "folder", content: {
                    human1: {type: "file"}
                }
            },
            animals: {type: "folder", content: {}}
        }
    };
    var c = tree.content;
    if (name == undefined || name == "/" || name == "")
        return returnTree(tree, "/");
    name = name.substring(1).split("/");
    for (var i = 0; i < name.length; i++)
        if (name[i] in c) {
            c = c[name[i]];
        } else {
            return {error: "notFound", "folder": name[i]};
        }
    return returnTree(c, name + "/");
}

function returnTree(tree, path) {
    var o = {"ret": []};
    for (var name in tree.content)
        if (tree.content[name].type == "folder")
            o.ret.push({name: name, type: "folder", fname: path + name});
        else
            o.ret.push({name: name, type: "file", fname: path + name});

    return o;
}


function getFileContent(name) {
    if (name == "human/human1")
        return JSON.stringify({
            parts: [
                {
                    name: "root-torso",
                    material: {name: "WOOL", data: 2},
                    children: [
                        {
                            name: "head",
                            material: {name: "SKULL_ITEM", data: 2, skullowner: "Notch"},
                            trans: [0, .45, 0]
                        },
                        {name: "l-arm", material: {name: "WOOL", data: 1}, trans: [-.34, .5, 0], rot: [Math.PI,0,0]},
                        {name: "r-arm", material: {name: "WOOL", data: 1}, trans: [.35, .5, 0], rot: [Math.PI,0,0]},
                        {name: "l-leg", material: {name: "WOOL", data: 0}, trans: [-.1,-.2,0], rot: [Math.PI,0,0]},
                        {name: "r-leg", material: {name: "WOOL", data: 0}, trans: [.1,-.2,0], rot: [Math.PI,0,0]}
                    ]
                }
            ]
        });
    return "##";
}


//    <button type="button" class="btn btn-default" data-name="" data-folder=""><span class="glyphicon glyphicon-triangle-right" aria-hidden="true"></span> Folder1        </button>        <button type="button" class="btn btn-default" data-name="" data-folder=""><span class="glyphicon glyphicon-triangle-right" aria-hidden="true"></span> Folder1        </button>        