/**
 * Created by L�pges on 15.06.2015.
 */

var jsonITEM;
var textureMap;
var elements = [];

var transparentFace = new THREE.MeshBasicMaterial({color: 0xffffff, transparent: true, opacity: 0.0, overdraw: true});
var m = new THREE.MeshBasicMaterial({color: 0xff0000});
var a = new THREE.MeshBasicMaterial({color: 0x00ff00});

var TToLoad = 0;

function loadFromModal() {
    jsonITEM = JSON.parse($("#ta_loadC").val());
    load();
}

function loadFromFile(file){
     $.getJSON(file, function(data){
         jsonITEM=data;
         load();

     }).fail(function (e){
         alert(e);
         console.log(e);
     });

}

function load(){

    TToLoad = 0;
    if (typeof(scene) === "undefined")
        setup();
    else {
        reset();
    }
    textureMap = {};
    var ls = $("#loadStatus").empty();
    ls = $("<ul/>", {class: "list-group"}).appendTo(ls);
    var ms = $("#itemData table tbody").empty();
    if ("__comment" in jsonITEM)
        ms.append($("<tr/>").append($("<td/>", {text: "Comment:"})).append($("<td/>", {text: jsonITEM["__comment"]})));

    if ("textures" in jsonITEM) {
        $("<li>", {class: "list-group-item list-group-item-danger", text: "Loading textures(WIP)"}).appendTo(ls);

        //var tl = $("<ul/>", {class: "list-group"});
        //$("<li>", {class: "list-group-item", text: "Loading textures"}).append(tl).appendTo(ls);
        //$.each(jsonITEM["textures"], function (key, val) {
        //    TToLoad++;
        //    var temp = $("<li>", {
        //        class: "list-group-item list-group-item-warning",
        //        text: key + "(" + val + ")"
        //    }).appendTo(tl);
        //    textureMap[key] = new THREE.MeshPhongMaterial({
        //        map: THREE.ImageUtils.loadTexture('rp/textures/' + val + '.png', undefined, function () {
        //            temp.removeClass("list-group-item-warning").addClass("list-group-item-success");
        //            if (--TToLoad == 0)
        //                continueLoad(ls);
        //        }, function (error) {
        //            console.log(error);
        //            temp.removeClass("list-group-item-warning").addClass("list-group-item-danger");
        //            if (--TToLoad == 0)
        //                continueLoad(ls);
        //
        //        })
        //    });
        //});
        //
        //if (TToLoad == 0)
        //    continueLoad(ls);
    }
    //TODO: Texture
    continueLoad(ls);
}

function s(o) {
    var r = undefined;
    for (var i = 0; i < o.length; i++)
        r = (r != undefined) ? r + "#" + o[i] : o[i];
    return "(" + r + ")";
}

function s1(a, b) {
    var r = "";
    if (a.length != b.length)
        return "length!";
    for (var i = 0; i < a.length; i++) {
        //if (r == undefined)
        //    r = "";
        //else
        //    r = r.concat("#");
        //var n = parseFloat(a[i]) - parseFloat(b[i]);
        //if (n < 0)
        //    console.log("HERE!");
        r += "#".concat(parseFloat(a[i]) - parseFloat(b[i]));
    }

    return "(".concat(r.substring(1)).concat(")");

}

function continueLoad(ls) {
    var tl = $("<ul/>", {class: "list-group"});
    $("<li>", {class: "list-group-item", text: "Loading elements"}).append(tl).appendTo(ls);
    $.each(jsonITEM["elements"], function (key, val) {
        var temp = $("<li>", {
            class: "list-group-item list-group-item-warning",
            text: "#" + key + "  " + s(val["from"]) + "     " + s(val["to"]) + " ####  " + s1(val["to"], val["from"]),
            title: s1(val["to"], val["from"]),
            'data-eid': "#" + key
        }).appendTo(tl);//.delay(2000).fadeOut();

        var materials = [];

        var w = val["to"][0] - val["from"][0], h = val["to"][1] - val["from"][1], d = val["to"][2] - val["from"][2];

        var geom = new THREE.BoxGeometry(w, h, d);

        //geom.faceVertexUvs[0] = [];

        //loadFace(materials, val.faces, "east", geom, 0);
        //loadFace(materials, val.faces, "down", geom, 2)
        //loadFace(materials, val.faces, "up", geom, 4);
        //loadFace(materials, val.faces, "down", geom, 6)
        //loadFace(materials, val.faces, "north", geom, 8)
        //loadFace(materials, val.faces, "south", geom, 10)
        var box = new THREE.Mesh(geom,  m);//materials[0]);//new THREE.MeshFaceMaterial(materials));
        box.name = key;
        box.position.set(val["from"][0] + 0.5 * w, val["from"][1] + 0.5 * h, val["from"][2] + 0.5 * d);
        scene.add(box);
        elements.push(box);

        temp.removeClass("list-group-item-warning").addClass("list-group-item-success");

        //return false;

    });
    scene.add(wrapper);
    window.setTimeout(render, 500);

}

var ttt;

function loadFace(list, l, face, geom, i) {
    geom.faces[i].materialIndex = i;
    if (face in l) {
        var o = l[face];
        var uv = [];
        //console.log("face:" + face, o);
        var texture = o.texture.substring(1);
        if (!(texture in textureMap)) {
            console.log(texture + "is not a valid texture");
            list.push(transparentFace);
        } else {
            if ("uv" in o) {
                var t = textureMap[texture];
                var h = t.map.image.height, w = t.map.image.width;
                var x1 = o.uv[0] / 16, x2 = o.uv[2] / 16, y1 = o.uv[3] / 16, y2 = o.uv[1] / 16;
                console.log(x1, x2, y1, y2, i);
                //x1=y1=0; x2=y2=1;
                x1=y1=0;x2=y2=1;
                var uv00 = new THREE.Vector2(x1, y1),
                    uv10= new THREE.Vector2(x2, y1),
                    uv11= new THREE.Vector2(x2, y2),
                    uv01= new THREE.Vector2(x1, y2);
                list.push(textureMap[texture]);
                geom.faceVertexUvs[0][i] =  [uv00,uv10,uv01];//[uv11,uv01,uv10];//[uv00,uv10,uv01];//[uv[0], uv[1], uv[3]];
                geom.faceVertexUvs[0][i + 1] = [uv10,uv11,uv01];//[uv01,uv00,uv10];//[uv10,uv11,uv01];//[uv[1], uv[2], uv[3]];

            } else {
                console.log("No UV");
                list.push(textureMap[texture]);

            }
        }
    } else {
        list.push(transparentFace);
    }
}

function animate() {

    requestAnimationFrame(animate);
    controls.update();

}

function render() {

    renderer.render(scene, camera);
}
function onWindowResize() {

    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();

    renderer.setSize(window.innerWidth, window.innerHeight);

    controls.handleResize();

    render();

}

//function render() {
//    //if (elements.length > 0) {
//    //
//    //    renderer.render(scene, camera);
//    //
//    //    var deltaTime = clock.getDelta();
//    //    elements[0].rotation.y = rotY + getMouseDeltaX();
//    //    elements[0].rotation.x = rotX + getMouseDeltaY();
//    //}
//    if (renderer !== undefined)
//        renderer.render(scene, camera);
//    requestAnimationFrame(render);
//}


function reset() {
    $.each(elements, function (k, v) {
        scene.remove(v)
    });
    scene.remove(wrapper);
}

var raycaster = new THREE.Raycaster();
var mouse = new THREE.Vector2();
//window.addEventListener( 'mousemove', onMouseMove, false );
function onMouseMove(event) {

    // calculate mouse position in normalized device coordinates
    // (-1 to +1) for both components

    mouse.x = ( event.clientX / window.innerWidth ) * 2 - 1;
    mouse.y = -( event.clientY / window.innerHeight ) * 2 + 1;

}


function setup() {
    width = $("#gl").width();
    height = $("#gl").height();

    renderer = new THREE.WebGLRenderer({antialias: true, alpha: true});
    renderer.setSize(width, height);
    $("#gl").append(renderer.domElement);
    scene = new THREE.Scene();

    camera = new THREE.PerspectiveCamera(45, width / height, 0.1, 1000);
    camera.position.y = 6;
    camera.position.z = 100;
    camera.position.x = 0;
    //camera.position.set( 110, 110, 250);

    camera.lookAt(new THREE.Vector3(0, 0, 0));
    scene.add(camera);

    //var pointLight = new THREE.PointLight(0xffffff);
    //pointLight.position.set(0, 300, 200);
    //
    //scene.add(pointLight);
    scene.add(new THREE.AmbientLight(0xffffff));

    wrapper = new THREE.Object3D();


    //Handle rotating with mouse
    $("#gl")
        .mousedown(function (event) {
            if (event.which == 1) {
                mouse.x = ((event.pageX - $(this).offset().left) / $(this).innerWidth()) * 2 - 1;
                mouse.y = -((event.pageY - $(this).offset().top) / $(this).innerHeight()) * 2 + 1;


                raycaster.setFromCamera(mouse, camera);

                // calculate objects intersecting the picking ray
                var intersects = raycaster.intersectObjects(elements);
                for (var i = 0; i < intersects.length; i++) {
                    console.log(i + "==>", intersects[i].object.name);
                    //const n = i;

                    //intersects[i].object.material = a;
                    $("[data-eid='#" + intersects[i].object.name + "']").
                        removeClass("list-group-item-success").addClass("list-group-item-info").delay(2000).queue(function () {
                            $(this).addClass("list-group-item-success").removeClass("list-group-item-info");
                            $(this).dequeue();
                        });
                    //window.setTimeout(function (i) {
                    //intersects[i].object.material = m;
                    //render();
                    //}, 2000,i);
                }
                render();

            }
        });


    controls = new THREE.TrackballControls(camera, $("#gl").get(0));

    controls.rotateSpeed = 1.0;
    controls.zoomSpeed = 1.2;
    controls.panSpeed = 0.8;

    controls.noZoom = false;
    controls.noPan = false;

    controls.staticMoving = true;
    controls.dynamicDampingFactor = 0.3;

    controls.keys = [65, 83, 68];

    controls.addEventListener('change', render);


    // grid

    var size = 8, step = 1;

    var geometry = new THREE.Geometry();

    for (var i = -size; i <= size; i += step) {

        geometry.vertices.push(new THREE.Vector3(-size, 0, i));
        geometry.vertices.push(new THREE.Vector3(size, 0, i));

        geometry.vertices.push(new THREE.Vector3(i, 0, -size));
        geometry.vertices.push(new THREE.Vector3(i, 0, size));
    }

    var material = new THREE.LineBasicMaterial({color: 0x000000, opacity: 0.2, transparent: true});

    var line = new THREE.Line(geometry, material, THREE.LinePieces);
    line.position.add(new THREE.Vector3(8, 0, 8));
    scene.add(line);

    animate();
    render();
}

var width, height, renderer, scene, camera, controls, wrapper;

