/**
 * Created by L�pges on 17.06.2015.
 */
var ItemRenderer = function(ls, object, size) {
    var _this = this;

    var ls = ls;
    this.object = object;
    this.jsonITEM;
    this.elements=[];
    this.size =  ( size !== undefined ) ? size : 0.02;

    var m = new THREE.MeshBasicMaterial({color: 0xff0000});
    var a = new THREE.MeshBasicMaterial({color: 0x00ff00});

    var texturesToLoad=0;

    this.loadFromText = function (text) {
        _this.load(JSON.parse(text));
    }

    this.loadFromFile = function (file, success, fail) {
        $.getJSON(file, function(data){
            _this.load(data);
            if (success)success();
        }).fail(function (e){
            if (fail) fail(e);
        });
    }


    this.load = function (json) {
        if (_this.elements.length > 0)
            _this.reset();
        _this.jsonITEM = json;
        if (ls)
            ls.empty();
        _this.loadTextures();
    }

    this.reset = function(){
        $.each(_this.elements, function (k, v) {
            object.remove(v)
        });
        _this.elements=[];

    }


    this.loadTextures = function() {
        texturesToLoad=0;
        if ("textures" in _this.jsonITEM) {
            if (ls)
                $("<li>", {
                    class: "list-group-item list-group-item-danger",
                    text: "Loading textures(WIP)"
                }).appendTo(ls);

        }
        _this.loadElements();
    }

    this.loadElements = function(){
        var rot=[0,0,0],tr=[0,0,0],sc=[1,1,1];
        if ("display" in _this.jsonITEM && "thirdperson" in _this.jsonITEM.display){
            var n = _this.jsonITEM.display.thirdperson;
            if ("rotation" in n) {
                rot = n.rotation;
                _this.object.rotation.set(rot[0] * DEG2RAD, -rot[1] * DEG2RAD,rot[2] * DEG2RAD);
            }
            if ("translation" in n)
                tr= n.translation;
            if ("scale" in n)
                sc= n.scale;
            console.log(rot);
        }
        if (ls) {
            var tl = $("<ul/>", {class: "list-group"});
            $("<li>", {class: "list-group-item", text: "Loading elements"}).append(tl).appendTo(ls);
        }
        $.each(_this.jsonITEM["elements"], function (key, val) {
            if (ls)
            var temp = $("<li>", {
                class: "list-group-item list-group-item-warning",
                text: "#" + key + "  " + s(val["from"]) + "     " + s(val["to"]) + " ####  " + s1(val["to"], val["from"]),
                title: s1(val["to"], val["from"]),
                'data-eid': "#" + key
            }).appendTo(tl);//.delay(2000).fadeOut();


            var w = val["to"][0] - val["from"][0], h = val["to"][1] - val["from"][1], d = val["to"][2] - val["from"][2];

            var geom = new THREE.BoxGeometry(w*_this.size*sc[0], h*_this.size*sc[1], d*_this.size*sc[2]);

            var box = new THREE.Mesh(geom,  m);
            box.name = key;
            box.position.set(((val["from"][0] -8+ 0.5 * w)*sc[0] - tr[0])*_this.size ,
                ((val["from"][1]-8 + 0.5 * h)*sc[1] - tr[1])*_this.size ,
                ((val["from"][2]-8 + 0.5 * d)*sc[2] - tr[2])*_this.size );
            _this.object.add(box);
            _this.elements.push(box);
            if (ls)
                temp.removeClass("list-group-item-warning").addClass("list-group-item-success");

        });

    }

}

