var rigidBodyData = {
    root: {
        name: "root-torso",
        material: {name: "WOOL", data: 2},
        children: [
            {
                name: "head",
                material: {name: "WOOL", data: 2, skullowner: "Notch"},
                trans: [0, .45, 0]
            },
            {name: "l-arm", material: {name: "WOOL", data: 1}, trans: [-.34, .5, 0], rot: [Math.PI, 0, 0]},
            {name: "r-arm", material: {name: "WOOL", data: 1}, trans: [.35, .5, 0], rot: [Math.PI, 0, 0]},
            {name: "l-leg", material: {name: "WOOL", data: 0}, trans: [-.1, -.2, 0], rot: [Math.PI, 0, 0]},
            {name: "r-leg", material: {name: "WOOL", data: 0}, trans: [.1, -.2, 0], rot: [Math.PI, 0, 0]}
        ]
    }
}, ridigBodyObjects = {};
var DEG2RAD = Math.PI / 180;
var width, height, renderer, scene, camera;
var clock = new THREE.Clock;
var rotY = 0, rotX = 0;
var viewCenter = new THREE.Vector3(0, 0, 0);
var rWrapper;


$(function () {
    //Handle rotating with mouse
    $("#gl")
        .mousedown(function (event) {
            mouseDownX = event.pageX;
            mouseDownY = event.pageY;
        })
        .mousemove(function (event) {
            mouseMoveX = event.pageX;
            mouseMoveY = event.pageY;
        })
        .mouseup(function (event) {
            rotY += getMouseDeltaX();
            rotX += getMouseDeltaY();
            mouseDownX = null;
            mouseDownY = null;
        });

    //setup();
    //render();

});

var geoMat = {
    "WOOL:0": {
        "__comment": "Model generated using MrCrayfish's Model Creator (http://mrcrayfish.com/modelcreator/)",
        "textures": {
            "0": "green"
        },
        "elements": [
            {
                "name": "Cube",
                "from": [6.0, 4.0, 6.0],
                "to": [10.0, 16.0, 10.0],
                "faces": {
                    "north": {"texture": "#0", "uv": [0.0, 0.0, 4.0, 4.0]},
                    "east": {"texture": "#0", "uv": [0.0, 0.0, 12.0, 4.0]},
                    "south": {"texture": "#0", "uv": [0.0, 0.0, 4.0, 4.0]},
                    "west": {"texture": "#0", "uv": [0.0, 0.0, 12.0, 4.0]},
                    "up": {"texture": "#0", "uv": [0.0, 0.0, 4.0, 12.0]},
                    "down": {"texture": "#0", "uv": [0.0, 0.0, 4.0, 12.0]}
                }
            }
        ],
        "display": {
            "head": {
                "rotation": [
                    0,
                    0,
                    0
                ],
                "translation": [
                    0,
                    0,
                    0
                ],
                "scale": [
                    1.5,
                    1.5,
                    1.5
                ]
            }
        }
    },
    "WOOL:1": {
        "__comment": "Model generated using MrCrayfish's Model Creator (http://mrcrayfish.com/modelcreator/)",
        "textures": {
            "0": "red"
        },
        "elements": [
            {
                "name": "Cube",
                "from": [6.0, 4.0, 6.0],
                "to": [10.0, 16.0, 10.0],
                "faces": {
                    "north": {"texture": "#0", "uv": [0.0, 0.0, 4.0, 4.0]},
                    "east": {"texture": "#0", "uv": [0.0, 0.0, 12.0, 4.0]},
                    "south": {"texture": "#0", "uv": [0.0, 0.0, 4.0, 4.0]},
                    "west": {"texture": "#0", "uv": [0.0, 0.0, 12.0, 4.0]},
                    "up": {"texture": "#0", "uv": [0.0, 0.0, 4.0, 12.0]},
                    "down": {"texture": "#0", "uv": [0.0, 0.0, 4.0, 12.0]}
                }
            }
        ],
        "display": {
            "head": {
                "rotation": [
                    0,
                    0,
                    0
                ],
                "translation": [
                    0,
                    0,
                    0
                ],
                "scale": [
                    1.5,
                    1.5,
                    1.5
                ]
            }
        }
    },
    "WOOL:2": {
        "__comment": "Model generated using MrCrayfish's Model Creator (http://mrcrayfish.com/modelcreator/)",
        "textures": {
            "0": "yellow"
        },
        "elements": [
            {
                "name": "Cube",
                "from": [4.0, 0.0, 6.0],
                "to": [12.0, 12.0, 10.0],
                "faces": {
                    "north": {"texture": "#0", "uv": [0.0, 0.0, 8.0, 12.0]},
                    "east": {"texture": "#0", "uv": [0.0, 0.0, 4.0, 12.0]},
                    "south": {"texture": "#0", "uv": [0.0, 0.0, 8.0, 12.0]},
                    "west": {"texture": "#0", "uv": [0.0, 0.0, 4.0, 12.0]},
                    "up": {"texture": "#0", "uv": [0.0, 0.0, 8.0, 4.0]},
                    "down": {"texture": "#0", "uv": [0.0, 0.0, 8.0, 4.0]}
                }
            }
        ],
        "display": {
            "head": {
                "rotation": [
                    0,
                    0,
                    0
                ],
                "translation": [
                    0,
                    0,
                    0
                ],
                "scale": [
                    1.5,
                    1.5,
                    1.5
                ]
            }
        }
    }
};

function loadGeo(name) {
    alert(name + " - not found block");
}

var m = new THREE.MeshBasicMaterial({color: 0xff0000});


function setup() {
    width = $("#gl").width();
    height = $("#gl").height();

    renderer = new THREE.WebGLRenderer({antialias: true, alpha: true});
    renderer.setSize(width, height);
    $("#gl").append(renderer.domElement);


    scene = new THREE.Scene();


    camera = new THREE.PerspectiveCamera(45, width / height, 0.1, 1000);
    camera.position.y = 6;
    camera.position.z = 100;
    camera.position.x = 0;
    //camera.position.set( 110, 110, 250);

    camera.lookAt(new THREE.Vector3(0, 0, 0));
    scene.add(camera);

    //var pointLight = new THREE.PointLight(0xffffff);
    //pointLight.position.set(0, 300, 200);
    //
    //scene.add(pointLight);
    scene.add(new THREE.AmbientLight(0xffffff));


    rWrapper = new THREE.Object3D();
    loadParts();

    renderer.render(scene, camera);

}

function clearParts() {
    if (scene == undefined)return;
    for (var key in ridigBodyObjects) {
        console.log(">>", key);
        clearParts(ridigBodyObjects[key]);
    }
}

function clearPart(part) {
    if ("obj" in part)
        for (var i = 0; i < part.obj.length; i++)
            scene.remove(part.obj[i]);
    if ("children" in part)
        for (var c in part.children)
            clearPart(part.children[c]);
}


function loadParts() {
    clearParts();
    loadPart(rigidBodyData.root, undefined);
}

function loadPart(part, pList) {
    var matName = part.material.name;
    if ("data" in part.material)
        matName += ":" + part.material.data;
    console.log("Loading: ", part);
    if (!(matName in geoMat)) {
        loadGeo(matName);
        return;
    }
    var geo = geoMat[matName];
    var pD = {name: part.name, obj:[]};

    $.each(geo["elements"], function (key, val) {
        var w = val["to"][0] - val["from"][0], h = val["to"][1] - val["from"][1], d = val["to"][2] - val["from"][2];

        var geom = new THREE.BoxGeometry(w, h, d);
        var box = new THREE.Mesh(geom, m);
        box.name = key;
        box.position.set(val["from"][0] + 0.5 * w, val["from"][1] + 0.5 * h, val["from"][2] + 0.5 * d);
        scene.add(box);

        pD.obj.push(box);

        //return false;

    });
    if (pList == undefined){
        ridigBodyObjects[part.name]=pD;
    }else
        pList.push(pD);


    if ("children" in part) {
        pD["children"]=[];
        $.each(part.children, function (k, v) {
            loadPart(v, pD.children);
        });
    }
}































