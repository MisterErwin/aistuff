package gl.mc.mistererwin.galaxion;

import com.artemis.Entity;
import gl.mc.mistererwin.galaxion.components.ai.AI;
import gl.mc.mistererwin.galaxion.components.ai.ArmorStandEntitiy;
import gl.mc.mistererwin.galaxion.implementation.ai.goals.Goal_MoveToPosition;
import gl.mc.mistererwin.galaxion.manager.EntityTracker;
import gl.mc.mistererwin.galaxion.scenarios.IScenario;
import gl.mc.mistererwin.galaxion.scenarios.ScenarioLib;
import gl.mc.mistererwin.galaxion.scenarios.ScenarioManager;
import gl.mc.mistererwin.galaxion.utils.EasyCommandManager;
import gl.mc.mistererwin.galaxion.utils.libs.EntityLib;
import gl.mc.mistererwin.galaxion.websocket.AISocketServer;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.util.BlockIterator;

import java.util.Arrays;
import java.util.LinkedList;

//import com.comphenix.protocol.ProtocolLibrary;
//import com.comphenix.protocol.ProtocolManager;

public class AIStuff extends JavaPlugin {

    public static AIStuff instance;

    private ScenarioManager scenarioManager;

    private AISocketServer webSocket;

    public ScenarioManager getScenarioManager() {
        return scenarioManager;
    }


    public AISocketServer getWebSocket() {
        return webSocket;
    }

    //    World world;

//    public World getWorld() {
//        return world;
//    }
    // private ProtocolManager protocolManager;
//    WebWatcher w;


    @Override
    public void onEnable() {
        try {
            webSocket = new AISocketServer(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
        scenarioManager = new ScenarioManager(this);
        scenarioManager.addScenario(ScenarioLib.testScen());
//        world = new World();
//
//        world.setManager(new UuidEntityManager());
//
//        world.setManager(new GroupManager());
//        world.setManager(new TagManager());
//        world.setManager(new EntityTracker());
//        world.setManager(new PlayerManager());
//
//        world.setSystem(new AnimationSystem());
//        world.setSystem(new PathPlanningSystem());
//
//        world.setSystem(new AIProcessSystem());
//        world.setSystem(new AIArbitrateSystem());
//
//        world.setSystem(new MovementSystem());
//
//        world.setSystem(new WeaponSystem());
//        world.setSystem(new BulletSystem());
//
//
//        world.initialize();
//
//        getServer().getPluginManager().registerEvents(world.getSystem(WeaponSystem.class), this);
//        getServer().getPluginManager().registerEvents(world.getSystem(BulletSystem.class),this);
//
//        Bukkit.getScheduler().runTaskTimer(this, () -> tickWorld(), 1, 1);

        for (String r : Arrays.asList("walk", "run", "jump", "preJump", "idle"))
            saveResource("animations/" + r + ".ani.yml", false);

        // general
        // protocolManager = ProtocolLibrary.getProtocolManager();
        instance = this;
        Bukkit.getPluginManager().registerEvents(new Listener() {
            @EventHandler
            public void onPlayerInteract(PlayerInteractEvent e) {
                if (e.getPlayer().getItemInHand() == null || !e.getAction().equals(Action.RIGHT_CLICK_AIR))
                    return;
                if (e.getPlayer().getItemInHand().getType().equals(Material.REDSTONE)) {
                    IScenario is = getScenarioManager().getScenario(e.getPlayer());
                    if (is == null || !is.isRunning())
                        return;
                    BlockIterator it = new BlockIterator(e.getPlayer(), 100);
                    LinkedList<Entity> es = is.getWorld().getManager(EntityTracker.class).getWithComponent(AI.class);
                    while (it.hasNext()) {
                        Block b = it.next();


                        new LinkedList<Entity>(es).stream().filter(en -> en.getComponent(ArmorStandEntitiy.class) != null && en.getComponent(ArmorStandEntitiy.class).entity.getLocation().distanceSquared(b.getLocation()) < 19).forEach(en -> {
                            es.remove(en);
                            en.getComponent(AI.class).getGoal().addSubgoal(new Goal_MoveToPosition(en, e.getPlayer().getLocation().add(0, -1, 0)));
                            e.getPlayer().sendMessage("Found one");
                        });
                    }
                } else if (e.getPlayer().getItemInHand().getType().equals(Material.BLAZE_ROD)) {
                    IScenario is = getScenarioManager().getScenario(e.getPlayer());
                    if (is == null || !is.isRunning())
                        return;

                    e.getPlayer().sendMessage("Spawned");
                    EntityLib.addAS(e.getPlayer().getLocation(), is);
                }

            }

            @EventHandler
            public void onDisconnect(PlayerQuitEvent e) {
                scenarioManager.leaveScenario(e.getPlayer());
            }

        }, this);

//        getCommand("galaxion").setExecutor(new CommandManager());
        EasyCommandManager ecm = new EasyCommandManager();
        getCommand("galaxion").setExecutor(ecm);
        CommandRegister.registerCommands(ecm);
    }


    @Override
    public void onDisable() {
        try {
            webSocket.stop();
        }catch (Exception|Error e){
            e.printStackTrace();
        }

        scenarioManager.disable();
    }


}
