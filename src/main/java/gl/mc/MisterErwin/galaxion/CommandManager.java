package gl.mc.mistererwin.galaxion;

import com.artemis.Entity;
import gl.mc.mistererwin.galaxion.components.ai.Target;
import gl.mc.mistererwin.galaxion.manager.EntityTracker;
import gl.mc.mistererwin.galaxion.scenarios.IScenario;
import gl.mc.mistererwin.galaxion.systems.AnimationSystem;
import gl.mc.mistererwin.galaxion.systems.WeaponSystem;
import gl.mc.mistererwin.galaxion.utils.AWorldUtils;
import gl.mc.mistererwin.galaxion.utils.jsonchatlib.*;
import gl.mc.mistererwin.galaxion.utils.libs.EntityLib;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Map;


/**
 * Created by mistererwin on 23.05.2015.
 * In case you need it, ask me ;)
 */
public class CommandManager implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] args) {
        if (is(args, "ani", "reload") || is(args, "animations", "reload")) {
            AnimationSystem.reload();
            commandSender.sendMessage("Reloading...");
        } else if (is(args, "weapon", "give") || is(args, "w", "give") && args.length > 2) {
            commandSender.sendMessage("Giving gun " + args[2]);
            IScenario scenario = AIStuff.instance.getScenarioManager().getScenario((Player) commandSender);
            if (scenario != null && scenario.isRunning())
                scenario.getWorld().getSystem(WeaponSystem.class).giveWeapon(((Player) commandSender).getInventory(),
                        AWorldUtils.getEntityByBukktiPlayer(scenario.getWorld(), (Player) commandSender),
                        args[2]);
            else
                commandSender.sendMessage("You have to be in a running scenario");
        } else if (is(args, "scenario", "join") || is(args, "s", "join") && args.length > 2) {
            AIStuff.instance.getScenarioManager().joinScenario((Player) commandSender, args[2]);
        } else if (is(args, "scenario", "start") || is(args, "s", "start") && args.length > 2) {
            IScenario is = AIStuff.instance.getScenarioManager().getScenario(args[2]);
            if (is != null)
                AIStuff.instance.getScenarioManager().startScenario(is);

        } else if (is(args, "scenario") || is(args, "s")) {
            commandSender.sendMessage("List of scenarios:");
            AIStuff.instance.getScenarioManager().getScenarios().forEach(scenario -> {
                        JSONChatMessage m = new JSONChatMessage(">", JSONChatColor.YELLOW, null);
                        JSONChatExtra e = new JSONChatExtra(scenario.getUniqueName(), (scenario.isRunning() ? JSONChatColor.GREEN : JSONChatColor.GRAY), null);
                        e.setHoverEvent(JSONChatHoverEventType.SHOW_TEXT, "Click to join");
                        e.setClickEvent(JSONChatClickEventType.RUN_COMMAND, "/galaxion scenario join " + scenario.getUniqueName());
                        m.addExtra(e);
                        m.sendToPlayer((Player) commandSender);
                    }
            );
        } else if (is(args, "target")) {
            IScenario scenario = AIStuff.instance.getScenarioManager().getScenario((Player) commandSender);
            if (scenario != null && scenario.isRunning()) {
                Entity pl = AWorldUtils.getEntityByBukktiPlayer(scenario.getWorld(), (Player) commandSender);
                for (Map.Entry<Entity, Target> e : scenario.getWorld().getManager(EntityTracker.class).getBothWithComponent(Target.class)) {
                    if (AWorldUtils.getLocation(e.getKey()).distanceSquared(((Player) commandSender).getLocation()) < 1000) {
                        e.getValue().target = pl;
                        commandSender.sendMessage("Set target on you");
                    }
                }
            } else
                commandSender.sendMessage("You have to be in a running scenario");
        } else if (is(args, "spawn")) {

            IScenario scenario = AIStuff.instance.getScenarioManager().getScenario((Player) commandSender);
            if (scenario != null && scenario.isRunning()) {
                if (args.length > 1) {
                    switch (args[1].toLowerCase()) {
                        case "as":
                        case "armorstand":
                            EntityLib.addAS(((Player) commandSender).getLocation(), scenario);
                            return true;
                        case "dummy":
                        case "d":
                            EntityLib.addDummy(((Player) commandSender).getLocation(), scenario);
                            return true;
                        case "rigid":
                        case "r":
                            EntityLib.addRigid(((Player) commandSender).getLocation(), scenario);
                            return true;

                    }
                }
                JSONChatMessage m = new JSONChatMessage("Avaible types: ", JSONChatColor.AQUA, null);
                JSONChatExtra e = new JSONChatExtra("ArmorStand  ", JSONChatColor.GOLD, null);
                e.setClickEvent(JSONChatClickEventType.RUN_COMMAND, "/galaxion spawn armorstand");
                m.addExtra(e);
                e = new JSONChatExtra("Dummy  ", JSONChatColor.GOLD, null);
                e.setClickEvent(JSONChatClickEventType.RUN_COMMAND, "/galaxion spawn dummy");
                m.addExtra(e);
                e = new JSONChatExtra("rigid  ", JSONChatColor.GOLD, null);
                e.setClickEvent(JSONChatClickEventType.RUN_COMMAND, "/galaxion spawn rigid");
                m.addExtra(e);

                m.sendToPlayer((Player) commandSender);
            } else
                commandSender.sendMessage("You have to be in a running scenario");
        } else if (is(args, "set")) {
//
//            if (args.length < 6)
//                commandSender.sendMessage("galaxion set [nr] <t/r> [valX] [valY] [valZ]");
//            else {
//                IScenario scenario = AIStuff.instance.getScenarioManager().getScenario((Player) commandSender);
//                if (scenario != null && scenario.isRunning()) {
//                    AniPart ap = scenario.getWorld().getManager(EntityTracker.class).getComWithComponent(AniPart.class).get(0);
//                    int slot = Integer.parseInt(args[1]);
//                    AniPart p = (slot >= 0) ? ap.children[slot] : ap;
//                    if (args[2].equalsIgnoreCase("r")) {
//                        p.localTransform.setRotation(new Quaternion(new float[]{Float.parseFloat(args[3]), Float.parseFloat(args[4]), Float.parseFloat(args[5])}));
//                    } else {
//                        p.localTransform.setTranslation(Float.parseFloat(args[3]), Float.parseFloat(args[4]), Float.parseFloat(args[5]));
//                    }
//                } else
//                    commandSender.sendMessage("You have to be in a running scenario");
//
//
//            }
        } else
            commandSender.sendMessage("no command found");
        return true;
    }

    private boolean is(String[] args, String... options) {
        if (args.length < options.length)
            return false;
        for (int i = 0; i < options.length; i++)
            if (!args[i].equalsIgnoreCase(options[i]))
                return false;
        return true;
    }
}
