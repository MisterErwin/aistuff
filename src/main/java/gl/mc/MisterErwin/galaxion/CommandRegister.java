package gl.mc.mistererwin.galaxion;

import com.artemis.Entity;
import gl.mc.mistererwin.galaxion.components.animation.Animated;
import gl.mc.mistererwin.galaxion.components.animation.Animations;
import gl.mc.mistererwin.galaxion.components.character.Parted;
import gl.mc.mistererwin.galaxion.implementation.animations.LoopMode;
import gl.mc.mistererwin.galaxion.manager.EntityTracker;
import gl.mc.mistererwin.galaxion.scenarios.IScenario;
import gl.mc.mistererwin.galaxion.systems.AnimationSystem;
import gl.mc.mistererwin.galaxion.systems.WeaponSystem;
import gl.mc.mistererwin.galaxion.utils.AWorldUtils;
import gl.mc.mistererwin.galaxion.utils.EasyCommandManager;
import gl.mc.mistererwin.galaxion.utils.jsonchatlib.*;
import gl.mc.mistererwin.galaxion.utils.libs.EntityLib;
import gl.mc.mistererwin.galaxion.utils.math.Quaternion;
import gl.mc.mistererwin.galaxion.utils.math.Vector3f;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.BlockCommandSender;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Arrays;
import java.util.List;

/**
 * Created by mistererwin on 07.07.2015.
 * In case you need it, ask me ;)
 */
public class CommandRegister {

    public static void registerCommands(EasyCommandManager ecm) {
        ecm.registerCommand(new EasyCommandManager.EasyCommand() {
            @Override
            public void onCommand(String[] args, CommandSender commandSender) {
                commandSender.sendMessage("HEY");
            }
        },"hey").setAlias("h");

        ecm.registerCommand(new EasyCommandManager.EasyCommand() {
            @Override
            public void onCommand(String[] args, CommandSender commandSender) {
                AnimationSystem.reload();
                commandSender.sendMessage("Reloading...");

            }
        }, "ani", "reload");
        ecm.registerCommand(new EasyCommandManager.EasyCommand() {
            @Override
            public void onCommand(String[] args, CommandSender commandSender) {
                commandSender.sendMessage("Giving gun " + args[2]);
                IScenario scenario = AIStuff.instance.getScenarioManager().getScenario((Player) commandSender);
                if (scenario != null && scenario.isRunning())
                    scenario.getWorld().getSystem(WeaponSystem.class).giveWeapon(((Player) commandSender).getInventory(),
                            AWorldUtils.getEntityByBukktiPlayer(scenario.getWorld(), (Player) commandSender),
                            args[2]);
                else
                    commandSender.sendMessage("You have to be in a running scenario");

            }
        }, "weapon", "give", EasyCommandManager.PLACEHOLDER_ANYTHING).setAlias("w", "give", EasyCommandManager.PLACEHOLDER_ANYTHING);

        ecm.registerCommand(new EasyCommandManager.EasyCommand() {
            @Override
            public void onCommand(String[] args, CommandSender commandSender) {
                AIStuff.instance.getScenarioManager().joinScenario((Player) commandSender, args[2]);
            }
        }, "scenario", "join", EasyCommandManager.PLACEHOLDER_ANYTHING).setAlias("s", "join", EasyCommandManager.PLACEHOLDER_ANYTHING);

        ecm.registerCommand(new EasyCommandManager.EasyCommand() {
            @Override
            public void onCommand(String[] args, CommandSender commandSender) {
                commandSender.sendMessage("List of scenarios:");
                AIStuff.instance.getScenarioManager().getScenarios().forEach(scenario -> {
                            JSONChatMessage m = new JSONChatMessage(">", JSONChatColor.YELLOW, null);
                            JSONChatExtra e = new JSONChatExtra(scenario.getUniqueName(), (scenario.isRunning() ? JSONChatColor.GREEN : JSONChatColor.GRAY), null);
                            e.setHoverEvent(JSONChatHoverEventType.SHOW_TEXT, "Click to join");
                            e.setClickEvent(JSONChatClickEventType.RUN_COMMAND, "/galaxion scenario join " + scenario.getUniqueName());
                            m.addExtra(e);
                            m.sendToPlayer((Player) commandSender);
                        }
                );
            }
        }, "scenario").setAlias("s");
        ecm.registerCommand(new EasyCommandManager.EasyCommand() {
            @Override
            public void onCommand(String[] args, CommandSender commandSender) {
                IScenario scenario = AIStuff.instance.getScenarioManager().getScenario((Player) commandSender);
                EntityLib.addRigid(((Player) commandSender).getLocation(), scenario);
            }
        }, "spawn", "rigid");
        ecm.registerCommand(new EasyCommandManager.EasyCommand() {
            @Override
            public void onCommand(String[] args, CommandSender commandSender) {
                IScenario scenario = AIStuff.instance.getScenarioManager().getScenario((Player) commandSender);
                EntityLib.addAS(((Player) commandSender).getLocation(), scenario);
            }
        }, "spawn", "as").setAlias("spawn", "armorstand");


        ecm.registerCommand(new EasyCommandManager.EasyCommand() {
            @Override
            public void onCommand(String[] args, CommandSender commandSender) {
                Player p = null;
                if (getString(args, 0).equals("@p"))
                    if (commandSender instanceof BlockCommandSender)
                        p = getNearbyPlayer(((BlockCommandSender) commandSender).getBlock().getLocation());
                    else if (commandSender instanceof Player)
                        p = getNearbyPlayer(((Player) commandSender).getLocation(), p);
                    else {
                        commandSender.sendMessage("Sorry, can't get your Location");
                        return;
                    }
                else
                    p = Bukkit.getPlayer(getString(args, 1));
                if (p == null) {
                    commandSender.sendMessage(getString(args, 1) + " not found!");
                    return;
                }
                IScenario scenario = AIStuff.instance.getScenarioManager().getScenario(p);
                if (scenario == null) {
                    scenario = AIStuff.instance.getScenarioManager().getScenarios().get(0);
                    AIStuff.instance.getScenarioManager().joinScenario(p, scenario.getUniqueName());
                }
                EntityLib.addRigid(p.getLocation(), scenario);
            }
        }, "joinandspawn", EasyCommandManager.PLACEHOLDER_ANYTHING).setAlias("jas", EasyCommandManager.PLACEHOLDER_ANYTHING);

        ecm.registerCommand(new EasyCommandManager.EasyCommand() {
            @Override
            public void onCommand(String[] args, CommandSender commandSender) {
                IScenario scenario = AIStuff.instance.getScenarioManager().getScenario((Player) commandSender);
                if (scenario == null) {
                    scenario = AIStuff.instance.getScenarioManager().getScenarios().get(0);
                    AIStuff.instance.getScenarioManager().joinScenario((Player) commandSender, scenario.getUniqueName());
                }
                EntityLib.addRigid(((Player) commandSender).getLocation(), scenario);
            }
        }, "joinandspawn").setAlias("jas");

        ecm.registerCommand(new EasyCommandManager.EasyCommand() {
            @Override
            public void onCommand(String[] args, CommandSender commandSender) {
                IScenario scenario = null;

                if (commandSender instanceof Player)
                    scenario = AIStuff.instance.getScenarioManager().getScenario((Player) commandSender);
                if (scenario == null) {
                    scenario = AIStuff.instance.getScenarioManager().getScenarios().get(0);
                }
                for (Entity entity : scenario.getWorld().getManager(EntityTracker.class).getWithComponent(Animations.class)) {
                    try {
                        entity.getComponent(Animated.class).channels.get(0).setAnim(getString(args, 1), entity.getComponent(Animations.class));
                    } catch (IllegalArgumentException ex) {
                        commandSender.sendMessage(ChatColor.RED + getString(args, 1) + " not found!");
                        break;
                    }
                    entity.getComponent(Animated.class).channels.get(0).setLoopMode(LoopMode.Loop);
                }

            }
        }, "anim", EasyCommandManager.PLACEHOLDER_ANYTHING, "loop").setAlias("anim", EasyCommandManager.PLACEHOLDER_ANYTHING, "l").setAlias("anim", EasyCommandManager.PLACEHOLDER_ANYTHING);

        ecm.registerCommand(new EasyCommandManager.EasyCommand() {
            @Override
            public void onCommand(String[] args, CommandSender commandSender) {
                IScenario scenario = null;
                if (commandSender instanceof Player)
                    scenario = AIStuff.instance.getScenarioManager().getScenario((Player) commandSender);
                if (scenario == null) {
                    scenario = AIStuff.instance.getScenarioManager().getScenarios().get(0);
                }
                for (Entity entity : scenario.getWorld().getManager(EntityTracker.class).getWithComponent(Animations.class)) {
                    try {
                        entity.getComponent(Animated.class).channels.get(0).setAnim(getString(args, 1), entity.getComponent(Animations.class));
                    } catch (IllegalArgumentException ex) {
                        commandSender.sendMessage(ChatColor.RED + getString(args, 1) + " not found!");
                        break;
                    }
                    entity.getComponent(Animated.class).channels.get(0).setLoopMode(LoopMode.DontLoop);
                }

            }
        }, "anim", EasyCommandManager.PLACEHOLDER_ANYTHING, "dontloop").setAlias("anim", EasyCommandManager.PLACEHOLDER_ANYTHING, "dl");


        ecm.registerCommand(new EasyCommandManager.EasyCommand() {
            @Override
            public void onCommand(String[] args, CommandSender commandSender) {
                IScenario scenario = null;
                if (commandSender instanceof Player)
                    scenario = AIStuff.instance.getScenarioManager().getScenario((Player) commandSender);
                if (scenario == null) {
                    scenario = AIStuff.instance.getScenarioManager().getScenarios().get(0);
                }
                Parted p = scenario.getWorld().getManager(EntityTracker.class).getComWithComponent(Parted.class).get(0);
                if (p.partList.length < getInt(args, 1))
                    commandSender.sendMessage("Not a valid entitiypart");
                else {
                    commandSender = (commandSender instanceof BlockCommandSender)?Bukkit.getConsoleSender():commandSender;
                    Quaternion quaternion = p.partList[getInt(args, 1)].localTransform.getRotation();
                    float[] angles = quaternion.toAngles(null);
                    commandSender.sendMessage("Local: x:" + angles[0] + "  y:" + angles[1] + "   z:" + angles[2] + quaternion.toString() + "//" +
                            (quaternion.getX() * quaternion.getX() + quaternion.getY() * quaternion.getY() + quaternion.getZ() * quaternion.getZ() + quaternion.getW() * quaternion.getW()) + "//" +
                            (quaternion.getX() * quaternion.getY() + quaternion.getZ() * quaternion.getW()));
                    Vector3f angle = new Vector3f();
                    float a = quaternion.toAngleAxis(angle);
                    commandSender.sendMessage("angle:" + a + "  " + angle.toString());
                }
            }
        }, "getpose", EasyCommandManager.PLACEHOLDER_INT).setAlias("gp", EasyCommandManager.PLACEHOLDER_INT);



//        ecm.registerCommand(new EasyCommandManager.EasyCommand() {
//            @Override
//            public void onCommand(String[] args, CommandSender commandSender) {
//                IScenario scenario = null;
//                if (commandSender instanceof Player)
//                    scenario = AIStuff.instance.getScenarioManager().getScenario((Player) commandSender);
//                if (scenario == null) {
//                    scenario = AIStuff.instance.getScenarioManager().getScenarios().get(0);
//                }
//                Parted p = scenario.getWorld().getManager(EntityTracker.class).getComWithComponent(Parted.class).get(0);
//                if (p.partList.length < getInt(args, 1))
//                    commandSender.sendMessage("Not a valid entitiypart");
//                else {
//                    commandSender = (commandSender instanceof BlockCommandSender)?Bukkit.getConsoleSender():commandSender;
//                    Quaternion quaternion = p.partList[getInt(args, 1)].localTransform.getRotation();
//                    Vector3f angle = new Vector3f((float)getDouble(args,2),(float)getDouble(args,3),(float)getDouble(args,4));
//                    float a = quaternion.t
//                    commandSender.sendMessage("angle:" + a + "  " + angle.toString());
//                }
//            }
//        }, "getpose", EasyCommandManager.PLACEHOLDER_INT, EasyCommandManager.PLACEHOLDER_DOUBLE, EasyCommandManager.PLACEHOLDER_DOUBLE, EasyCommandManager.PLACEHOLDER_DOUBLE).
//                setAlias("gp", EasyCommandManager.PLACEHOLDER_INT, EasyCommandManager.PLACEHOLDER_DOUBLE, EasyCommandManager.PLACEHOLDER_DOUBLE, EasyCommandManager.PLACEHOLDER_DOUBLE);

        ecm.registerCommand(new EasyCommandManager.EasyCommand() {
            @Override
            public void onCommand(String[] args, CommandSender commandSender) {
                IScenario scenario = null;
                if (commandSender instanceof Player)
                    scenario = AIStuff.instance.getScenarioManager().getScenario((Player) commandSender);
                if (scenario == null) {
                    scenario = AIStuff.instance.getScenarioManager().getScenarios().get(0);
                }
                Parted p = scenario.getWorld().getManager(EntityTracker.class).getComWithComponent(Parted.class).get(0);
                if (p.partList.length < getInt(args, 1))
                    commandSender.sendMessage("Not a valid entitiypart");
                else {
                    p.partList[getInt(args, 1)].userControlled = true;
                    p.partList[getInt(args, 1)].localTransform.getRotation().fromAngles(
                            (float) Math.toRadians(Float.valueOf(args[2])), (float) Math.toRadians(Float.valueOf(args[3])), (float) Math.toRadians(Float.valueOf(args[4]))
                    ).normalizeLocal();
                }
            }
        }, "setpose", EasyCommandManager.PLACEHOLDER_INT, EasyCommandManager.PLACEHOLDER_DOUBLE, EasyCommandManager.PLACEHOLDER_DOUBLE, EasyCommandManager.PLACEHOLDER_DOUBLE).setAlias("sp", EasyCommandManager.PLACEHOLDER_INT, EasyCommandManager.PLACEHOLDER_DOUBLE, EasyCommandManager.PLACEHOLDER_DOUBLE, EasyCommandManager.PLACEHOLDER_DOUBLE);


        ecm.registerCommand(new EasyCommandManager.EasyCommand() {
            @Override
            public void onCommand(String[] args, CommandSender commandSender) {
                IScenario scenario = null;
                if (commandSender instanceof Player)
                    scenario = AIStuff.instance.getScenarioManager().getScenario((Player) commandSender);
                if (scenario == null) {
                    scenario = AIStuff.instance.getScenarioManager().getScenarios().get(0);
                }
                Parted p = scenario.getWorld().getManager(EntityTracker.class).getComWithComponent(Parted.class).get(0);
                if (p.partList.length < getInt(args, 1))
                    commandSender.sendMessage("Not a valid entitiypart");
                else {
                    p.partList[getInt(args, 1)].userControlled = true;
                    p.partList[getInt(args, 1)].localTransform.getRotation().fromAngleAxis((float) getDouble(args, 5), new Vector3f(
                                    (float) Math.toRadians(Float.valueOf(args[2])), (float) Math.toRadians(Float.valueOf(args[3])), (float) Math.toRadians(Float.valueOf(args[4])))
                    ).normalizeLocal();
                }
            }
        }, "setpose", EasyCommandManager.PLACEHOLDER_INT, EasyCommandManager.PLACEHOLDER_DOUBLE, EasyCommandManager.PLACEHOLDER_DOUBLE, EasyCommandManager.PLACEHOLDER_DOUBLE,EasyCommandManager.PLACEHOLDER_DOUBLE)
                .setAlias("sp", EasyCommandManager.PLACEHOLDER_INT, EasyCommandManager.PLACEHOLDER_DOUBLE, EasyCommandManager.PLACEHOLDER_DOUBLE, EasyCommandManager.PLACEHOLDER_DOUBLE, EasyCommandManager.PLACEHOLDER_DOUBLE);


        ecm.registerCommand(new EasyCommandManager.EasyCommand() {
            @Override
            public void onCommand(String[] args, CommandSender commandSender) {
                IScenario scenario = null;
                if (commandSender instanceof Player)
                    scenario = AIStuff.instance.getScenarioManager().getScenario((Player) commandSender);
                if (scenario == null) {
                    scenario = AIStuff.instance.getScenarioManager().getScenarios().get(0);
                }
                Parted p = scenario.getWorld().getManager(EntityTracker.class).getComWithComponent(Parted.class).get(0);
                for (int i = 0, l = p.partList.length; i < l; i++)
                    System.out.println(i + ">>" + p.partList[i].name);

            }
        }, "getpartindex").setAlias("gpi");

        ecm.registerCommand(new EasyCommandManager.EasyCommand() {
            @Override
            public void onCommand(String[] args, CommandSender commandSender) {
                IScenario scenario = null;
                if (commandSender instanceof Player)
                    AIStuff.instance.getScenarioManager().getScenario((Player) commandSender);
                if (scenario == null)
                    scenario = AIStuff.instance.getScenarioManager().getScenarios().get(0);

                scenario.getWorld().getManager(EntityTracker.class).getWithComponent(Parted.class).get(0).deleteFromWorld();

            }
        }, "del").setAlias("delete");
    }


    private static Player getNearbyPlayer(Location location, Player... exclude) {
        List<Player> ex = Arrays.asList(exclude);
        Player player = null;
        double dist = Double.MAX_VALUE, tdist;
        for (Player p : location.getWorld().getPlayers())
            if (!ex.contains(p) && (tdist = p.getLocation().distanceSquared(location)) < dist) {
                dist = tdist;
                player = p;
            }
        return player;
    }
}
