package gl.mc.mistererwin.galaxion.utils;

import com.artemis.Entity;
import com.artemis.World;
import com.artemis.managers.PlayerManager;
import com.artemis.managers.UuidEntityManager;
import com.artemis.utils.ImmutableBag;
import gl.mc.mistererwin.galaxion.AIStuff;
import gl.mc.mistererwin.galaxion.components.ai.ArmorStandEntitiy;
import gl.mc.mistererwin.galaxion.components.generic.PlayerEntity;
import gl.mc.mistererwin.galaxion.scenarios.IScenario;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;

/**
 * Created by mistererwin on 24.05.2015.
 * In case you need it, ask me ;)
 */
public class AWorldUtils {

    public static org.bukkit.entity.Entity getEntity(Entity e) {
        ArmorStandEntitiy ex = e.getComponent(ArmorStandEntitiy.class);
        if (ex != null)
            return ex.entity;
        if (e.getComponent(PlayerEntity.class) != null)
            return Bukkit.getPlayerExact(e.getWorld().getManager(PlayerManager.class).getPlayer(e));
        return null;

    }

    public static IScenario getScenarioByBukkitEntity(org.bukkit.entity.Entity e){
        if (!e.hasMetadata("scenarioName"))
            return null;

       return AIStuff.instance.getScenarioManager().getScenario(e.getMetadata("scenarioName").get(0).asString());
    }

    public static Location getLocation(Entity e) {
        org.bukkit.entity.Entity be;
        return ((be = getEntity(e)) != null) ? be.getLocation() : null;
    }

    public static Player getPlayerByEntity(Entity e) {
        String s = e.getWorld().getManager(PlayerManager.class).getPlayer(e);
        return (s!=null)?Bukkit.getPlayerExact(s):null;
    }

    public static Entity getEntityByBukktiPlayer(World w, org.bukkit.entity.Player player) {
        ImmutableBag<Entity> be = w.getManager(PlayerManager.class).getEntitiesOfPlayer(player.getName());
        if (be.isEmpty())
            return null;
        return be.get(0);
    }

    public static Entity getEntityByBukktiEntity(World w, org.bukkit.entity.Entity entity) {
        return w.getManager(UuidEntityManager.class).getEntity(entity.getUniqueId());
    }


}
