package gl.mc.mistererwin.galaxion.utils;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import java.util.ArrayList;

/**
 * Created by mistererwin on 07.07.2015.
 * In case you need it, ask me ;)
 */
public class EasyCommandManager implements CommandExecutor {

    public static final String PLACEHOLDER_ANYTHING = "{P}";
    public static final String PLACEHOLDER_INT = "{PI}";
    public static final String PLACEHOLDER_DOUBLE = "{PD}";

    private ECMNode root = new ECMNode(null, null, new ArrayList<>());


    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] args) {
        if (args.length > 0)
            for (ECMNode n : root.node)
                if (onCMD(args, n, 0, commandSender))
                    return true;

        commandSender.sendMessage("Not handled");
        return true;
    }

    public ECMRet registerCommand(EasyCommand cmd, String... args) {
        ECMNode n = root, b;
        for (int i = 0; i < args.length - 1; i++) {
            b = getNode(n, args[i]);
            if (b == null) {
                b = new ECMNode(args[i], null, new ArrayList<>());
                n.node.add(b);
            }
            n = b;
        }
        b = getNode(n, args[args.length - 1]);
        if (b == null)
            b = new ECMNode(args[args.length - 1], cmd, new ArrayList<>());
        else
            b.cmd = cmd;
        n.node.add(b);

        return new ECMRet(this, cmd);
    }

    private ECMNode getNode(ECMNode parent, String key) {
        if (parent.node == null) return null;
        for (ECMNode n : parent.node)
            if (n.key.equals(key))
                return n;
        return null;
    }

    private boolean equals(String a, String b) {
        return (a == PLACEHOLDER_ANYTHING || (a == PLACEHOLDER_DOUBLE && MathUtils.isDouble(b)) || (a == PLACEHOLDER_INT && MathUtils.isInt(b)) || a.equals(b));
    }

    private boolean onCMD(String[] args, ECMNode cn, int n, CommandSender sender) {
        if (n >= args.length || !equals(cn.key, args[n]))
            return false;
        if (cn.node != null && n < args.length ) {
            for (ECMNode nn : cn.node)
                if (onCMD(args, nn, n + 1, sender))
                    return true;
        }
        if (cn.cmd == null) return false;
        try {
            cn.cmd.onCommand(args, sender);
        } catch (Exception e) {
            e.printStackTrace();
            sender.sendMessage(ChatColor.RED + "An exception occured " + ChatColor.GRAY + e.getLocalizedMessage());
        } catch (Error e) {
            e.printStackTrace();
            sender.sendMessage(ChatColor.RED + "An error occured " + ChatColor.GRAY + e.getLocalizedMessage());
        }
        return true;
    }


    private class ECMNode {
        private final String key;
        private final ArrayList<ECMNode> node;
        private EasyCommand cmd;

        public ECMNode(String key, EasyCommand cmd, ArrayList<ECMNode> node) {
            this.key = key;
            this.cmd = cmd;
            this.node = node;
        }


    }

    public class ECMRet {
        private final EasyCommandManager ecm;
        private final EasyCommand command;

        private ECMRet(EasyCommandManager ecm, EasyCommand command) {
            this.ecm = ecm;
            this.command = command;
        }

        public ECMRet setAlias(String... keys) {
            ecm.registerCommand(this.command, keys);
            return this;
        }
    }

    public abstract static class EasyCommand {
        public abstract void onCommand(String[] args, CommandSender commandSender);

        protected static String getString(String[] args, int index) {
            return args[index];
        }

        protected static int getInt(String[] args, int index) {
            return MathUtils.toInt(getString(args, index));
        }

        protected static double getDouble(String[] args, int index) {
            return MathUtils.toDouble(getString(args, index));
        }
    }
}
