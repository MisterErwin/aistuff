package gl.mc.mistererwin.galaxion.utils;

import org.apache.commons.lang.Validate;
import org.apache.commons.lang.WordUtils;
import org.bukkit.ChatColor;

import java.lang.reflect.Method;
import java.text.MessageFormat;
import java.util.Arrays;
import java.util.List;
import java.util.StringJoiner;

/**
 * Created by mistererwin on 24.05.2015.
 * In case you need it, ask me ;)
 */
public class FormatUtil
{
    private static final String[] rainbowColors = { "c", "6", "e", "a", "b", "d", "5" };

    private static final List<String> VOWELS = Arrays.asList("a", "e", "i", "o", "u");

    public static String format(String format, Object[] objects)
    {
        Validate.notNull(format, "format cannot be null!");
        try
        {
            format = MessageFormat.format(format, objects);
        } catch (Throwable ex) {
        }
        return replaceColors(format);
    }

    public static String replaceColors(String message)
    {
        Validate.notNull(message, "message cannot be null!");
        message = message.replaceAll("(&([zZ]))", "&z");
        if (message.contains("&z"))
        {
            StringBuilder ret = new StringBuilder();
            String[] ss = message.split("&z");
            ret.append(ss[0]);
            ss[0] = null;

            for (String s : ss)
            {
                if (s != null)
                {
                    int index = 0;
                    while ((index < s.length()) && (s.charAt(index) != '&'))
                    {
                        ret.append("&" + rainbowColors[(index % rainbowColors.length)]);
                        ret.append(s.charAt(index));
                        index++;
                    }

                    if (index < s.length())
                    {
                        ret.append(s.substring(index));
                    }
                }
            }

            message = ret.toString();
        }

        return ChatColor.translateAlternateColorCodes('&', message);
    }

    public static String getFriendlyName(Object obj)
    {
        Validate.notNull(obj, "obj cannot be null!");
        try
        {
            Method method = ReflectionUtils.getMethod(obj.getClass(), "toString");
            if (method.getDeclaringClass().getSuperclass() == null)
                return obj.getClass().getSimpleName();
        } catch (Throwable ex) {  }

        return getFriendlyName(obj.toString());
    }

    public static String getFriendlyName(String string)
    {
        Validate.notNull(string, "string cannot be null!");

        String ret = string.toLowerCase();
        ret = ret.replaceAll("_", " ");
        return WordUtils.capitalize(ret);
    }

    public static String getArticle(String string)
    {
        Validate.notNull(string, "string cannot be null!");

        string = string.toLowerCase();
        for (String vowel : VOWELS)
        {
            if (string.startsWith(vowel)) {
                return "an";
            }
        }
        return "a";
    }

    public static String getPlural(String string, int amount)
    {
        Validate.notNull(string, "string cannot be null!");

        amount = Math.abs(amount);
        if ((amount == 0) || (amount > 1))
        {
            if (!string.toLowerCase().endsWith("s")) {
                return string + "s";
            }
        }
        return string;
    }

    public static String join(String delimiter, String[] args)
    {
        Validate.notNull(delimiter, "glue cannot be null");
        Validate.noNullElements(args, "args cannot have null elements!");
        StringJoiner sj = new StringJoiner(delimiter);
        for (String a : args)
            sj.add(a);
        return sj.toString();

//        return new StringJoiner(delimiter).appendAll(args).toString();
    }

    public static String join(String[] args)
    {
        return join(" ", args);
//        Validate.noNullElements(args, "args cannot have null elements!");
//
//        return StringJoiner.SPACE.newString().appendAll(args).toString();
    }
}