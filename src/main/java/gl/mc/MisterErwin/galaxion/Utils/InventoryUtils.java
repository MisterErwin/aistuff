package gl.mc.mistererwin.galaxion.utils;

import org.apache.commons.lang.Validate;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

/**
 * Created by mistererwin on 24.05.2015.
 * In case you need it, ask me ;)
 */
public class InventoryUtils {

    public static int amount(Player p, MyMaterial mat) {
        return amount(p.getInventory(), mat.getMaterial(), mat.getData());
    }


    public static int amount(Inventory inventory, Material type, short dat) {
        Validate.notNull(inventory, "inventory cannot be null!");
        Validate.notNull(type, "type cannot be null!");

        int ret = 0;
        ItemStack[] items = inventory.getContents();
        for (int slot = 0; slot < items.length; slot++) {
            ItemStack item = items[slot];
            if ((item != null) && (item.getType() != Material.AIR)) {
                Material mat = item.getType();
                short duration = item.getDurability();
                int amt = item.getAmount();
                if (mat == type) {
                    if ((dat == -1) || (dat == duration)) {
                        ret += amt;
                    }
                }
            }
        }
        return ret;
    }

    public static void remove(Player p, MyMaterial m, int amt) {
        remove(p.getInventory(), m.getMaterial(), m.getData(), amt);
    }


    public static void remove(Inventory inventory, Material type, short dat, int amt) {
        Validate.notNull(inventory, "inventory cannot be null!");
        Validate.notNull(type, "type cannot be null!");
        Validate.isTrue(amt > 0, "amt cannot be less than 0!");

        int start = amt;
        if (inventory != null) {
            ItemStack[] items = inventory.getContents();
            for (int slot = 0; slot < items.length; slot++) {
                if (items[slot] != null) {
                    Material mat = items[slot].getType();
                    short duration = items[slot].getDurability();
                    int itmAmt = items[slot].getAmount();
                    if ((mat == type) && ((dat == duration) || (dat == -1))) {
                        if (amt > 0) {
                            if (itmAmt >= amt) {
                                itmAmt -= amt;
                                amt = 0;
                            } else {
                                amt = start - itmAmt;
                                itmAmt = 0;
                            }
                            if (itmAmt > 0) {
                                inventory.getItem(slot).setAmount(itmAmt);
                            } else {
                                inventory.setItem(slot, null);
                            }
                        }
                        if (amt <= 0)
                            return;
                    }
                }
            }
        }
    }

}
