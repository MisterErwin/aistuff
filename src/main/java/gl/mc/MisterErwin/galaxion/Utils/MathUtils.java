package gl.mc.mistererwin.galaxion.utils;

import java.text.DecimalFormat;

/**
 * Created by mistererwin on 30.04.2015.
 */
public class MathUtils {

    public static double double_epsilon = 2.2204460492503131e-16;

    public static float clamp(float val, float min, float max) {
        return Math.max(min, Math.min(max, val));
    }

    public static double clamp(double val, double min, double max) {
        return Math.max(min, Math.min(max, val));
    }


//    public static class Matrix{
//        double _11, _12, _13;
//        double _21, _22, _23;
//        double _31, _32, _33;
//
//        Matrix()
//        {
//            _11=0.0; _12=0.0; _13=0.0;
//            _21=0.0; _22=0.0; _23=0.0;
//            _31=0.0; _32=0.0; _33=0.0;
//        }
//
//        //create an identity matrix
//        public static void identity(Matrix m_Matrix){
//            m_Matrix._11 = 1; m_Matrix._12 = 0; m_Matrix._13 = 0;
//
//            m_Matrix._21 = 0; m_Matrix._22 = 1; m_Matrix._23 = 0;
//
//            m_Matrix._31 = 0; m_Matrix._32 = 0; m_Matrix._33 = 1;
//        }
//
//
//        //create a transformation matrix
//     public static void translate(Matrix mat, double x, double y)
//        {
//            mat._11 = 1; mat._12 = 0; mat._13 = 0;
//
//            mat._21 = 0; mat._22 = 1; mat._23 = 0;
//
//            mat._31 = x;    mat._32 = y;    mat._33 = 1;
//
//            //and multiply
//            MatrixMultiply(mat);
//        }
//    }
//
//    public static Matrix MatrixMultiply(Matrix mIn)
//    {
//        Matrix mat_temp = new Matrix();
//
//        //first row
//        mat_temp._11 = (m_Matrix._11*mIn._11) + (m_Matrix._12*mIn._21) + (m_Matrix._13*mIn._31);
//        mat_temp._12 = (m_Matrix._11*mIn._12) + (m_Matrix._12*mIn._22) + (m_Matrix._13*mIn._32);
//        mat_temp._13 = (m_Matrix._11*mIn._13) + (m_Matrix._12*mIn._23) + (m_Matrix._13*mIn._33);
//
//        //second
//        mat_temp._21 = (m_Matrix._21*mIn._11) + (m_Matrix._22*mIn._21) + (m_Matrix._23*mIn._31);
//        mat_temp._22 = (m_Matrix._21*mIn._12) + (m_Matrix._22*mIn._22) + (m_Matrix._23*mIn._32);
//        mat_temp._23 = (m_Matrix._21*mIn._13) + (m_Matrix._22*mIn._23) + (m_Matrix._23*mIn._33);
//
//        //third
//        mat_temp._31 = (m_Matrix._31*mIn._11) + (m_Matrix._32*mIn._21) + (m_Matrix._33*mIn._31);
//        mat_temp._32 = (m_Matrix._31*mIn._12) + (m_Matrix._32*mIn._22) + (m_Matrix._33*mIn._32);
//        mat_temp._33 = (m_Matrix._31*mIn._13) + (m_Matrix._32*mIn._23) + (m_Matrix._33*mIn._33);
//
//        m_Matrix = mat_temp;
//    }

    public static int toInt(Object object) {
        if ((object instanceof Number)) {
            return ((Number) object).intValue();
        }

        try {
            return Integer.valueOf(object.toString()).intValue();
        } catch (Throwable ex) {
        }
        return -1;
    }

    public static boolean isInt(Object object) {
        return toInt(object) != -1;
    }

    public static float toFloat(Object object) {
        if ((object instanceof Number)) {
            return ((Number) object).floatValue();
        }

        try {
            return Float.valueOf(object.toString()).floatValue();
        } catch (Throwable ex) {
        }
        return -1.0F;
    }

    public static boolean isFloat(Object object) {
        return toFloat(object) != -1.0F;
    }

    public static double toDouble(Object object) {
        if ((object instanceof Number)) {
            return ((Number) object).doubleValue();
        }

        try {
            return Double.valueOf(object.toString()).doubleValue();
        } catch (Throwable ex) {
        }
        return -1.0D;
    }

    public static boolean isDouble(Object object) {
        return toDouble(object) != -1.0D;
    }

    public static long toLong(Object object) {
        if ((object instanceof Number)) {
            return ((Number) object).longValue();
        }

        try {
            return Long.valueOf(object.toString()).longValue();
        } catch (Throwable ex) {
        }
        return -1L;
    }

    public static boolean isLong(Object object) {
        return toLong(object) != -1L;
    }

    public static short toShort(Object object) {
        if ((object instanceof Number)) {
            return ((Number) object).shortValue();
        }

        try {
            return Short.valueOf(object.toString()).shortValue();
        } catch (Throwable ex) {
        }
        return -1;
    }

    public static boolean isShort(Object object) {
        return toShort(object) != -1;
    }

    public static byte toByte(Object object) {
        if ((object instanceof Number)) {
            return ((Number) object).byteValue();
        }

        try {
            return Byte.valueOf(object.toString()).byteValue();
        } catch (Throwable ex) {
        }
        return -1;
    }

    public static boolean isByte(Object object) {
        return toByte(object) != -1;
    }

    public static double roundNumDecimals(double d, int num) {
        StringBuilder format = new StringBuilder("#.");
        for (int i = 0; i < num; i++)
            format.append("#");
        DecimalFormat f = new DecimalFormat(format.toString());
        return toDouble(f.format(d));
    }



}
