package gl.mc.mistererwin.galaxion.utils;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.material.MaterialData;

/**
 * Created by mistererwin on 24.05.2015.
 * In case you need it, ask me ;)
 */
public class MyMaterial {
    private final boolean ignoreData;
    private final Material material;
    private final short data;

    public MyMaterial(Material material, short data, boolean ignoreData) {
        this.ignoreData = ignoreData;
        this.material = material;
        this.data = data;
    }

    public MyMaterial(Material material, short data) {
        this(material, data, false);
    }

    public MyMaterial(Material material, MaterialData data) {
        this(material, (short) data.getData(), false);
    }

    public MyMaterial(Material material) {
        this(material, (short) 0, true);
    }

    public final boolean matches(ItemStack item) {
        return (item.getType() == this.material) && ((this.ignoreData) || (item.getDurability() == this.data));
    }

    public final ItemStack newItemStack(int amount) {
        if (amount <= 0) {
            amount = 1;
        }
        return new ItemStack(this.material, amount, this.ignoreData ? 0 : this.data);
    }

    public final String getName() {
        return FormatUtil.getFriendlyName(this.material);
    }

    public final String serialize() {
        return this.material.name() + (!this.ignoreData ? ":" + this.data : "");
    }

    public String toString() {
        if (this.ignoreData) {
            return this.material.toString();
        }
        return "MyMaterial { material = " + this.material + ", data = " + this.data + " }";
    }

    public boolean equals(Object obj) {
        if ((obj instanceof MyMaterial)) {
            MyMaterial that = (MyMaterial) obj;
            return (this.material == that.material) && ((this.ignoreData) || (this.data == that.data));
        }

        return false;
    }

    public int hashCode() {
        int hash = 101;
        hash *= (1 + this.material.hashCode());
        hash *= (1 + (this.ignoreData ? this.data : 0));
        return hash;
    }

    public static MyMaterial fromString(String string) {
        string = string.replaceAll(" ", "");
        try {
            if (string.contains(":")) {
                String[] split = string.split(":");
                Material material = MyMaterial.getMaterial(split[0]);
                if (material != null) {
                    short data = MathUtils.toShort(split[1]);
                    boolean ignoreData = data == -1;
                    if (data <= 0) {
                        data = 0;
                    }
                    return new MyMaterial(material, data, ignoreData);
                }
            }

            Material material = MyMaterial.getMaterial(string);
            if (material != null)
                return new MyMaterial(material);
        } catch (Throwable ex) {
        }

        return null;
    }

    public boolean isIgnoreData() {
        return this.ignoreData;
    }

    public Material getMaterial() {
        return this.material;
    }

    public short getData() {
        return this.data;
    }

    public static final org.bukkit.Material getMaterial(String string)
    {
        org.bukkit.Material ret = matchMaterial(string);
        if ((ret == null) && (MathUtils.isInt(string))) {
            ret = getMaterial(MathUtils.toInt(string));
        }
        return ret;
    }

    public static final org.bukkit.Material getMaterial(int ID)
    {
        org.bukkit.Material ret = org.bukkit.Material.getMaterial(ID);
        if (ret == null) {
            return Material.AIR;
        }
        return ret;
    }



    private static final org.bukkit.Material matchMaterial(String string)
    {
        org.bukkit.Material material = null;
        try
        {
            material = org.bukkit.Material.matchMaterial(string);
        } catch (Throwable ex) {
        }
        if (material == null)
        {
            try
            {
                org.bukkit.Material internal = Bukkit.getUnsafe().getMaterialFromInternalName(string);
                if (internal != org.bukkit.Material.AIR)
                    material = internal;
            } catch (Throwable ex) {
            }
        }
        return material;
    }

}
