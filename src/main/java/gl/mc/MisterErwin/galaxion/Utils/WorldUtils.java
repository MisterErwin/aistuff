package gl.mc.mistererwin.galaxion.utils;

//import gl.mc.mistererwin.galaxion.api.entity.IBaseEntity;
//import gl.mc.mistererwin.galaxion.api.entity.IHigherEye;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.util.BlockIterator;
import org.bukkit.util.Vector;

public class WorldUtils {


	public static boolean isLOSOkay(Vector s, Vector b, World w) {

		Vector d = b.subtract(s);
		if (d.lengthSquared() == 0)
			return true;
		int l = Math.min(100,(int)d.length());
		BlockIterator it = new BlockIterator(w, s, d, 0d,
				l);
		while (it.hasNext()&&l-->0) {
			Block b2 = it.next();
			if (b2.getType().isSolid()) {
				return false;
			}
		}
		return true;
	}

	public static boolean isLOSOkay(Location a, Location b) {
		return isLOSOkay(a.toVector(), b.toVector(),b.getWorld());
	}

//	public static boolean isLOSOkay(Location a, IBaseEntity other) {
//		return (isLOSOkay(a.toVector(), other.getLocation().toVector(),other.getLocation().getWorld()) || (other instanceof IHigherEye && isLOSOkay(a, ((IHigherEye)other).getEyeLocation())));
//	}


	public static boolean isSecondInFOVOfFirst(Vector posFirst, Vector facingFirst, Vector posSecond, double fovH, double fovV){
		Vector toTarget = posSecond.subtract(posFirst).normalize();
		//Vector a: facingFirst
		//Vector b: toTarget
		double hypothenuseSq = toTarget.lengthSquared();

		double tTY = toTarget.getY();
		double fFY = facingFirst.getY();
		toTarget.setY(0);
		facingFirst.setY(0);
		if (facingFirst.dot(toTarget) < Math.cos(fovH/2))
			return false;
		//Vertical
		double gegenKathete = Math.abs(tTY-fFY);
		return (Math.asin(gegenKathete/Math.sqrt(hypothenuseSq)) < fovV/2);
	}
}
