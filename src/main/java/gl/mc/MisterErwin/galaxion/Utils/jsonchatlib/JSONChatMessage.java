package gl.mc.mistererwin.galaxion.utils.jsonchatlib;

import gl.mc.mistererwin.galaxion.AIStuff;
import net.minecraft.server.v1_8_R2.IChatBaseComponent;
import net.minecraft.server.v1_8_R2.PacketPlayOutChat;
import org.bukkit.craftbukkit.v1_8_R2.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.util.List;

public class JSONChatMessage {
	private JSONObject chatObject;

	public JSONChatMessage(String text, JSONChatColor color,
			List<JSONChatFormat> formats) {
		chatObject = new JSONObject();
		chatObject.put("text", text);
		if (color != null) {
			chatObject.put("color", color.getColorString());
		}
		if (formats != null) {
			for (JSONChatFormat format : formats) {
				chatObject.put(format.getFormatString(), true);
			}
		}
	}

	public void addExtra(JSONChatExtra extraObject) {
		if (!chatObject.containsKey("extra")) {
			chatObject.put("extra", new JSONArray());
		}
		JSONArray extra = (JSONArray) chatObject.get("extra");
		extra.add(extraObject.toJSON());
		chatObject.put("extra", extra);
	}

	public void sendToPlayer(Player player) {
		try {
			IChatBaseComponent comp = IChatBaseComponent.ChatSerializer.a(chatObject.toJSONString());
			PacketPlayOutChat packet = new PacketPlayOutChat(comp, (byte) 1);
			((CraftPlayer) player).getHandle().playerConnection.sendPacket(packet);
		}catch(NoClassDefFoundError e){
			AIStuff.instance.getLogger().warning("Wrong bukkit version  - can't send a JSON Message");
			StringBuilder sb = new StringBuilder(chatObject.get("text").toString());
			JSONArray get = (JSONArray) chatObject.get("extra");
			for (int i = 0, getSize = (get).size(); i < getSize; i++) {
				Object o = get.get(i);
				sb.append(o.toString());
			}
			player.sendMessage(sb.toString());
		}
	}

	@Override
	public String toString() {
		return chatObject.toJSONString();
	}

}