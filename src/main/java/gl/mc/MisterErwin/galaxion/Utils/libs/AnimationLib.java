package gl.mc.mistererwin.galaxion.utils.libs;

import gl.mc.mistererwin.galaxion.AIStuff;
import gl.mc.mistererwin.galaxion.components.animation.Animations;
import gl.mc.mistererwin.galaxion.components.character.Parted;
import gl.mc.mistererwin.galaxion.implementation.animations.Animation;
import gl.mc.mistererwin.galaxion.implementation.animations.PartTrack;
import gl.mc.mistererwin.galaxion.utils.math.Quaternion;
import gl.mc.mistererwin.galaxion.utils.math.Vector3f;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by mistererwin on 15.07.2015.
 * In case you need it, ask me ;)
 */
public class AnimationLib {

    private static Map<Integer, Map<String, Animation>> animationMap = new HashMap<>();

    static {
//
//        animationMap.put("walk", createWalk());
//        animationMap.put("duck", createDuck());
//        animationMap.put("standup", inverse(animationMap.get("duck"), "standup"));
    }

    public static Animation get(Parted parted, String key) {
        Map<String, Animation> map = animationMap.get(parted.hashCode());
        return (map != null) ? map.get(key) : null;
    }

    public static Animation get(Integer hashcode, String key) {
        Map<String, Animation> map = animationMap.get(hashcode);
        return (map != null) ? map.get(key) : null;
    }

    public static void loadAnimations(Parted parted, Animations animations) {
        File f = new File(AIStuff.instance.getDataFolder(), "animations");
        if (!f.isDirectory())
            f.mkdirs();
        File[] files = f.listFiles();
        if (files == null)
            return;
        for (File file : files) {
            if (file.getName().endsWith(".xml"))
                loadAniFromFile(file, parted);
        }
//        System.out.println(parted.hashCode());
//        System.out.println((animationMap.get(parted.hashCode()) != null) ? animationMap.get(parted.hashCode()).size() : "null");

        if (animations != null && animationMap!=null&&!animationMap.isEmpty() && animationMap.get(parted.hashCode())!=null)
            animations.animationMap.putAll(animationMap.get(parted.hashCode()));
    }

/*
    private static Animation createWalk() {
        Animation walk = new Animation("walk", 40);
        Quaternion[] rotations = new Quaternion[5];
        Vector3f[] translations = new Vector3f[5];
        Arrays.fill(translations, new Vector3f(0, 0, 0));

        float[] times = new float[]{0, 10, 20, 30, 40};
        //r-legU
        rotations[0] = new Quaternion(new float[]{0, 0, 0});
        rotations[1] = new Quaternion(new float[]{0.261799388f, 0, 0});
        rotations[2] = new Quaternion(new float[]{0, 0, 0});
        rotations[3] = new Quaternion(new float[]{-0.261799388f, 0, 0});
        rotations[4] = new Quaternion(new float[]{0, 0, 0});

        PartTrack track = new PartTrack(1, times, translations, rotations);
        walk.addTrack(track);
        //l-legU
        translations = new Vector3f[5];
        Arrays.fill(translations, new Vector3f(0, 0, 0));
        rotations = new Quaternion[5];
        rotations[0] = new Quaternion(new float[]{0, 0, 0});
        rotations[1] = new Quaternion(new float[]{-0.261799388f, 0, 0});
        rotations[2] = new Quaternion(new float[]{0, 0, 0});
        rotations[3] = new Quaternion(new float[]{0.261799388f, 0, 0});
        rotations[4] = new Quaternion(new float[]{0, 0, 0});

        track = new PartTrack(3, times, translations, rotations);
        walk.addTrack(track);

        return walk;
    }
*/

/*
    private static Animation createDuck() {
        Animation duck = new Animation("duck", 40);
        Quaternion[] rotations = new Quaternion[2];
        Vector3f[] translations = new Vector3f[2];

        Arrays.fill(translations, new Vector3f(0, 0, 0));

        float[] times = new float[]{0, 30};
        //leg-U
        rotations[0] = new Quaternion(new float[]{0, 0, 0});
        rotations[1] = new Quaternion(new float[]{-2 * 0.261799388f, 0, 0});

        PartTrack track = new PartTrack(1, times, translations, rotations);
        duck.addTrack(track);
        track = new PartTrack(3, times, translations, rotations);
        duck.addTrack(track);

        //legL
        translations = new Vector3f[2];
        Arrays.fill(translations, new Vector3f(0, 0, 0));
        rotations = new Quaternion[2];
        rotations[0] = new Quaternion(new float[]{0, 0, 0});
        rotations[1] = new Quaternion(new float[]{3 * 0.261799388f, 0, 0});

        track = new PartTrack(2, times, translations, rotations);
        duck.addTrack(track);
        track = new PartTrack(4, times, translations, rotations);
        duck.addTrack(track);

        //torso

        translations = new Vector3f[2];
        translations[0] = new Vector3f(0, 0, 0);
        translations[1] = new Vector3f(0, -0.2f, 0);

        rotations = new Quaternion[2];
        Arrays.fill(rotations, new Quaternion());

        track = new PartTrack(0, times, translations, rotations);
        duck.addTrack(track);

        return duck;
    }
*/


/*    private static Animation inverse(Animation a, String newName) {
        Animation animation = new Animation((newName != null) ? newName : (a.getName() + "_inv"), a.getLength());
        for (PartTrack pt : a.getTracks()) {
            Vector3f[] translations = pt.getTranslations().clone();
            Quaternion[] rotations = pt.getRotations().clone();
            Collections.reverse(Arrays.asList(translations));
            Collections.reverse(Arrays.asList(rotations));

            animation.addTrack(new PartTrack(pt.getTargetBoneIndex(), pt.getTimes(), translations, rotations));
        }

        return animation;
    }*/


    public static void loadAniFromFile(File file, Parted parted) {
        try {
            SAXParserFactory factory = SAXParserFactory.newInstance();
            SAXParser saxParser = factory.newSAXParser();
            AniSaxHandler aniSaxHandler = new AniSaxHandler(parted);
            saxParser.parse(file, aniSaxHandler);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    private static class AniSaxHandler extends DefaultHandler {
        private AniBuilder aniBuilder;
        private TrackBuilder trackBuilder;
        private KeyframeBuilder keyframeBuilder;
        private Parted parted;
        private boolean variables = false;
        private String variableName;
        private StringBuffer variable = null;

        public AniSaxHandler(Parted parted) {
            this.parted = parted;
        }

        public void startElement(String uri, String localName, String qName,
                                 Attributes attributes) throws SAXException {
            if (variables) {
                variable = new StringBuffer();
                variableName = qName;
            } else if (qName.equalsIgnoreCase("variables"))
                variables = true;
            else if (aniBuilder == null && qName.equalsIgnoreCase("animation")) {
                aniBuilder = new AniBuilder(getFloat(attributes.getValue("length")), attributes.getValue("name"));
            } else if (trackBuilder != null) {
                if (keyframeBuilder == null && qName.equalsIgnoreCase("keyframe")) {
                    keyframeBuilder = new KeyframeBuilder(Float.valueOf(attributes.getValue("time")));
                } else {
                    if (qName.equalsIgnoreCase("translate"))
                        keyframeBuilder.translation = new Vector3f(getFloat(attributes.getValue("x")), getFloat(attributes.getValue("y")), getFloat(attributes.getValue("z")));
                    else if (qName.equalsIgnoreCase("rotate"))
                        keyframeBuilder.rotation = new Quaternion(new float[]{getFloat(attributes.getValue("x")), getFloat(attributes.getValue("y")), getFloat(attributes.getValue("z"))});

                }
            } else if (aniBuilder != null && qName.equalsIgnoreCase("track")) {
                int partID = -2;
                if (attributes.getValue("part") != null)
                    partID = parted.getPartIndex(attributes.getValue("part"));
                else if (attributes.getValue("partindex") != null)
                    partID = Integer.valueOf(attributes.getValue("partindex"));
                if (partID == -2)
                    throw new SAXException("[part] or [partindex] missing on track!");
                else if (partID==-1)
                    throw new SAXException("part not found on parted!");

                trackBuilder = new TrackBuilder(partID);
            }
        }

        public void endElement(String uri, String localName, String qName)
                throws SAXException {
//            System.out.println("<<" + qName + "||" + ((aniBuilder!=null)?"aniBuilder":"anull")+ "||" + ((trackBuilder!=null)?"TrackBuilder":"tnull")+ "||" + ((keyframeBuilder!=null)?"keyframeBuilder":"knull"));
            if (variables && variableName!=null) {
                floatVariables.put(variableName, Float.parseFloat(variable.toString()));
                variable = null;
                variableName = null;
            } else if (qName.equalsIgnoreCase("variables")) {
                variables = false;
            } else if (trackBuilder != null) {
                if (keyframeBuilder != null && qName.equalsIgnoreCase("keyframe")) {
                    trackBuilder.times.add(keyframeBuilder.time);
                    trackBuilder.translations.add(keyframeBuilder.translation);
                    trackBuilder.rotations.add(keyframeBuilder.rotation);
                    keyframeBuilder = null;
                } else if (qName.equalsIgnoreCase("track")) {
                    int size = trackBuilder.times.size();
                    if (size != trackBuilder.rotations.size() || size != trackBuilder.translations.size())
                        throw new SAXException("Sizes do not match for track:" + trackBuilder.targetBoneIndex + "(" + parted.getPart(trackBuilder.targetBoneIndex).name + ")");
                    float[] times = new float[size];
                    for (int i = 0; i < size; i++)
                        times[i] = trackBuilder.times.get(i);
                    aniBuilder.trackList.add(new PartTrack(trackBuilder.targetBoneIndex,
                            times,
                            trackBuilder.translations.toArray(new Vector3f[size]),
                            trackBuilder.rotations.toArray(new Quaternion[size])));
                    trackBuilder = null;
                }
            } else if (aniBuilder != null && qName.equalsIgnoreCase("animation")) {
                int hashcode = parted.hashCode();
//                System.out.println(hashcode + "closing");
                Animation animation = get(hashcode, aniBuilder.name);
                if (animation == null)
                    animation = new Animation(aniBuilder.name, aniBuilder.length);
                animation.getTracksList().clear();
                animation.setTracks(aniBuilder.trackList.toArray(new PartTrack[aniBuilder.trackList.size()]));
                Map<String, Animation> mmap = animationMap.get(hashcode);
                if (mmap == null) {
                    mmap = new HashMap<>();
//                    System.out.println("created map for " + hashcode);
                }
                mmap.put(animation.getName(), animation);
//                System.out.println("stored " + animation.getName() + " under hash " + hashcode);
                animationMap.put(hashcode, mmap);

                aniBuilder = null;
            }

        }

        @Override
        public void characters(char[] ch, int start, int length) throws SAXException {
            if (variable != null)
                variable.append(new String(ch, start, length));
        }

        private Map<String, Float> floatVariables = new HashMap<>();

        private Float toRadian(String s){
            return s.contains("�")?(float)Math.toRadians(Float.parseFloat(s.replaceAll("[^\\d.-]", ""))):Float.parseFloat(s);
        }

        private Float getFloat(String s) {
            if (s.contains("$")) {
                Matcher matcher = varPattern.matcher(s);
                if (matcher.matches()) {
                    String a = matcher.group(1),
                            b = matcher.group(2);
                    Float ret;
                    if (a == null)
                        ret = floatVariables.get(b);
                    else
                        ret = floatVariables.get(b) * (a.equals("-") ? -1 : Float.parseFloat(a));

                    if (ret == null)
                        throw new RuntimeException("Variable " + s + " not declared!");
                    return ret;
                }
            }
            return toRadian(s);
        }

    }

    private static Pattern varPattern = Pattern.compile("([-\\d\\.]+)?\\*?\\$(\\w*)");


    private static class AniBuilder {
        public String name;
        public float length;
        public List<PartTrack> trackList;

        public AniBuilder(float length, String name) {
            this.length = length;
            this.name = name;
            trackList = new ArrayList<>();
        }
    }

    public static class TrackBuilder {
        public int targetBoneIndex;
        public List<Float> times;
        public List<Vector3f> translations;
        public List<Quaternion> rotations;

        public TrackBuilder(int targetBoneIndex) {
            this.targetBoneIndex = targetBoneIndex;
            times = new ArrayList<>();
            translations = new ArrayList<>();
            rotations = new ArrayList<>();
        }
    }

    public static class KeyframeBuilder {
        public Vector3f translation = new Vector3f();
        public Quaternion rotation = new Quaternion();
        public float time;

        public KeyframeBuilder(float time) {
            this.time = time;
        }
    }


}
