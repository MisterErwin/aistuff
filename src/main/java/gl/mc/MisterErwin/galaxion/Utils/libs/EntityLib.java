package gl.mc.mistererwin.galaxion.utils.libs;

import com.artemis.Entity;
import com.artemis.utils.EntityBuilder;
import gl.mc.mistererwin.galaxion.AIStuff;
import gl.mc.mistererwin.galaxion.components.ai.*;
import gl.mc.mistererwin.galaxion.components.animation.AS_Animatable;
import gl.mc.mistererwin.galaxion.components.animation.AS_Animations;
import gl.mc.mistererwin.galaxion.components.animation.Animated;
import gl.mc.mistererwin.galaxion.components.animation.Animations;
import gl.mc.mistererwin.galaxion.components.character.AniPart;
import gl.mc.mistererwin.galaxion.components.character.Parted;
import gl.mc.mistererwin.galaxion.components.combat.Aim;
import gl.mc.mistererwin.galaxion.components.combat.Health;
import gl.mc.mistererwin.galaxion.components.combat.WeaponHolder;
import gl.mc.mistererwin.galaxion.components.generic.EntityLocated;
import gl.mc.mistererwin.galaxion.components.generic.FOV;
import gl.mc.mistererwin.galaxion.components.generic.Located;
import gl.mc.mistererwin.galaxion.components.generic.PlayerEntity;
import gl.mc.mistererwin.galaxion.components.web.WatchableEntity;
import gl.mc.mistererwin.galaxion.implementation.animations.AnimChannel;
import gl.mc.mistererwin.galaxion.implementation.generic.Fallable;
import gl.mc.mistererwin.galaxion.scenarios.IScenario;
import gl.mc.mistererwin.galaxion.systems.physics.PartSystem;
import gl.mc.mistererwin.galaxion.utils.math.Vector3f;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.metadata.FixedMetadataValue;

import java.util.*;

/**
 * Created by mistererwin on 23.05.2015.
 * In case you need it, ask me ;)
 */
public class EntityLib {

    public static void addDummy(Location loc, IScenario is) {
        ArmorStand as = (ArmorStand) loc.getWorld().spawnEntity(loc, EntityType.ARMOR_STAND);
        as.setArms(true);
        as.setBasePlate(false);
        as.setHelmet(new ItemStack(Material.WOOL, 1, (byte) 14));
        as.setMetadata("scenarioName", new FixedMetadataValue(AIStuff.instance, is.getUniqueName()));
        Entity e = new EntityBuilder(is.getWorld()).with(new AS_Animations(), AS_Animatable.forArmorStand(as), new ArmorStandEntitiy(as)
                , new FOV(), new Health(20, 20), new Located(as.getLocation()), new EntityLocated(as), new WatchableEntity("dummy")).tag("Dummy").group("dummy").UUID(as.getUniqueId()).build();
        AS_Animations.addAnimation(e.getComponent(AS_Animations.class), "idle");
//        e.getComponent(AI.class).initialize(e);
    }


    public static void addAS(Location loc, IScenario is) {
        ArmorStand as = (ArmorStand) loc.getWorld().spawnEntity(loc, EntityType.ARMOR_STAND);
        as.setArms(true);
        as.setBasePlate(false);
        as.setHelmet(new ItemStack(Material.GOLD_HELMET));

        as.setMetadata("scenarioName", new FixedMetadataValue(AIStuff.instance, is.getUniqueName()));
        Entity e = new EntityBuilder(is.getWorld()).with(new AI(), new AS_Animations(), AS_Animatable.forArmorStand(as), new ArmorStandEntitiy(as)
                , new FOV(), new Health(20, 20), new Jump(), new Moveable(0.2, 0.2, 0.1, 0.4),
                new Path(), new Turn(), new Located(as.getLocation()), new EntityLocated(as), new WatchableEntity("as")).tag("ArmorStand").group("aiA").UUID(as.getUniqueId()).build();
        AS_Animations.addAnimation(e.getComponent(AS_Animations.class), "idle");
        e.getComponent(AI.class).initialize(e);
    }

    public static void addRigid(Location loc, IScenario is) {
        loc = loc.clone().add(0, 0.9, 0);


        Animations animations = new Animations();
        Parted parted = getHumanRoot();

        EntityBuilder entityBuilder = new EntityBuilder(is.getWorld());
        entityBuilder.with(parted, animations, new Animated(), new Located(loc), new WatchableEntity("ps"), new Fallable());
        entityBuilder.tag("Parts");
        entityBuilder.UUID(UUID.randomUUID());
        Entity e = entityBuilder.build();


        PartSystem ps = is.getWorld().getSystem(PartSystem.class);
//        for (Map.Entry<String,Animation> entry : AnimationLib.entrySet())
//            ps.addAnim(entry.getValue(),animations);
        AnimationLib.loadAnimations(parted,animations);
//        ps.addAnim(AnimationLib.get("walk"),animations);
//        ps.addAnim(AnimationLib.get("duck"),animations);

        AnimChannel channel = ps.createChannel(e);
//        channel.setAnim("walk",animations);
//        channel.setLoopMode(LoopMode.Loop);
    }

    //    private final static Quaternion inverseRot = new Quaternion(new float[]{0,0,FastMath.PI});
//    private final static boolean inverseRot = true;

    public static Parted getHumanRoot() {
        Parted p = new Parted();
        p.rootPart = new AniPart("torso", null, new AniPart[5], new ItemStack(Material.WOOL, 1, (byte) 2));
        ItemStack is = new ItemStack(Material.SKULL_ITEM, 1, (byte) 3);
        SkullMeta sm = (SkullMeta) is.getItemMeta();
        sm.setOwner("Notch");
        is.setItemMeta(sm);
        p.rootPart.children[0] = new AniPart("head", p.rootPart, null, is, new Vector3f(0, .45f, 0)); //Head
        p.rootPart.children[1] = new AniPart("r-armU", p.rootPart, new AniPart[1], new ItemStack(Material.WOOL, 1, (byte) 3), new Vector3f(-.35f, .5f, 0)); //L-Arm-Upper
        p.rootPart.children[2] = new AniPart("l-armU", p.rootPart, new AniPart[1], new ItemStack(Material.WOOL, 1, (byte) 5), new Vector3f(.35f, .5f, 0)); //R-Arm
        p.rootPart.children[3] = new AniPart("r-legU", p.rootPart, new AniPart[1], new ItemStack(Material.WOOL, 1, (byte) 5), new Vector3f(-.1f, -.2f, 0)); //L-Leg
        p.rootPart.children[4] = new AniPart("l-legU", p.rootPart, new AniPart[1], new ItemStack(Material.WOOL, 1, (byte) 5), new Vector3f(.1f, -.2f, 0)); //R-Leg

        p.rootPart.children[1].children[0] = new AniPart("r-armL", p.rootPart.children[1], new AniPart[1], new ItemStack(Material.WOOL, 1, (byte) 4), new Vector3f(0, -.33f, 0));
        p.rootPart.children[2].children[0] = new AniPart("l-armL", p.rootPart.children[2], null, new ItemStack(Material.WOOL, 1, (byte) 4), new Vector3f(0, -.33f, 0));
        p.rootPart.children[3].children[0] = new AniPart("r-legL", p.rootPart.children[3], null, new ItemStack(Material.WOOL, 1, (byte) 6), new Vector3f(0, -.33f, 0));
        p.rootPart.children[4].children[0] = new AniPart("l-legL", p.rootPart.children[4], null, new ItemStack(Material.WOOL, 1, (byte) 6), new Vector3f(0, -.33f, 0));


        p.rootPart.children[1].children[0].children[0] = new AniPart("r-hand", p.rootPart.children[1].children[0],null, new ItemStack(Material.BRICK), new Vector3f(0,-.33f,0));

        List<AniPart> aniParts = new ArrayList<>();
        Stack<AniPart> toVisit = new Stack<>();
        AniPart ap;
        toVisit.add(p.rootPart);
        while (!toVisit.isEmpty()){
            ap = toVisit.pop();
            aniParts.add(ap);
            if (ap.children!=null)
                Collections.addAll(toVisit, ap.children);
        }
        p.partList = aniParts.toArray(new AniPart[aniParts.size()]);
        /*
        0>>torso
        1>>r-legU
        2>>r-legL
        3>>l-legU
        4>>l-legL
        5>>r-armU
        6>>r-armL
        7>>l-armU
        8>>l-armL
        9>>head
         */
//        for (int i=0,l=p.partList.length;i<l;i++)
//            System.out.println(i + ">>" + p.partList[i].name);
        return p;
    }

    public static void addPlayer(Player p, IScenario is) {
        p.spigot().setCollidesWithEntities(true);
        p.setMetadata("scenarioName", new FixedMetadataValue(AIStuff.instance, is.getUniqueName()));

        Entity e = new EntityBuilder(is.getWorld()).with(
                new Health(20, 20), new WeaponHolder(), new WatchableEntity("player"), new Located(p.getLocation()), new Aim(), new PlayerEntity(), new EntityLocated(p)).player(p.getName())
                .UUID(p.getUniqueId()).build();
    }
}
