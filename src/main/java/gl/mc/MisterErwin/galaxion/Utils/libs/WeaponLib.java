package gl.mc.mistererwin.galaxion.utils.libs;

import gl.mc.mistererwin.galaxion.api.weapons.SoundData;
import gl.mc.mistererwin.galaxion.implementation.weapons.WeaponStats;
import gl.mc.mistererwin.galaxion.utils.MyMaterial;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;

/**
 * Created by mistererwin on 24.05.2015.
 * In case you need it, ask me ;)
 */
public class WeaponLib {

    public static WeaponStats getShotgun(){
        WeaponStats ws = new WeaponStats("shotgun");
        ws.gunName = ChatColor.YELLOW + "Shotgun";
        ws.gun = new MyMaterial(Material.STONE_HOE);
        ws.amm = new MyMaterial(Material.SEEDS);
        ws.ammoAmtNeeded = 1;
        ws.bulletDelayTime = 28;
        ws.reloadType = "PUMP";
        ws.roundsPerBurst = 1;
        ws.bulletsPerClick = 7;
        ws.gunDamage = 1;
        ws.armorPenetration = 1;
        ws.maxDistance = 64;
        ws.bulletSpeed = 3;
        ws.accuracy = 0.2;
        ws.recoil = 1;
        ws.knockback = 1;
        ws.canAimRight = false;
        ws.explodeRadius = 0;
        ws.explosionDamage = 0;
        ws.fireRadius = 0;
        ws.canHeadShot = false;
        ws.canClickLeft = false;
        ws.canClickRight = true;
        ws.outOfAmmoMessage = ChatColor.translateAlternateColorCodes('&',"&6This gun needs &cSEEDS");
        ws.projType = "SNOWBALL";
        ws.firedSound = new SoundData[]{new SoundData(Sound.GHAST_FIREBALL, 20, 20, 0)};
        ws.hasSmokeTrail = false;
        ws.hasClip = true;
        ws.maxClipSize = 64;
        ws.reloadGunOnDrop = true;
        ws.reloadTime = 40;
        return ws;
    }

    public static WeaponStats getRifle(){
        WeaponStats ws = new WeaponStats("rifle");
        ws.gunName = ChatColor.YELLOW + "Rifle";
        ws.gun = new MyMaterial(Material.IRON_HOE);
        ws.amm = new MyMaterial(Material.CLAY_BALL);
        ws.ammoAmtNeeded = 1;
        ws.bulletDelayTime = 24;
        ws.reloadType = "NORMAL";
        ws.roundsPerBurst = 3;
        ws.bulletsPerClick = 1;
        ws.gunDamage = 2;
        ws.armorPenetration = 2;
        ws.maxDistance = 128;
        ws.bulletSpeed = 4;
        ws.accuracy = 0.08;
        ws.recoil = 0;
        ws.knockback = 4; //1;
        ws.canAimRight = false;
        ws.canAimLeft = true;
        ws.explodeRadius = 0;
        ws.explosionDamage = 0;
        ws.fireRadius = 0;
        ws.canHeadShot = true;
        ws.canClickLeft = true;
        ws.canClickRight = true;
        ws.outOfAmmoMessage = ChatColor.translateAlternateColorCodes('&',"&6This gun needs &cClayBalls");
        ws.projType = "SNOWBALL";
        ws.firedSound = new SoundData[]{new SoundData(Sound.NOTE_SNARE_DRUM, 20, 20, 0), new SoundData(Sound.ITEM_BREAK, 20, 20, 0),new SoundData(Sound.GHAST_FIREBALL, 20, 20, 0)};
        ws.hasSmokeTrail = false;
        ws.hasClip = true;
        ws.maxClipSize = 20;
        ws.reloadGunOnDrop = true;
        ws.reloadTime = 70;
        return ws;
    }

    public static WeaponStats getOP(){
        WeaponStats ws = new WeaponStats("op");
        ws.gunName = ChatColor.YELLOW + "Not OP At ALL";
        ws.gun = new MyMaterial(Material.DIAMOND_HOE);
        ws.amm = new MyMaterial(Material.CLAY_BALL);
        ws.ammoAmtNeeded = 0;
        ws.bulletDelayTime = 24;
        ws.reloadType = "NORMAL";
        ws.roundsPerBurst = 20;
        ws.bulletsPerClick = 1;
        ws.gunDamage = 2;
        ws.armorPenetration = 2;
        ws.maxDistance = 128;
        ws.bulletSpeed = 4;
        ws.accuracy = 0;
        ws.recoil = 0;
        ws.knockback = 2;
        ws.explodeRadius = 0;
        ws.explosionDamage = 0;
        ws.fireRadius = 0;
        ws.canHeadShot = true;
        ws.canClickRight = true;
        ws.outOfAmmoMessage = ChatColor.translateAlternateColorCodes('&',"&6This gun needs &cClayBalls");
        ws.projType = "SNOWBALL";
        ws.firedSound = new SoundData[]{new SoundData(Sound.NOTE_SNARE_DRUM, 20, 20, 0), new SoundData(Sound.ITEM_BREAK, 20, 20, 0),new SoundData(Sound.GHAST_FIREBALL, 20, 20, 0)};
        ws.hasSmokeTrail = false;
        ws.hasClip = true;
        ws.maxClipSize = 64;
        ws.reloadGunOnDrop = true;
        ws.reloadTime = 1;
        return ws;
    }

}
