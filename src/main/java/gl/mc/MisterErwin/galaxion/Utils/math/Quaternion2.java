package gl.mc.mistererwin.galaxion.utils.math;

import gl.mc.mistererwin.galaxion.utils.MathUtils;
import org.bukkit.util.EulerAngle;

/**
 * Holds a three degree of freedom orientation.
 * <p>
 * Quaternions have
 * several mathematical properties that make them useful for
 * representing orientations, but require four items of data to
 * hold the three degrees of freedom. These four items of data can
 * be viewed as the coefficients of a complex number with three
 * imaginary parts. The mathematics of the quaternion is then
 * defined and is roughly correspondent to the math of 3D
 * rotations. A quaternion is only a valid rotation if it is
 * normalised: i.e. it has a length of 1.
 *
 * @note Angular velocity and acceleration can be correctly
 * represented as vectors. Quaternions are only needed for
 * orientation.
 */
public class Quaternion2 {
    /**
     * Holds the real component of the quaternion.
     */
    public double r;

    /**
     * Holds the first complex component of the
     * quaternion.
     */
    public double i;

    /**
     * Holds the second complex component of the
     * quaternion.
     */
    public double j;

    /**
     * Holds the third complex component of the
     * quaternion.
     */
    public double k;


    /**
     * The default constructor creates a quaternion representing
     * a zero rotation.
     */
    public Quaternion2() {
        this(1, 0, 0, 0);
    }


    /**
     * The explicit constructor creates a quaternion with the given
     * components.
     *
     * @param r The real component of the rigid body's orientation
     *          quaternion.
     * @param i The first complex component of the rigid body's
     *          orientation quaternion.
     * @param j The second complex component of the rigid body's
     *          orientation quaternion.
     * @param k The third complex component of the rigid body's
     *          orientation quaternion.
     * @note The given orientation does not need to be normalised,
     * and can be zero. This function will not alter the given
     * values, or normalise the quaternion. To normalise the
     * quaternion (and make a zero quaternion a legal rotation),
     * use the normalise function.
     * @see Quaternion2#normalize()
     */

    public Quaternion2(double r, double i, double j, double k) {
        this.r = r;
        this.i = i;
        this.j = j;
        this.k = k;
    }


    /**
     * Normalises the quaternion to unit length, making it a valid
     * orientation quaternion.
     */
    public void normalize() {
        double d = r * r + i * i + j * j + k * k;

        // Check for zero length quaternion, and use the no-rotation
        // quaternion in that case.
        if (d < MathUtils.double_epsilon) {
            r = 1;
            return;
        }

        d = 1.0 / Math.sqrt(d);
        r *= d;
        i *= d;
        j *= d;
        k *= d;
    }

    /**
     * Multiplies the quaternion by the given quaternion.
     *
     * @param multiplier The quaternion by which to multiply.
     */
    public void mult(Quaternion2 multiplier)

    {
        double tr = r, ti = i, tj = j, tk = k;
        r = tr * multiplier.r - ti * multiplier.i -
                tj * multiplier.j - tk * multiplier.k;
        i = tr * multiplier.i + ti * multiplier.r +
                tj * multiplier.k - tk * multiplier.j;
        j = tr * multiplier.j + tj * multiplier.r +
                tk * multiplier.i - ti * multiplier.k;
        k = tr * multiplier.k + tk * multiplier.r +
                ti * multiplier.j - tj * multiplier.i;
    }

    /**
     * Adds the given vector to this, scaled by the given amount.
     * This is used to update the orientation quaternion by a rotation
     * and time.
     *
     * @param vector The vector to add.
     * @param scale  The amount of the vector to add.
     */
    public void addScaledVector(Vector3 vector, double scale) {

        Quaternion2 q = new Quaternion2(0,
                vector.x * scale,
                vector.y * scale,
                vector.z * scale);
        q.mult(this);
        r += q.r * 0.5;
        i += q.i * 0.5;
        j += q.j * 0.5;
        k += q.k * 0.5;
    }

    public void rotateByVector(Vector3 vector) {
        Quaternion2 q = new Quaternion2(0, vector.x, vector.y, vector.z);
        this.mult(q);
    }


//    public EulerAngle toEuler() {
//        double roll, pitch, yaw;
//        double test = i * j + k * r;
//        if (test > 0.499) { // singularity at north pole
//            pitch = 2 * Math.atan2(i, r);
//            yaw = Math.PI / 2;
//            roll = 0;
//            return new EulerAngle(roll, pitch, yaw);
//        }
//        if (test < -0.499) { // singularity at south pole
//            pitch = -2 * Math.atan2(i, r);
//            yaw = -Math.PI / 2;
//            roll = 0;
//            return new EulerAngle(roll, pitch, yaw);
//        }
//        double sqx = i * i;
//        double sqy = j * j;
//        double sqz = k * k;
//        pitch = Math.atan2(2 * j * r - 2 * i * k, 1 - 2 * sqy - 2 * sqz);
//        yaw = Math.asin(2 * test);
//        roll = Math.atan2(2 * i * r - 2 * j * k, 1 - 2 * sqx - 2 * sqz);
//        return new EulerAngle(roll, pitch, yaw);
//    }

    public EulerAngle toEuler() {
        double roll, pitch, yaw;


        double sqw = r * r;
        double sqx = i * i;
        double sqy = j * j;
        double sqz = k * k;
        double unit = sqx + sqy + sqz + sqw; // if normalised is one, otherwise is correction factor
        double test = i * j + k * r;
        if (test > 0.499 * unit) { // singularity at north pole
            pitch = 2 * Math.atan2(i, r);
            yaw = Math.PI / 2;
            roll = 0;
            return new EulerAngle(roll, pitch, yaw);

        }
        if (test < -0.499 * unit) { // singularity at south pole
            pitch = 2 * Math.atan2(i, r);
            yaw = -Math.PI / 2;
            roll = 0;
            return new EulerAngle(roll, pitch, yaw);

        }
        pitch = Math.atan2(2 * j * r - 2 * i * k, sqx - sqy - sqz + sqw);
        yaw = Math.asin(2 * test / unit);
        roll = Math.atan2(2 * i * r - 2 * j * k, -sqx + sqy - sqz + sqw);
        return new EulerAngle(roll, pitch, yaw);
    }

}
