package gl.mc.mistererwin.galaxion.utils.math;


/**
 * Holds a vector in 3 dimensions. Four data members are allocated
 * to ensure alignment in an array.
 *
 * @note This class contains a lot of inline methods for basic
 * mathematics. The implementations are included in the header
 * file.
 */
public class Vector3 {
    public double x, y, z;

    public Vector3() {
        this(0,0,0);
    }


        public Vector3(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }


    /**
     * Adds the given vector to this
     */
    public void add(Vector3 vector) {
        x += vector.x ;
        y += vector.y ;
        z += vector.z ;
    }

    /**
     * Subtracts the given vector to this
     */
    public void sub(Vector3 vector) {
        x -= vector.x ;
        y -= vector.y ;
        z -= vector.z ;
    }

    /**
     * Multiplies this vector by the given scalar.
     */
    public void mult(double val) {
        x *= val;
        y *= val;
        z *= val;
    }

    /**
     * Returns a copy of this vector scaled the given value.
     */
    public Vector3 multCopy(double value) {
        return new Vector3(x * value, y * value, z * value);
    }

    /**
     * Calculates and returns a component-wise product of this
     * vector with the given vector.
     */
    public Vector3 componentProduct(Vector3 vector) {
        return new Vector3(x * vector.x, y * vector.y, z * vector.z);
    }

    /**
     * Performs a component-wise product with the given vector and
     * sets this vector to its result.
     */
    public void componentProductUpdate(Vector3 vector) {
        x *= vector.x;
        y *= vector.y;
        z *= vector.z;
    }

    /**
     * Calculates and returns the vector product of this vector
     * with the given vector.
     */
    public Vector3 vectorProductCopy(Vector3 vector) {
        return new Vector3(y * vector.z - z * vector.y,
                z * vector.x - x * vector.z,
                x * vector.y - y * vector.x);
    }

    /**
     * Updates this vector to be the vector product of its current
     * value and the given vector.
     */
    public void vectorProduct(Vector3 vector) {
        this.x = y * vector.z - z * vector.y;
        this.y =
                z * vector.x - x * vector.z;
        this.z =
                x * vector.y - y * vector.x;
    }


    /**
     * Calculates and returns the scalar product of this vector
     * with the given vector.
     */
    public double scalarProduct(Vector3 vector) {
        return x * vector.x + y * vector.y + z * vector.z;
    }


    /**
     * Adds the given vector to this, scaled by the given amount.
     */
    public void addScaledVector(Vector3 vector, double scale) {
        x += vector.x * scale;
        y += vector.y * scale;
        z += vector.z * scale;
    }



    /**
     * Gets the magnitude of this vector.
     */
    public double magnitude() {
        return Math.sqrt(x * x + y * y + z * z);
    }

    /**
     * Gets the squared magnitude of this vector.
     */
    public double squareMagnitude() {
        return x * x + y * y + z * z;
    }

    /**
     * Limits the size of the vector to the given maximum.
     */
    public void trim(double size) {
        if (squareMagnitude() > size * size) {
            normalise();
            x *= size;
            y *= size;
            z *= size;
        }
    }


    /**
     * Turns a non-zero vector into a vector of unit length.
     */
    public void normalise() {
        double l = magnitude();
        if (l > 0)
            this.mult(1 / l);

    }

    /**
     * Returns the normalised version of a vector.
     */
    public Vector3 unitCopy() {
        double l = magnitude();
        if (l > 0)
            return this.multCopy(1 / l);
        else
            throw new IllegalArgumentException("Divide by zero");
    }

    /**
     * Checks if the two vectors have identical components.
     */
    public boolean equals(Vector3 other) {
        return x == other.x &&
                y == other.y &&
                z == other.z;
    }


    /**
     * Checks if this vector is component-by-component less than
     * the other.
     *
     * @note This does not behave like a single-value comparison:
     * !(a < b) does not imply (b >= a).
     */
    public boolean isLess(Vector3 other) {
        return x < other.x && y < other.y && z < other.z;
    }


    /**
     * Checks if this vector is component-by-component more than
     * the other.
     *
     * @note This does not behave like a single-value comparison:
     * !(a < b) does not imply (b >= a).
     */
    public boolean isMore(Vector3 other) {
        return x > other.x && y > other.y && z > other.z;
    }

    /**
     * Checks if this vector is component-by-component less than or equals
     * the other.
     *
     * @note This does not behave like a single-value comparison:
     * !(a <= b) does not imply (b > a).
     */
    public boolean isLessOrEquals(Vector3 other) {

        return x <= other.x && y <= other.y && z <= other.z;
    }

    /**
     * Checks if this vector is component-by-component more than or equals
     * the other.
     *
     * @note This does not behave like a single-value comparison:
     * !(a <= b) does not imply (b > a).
     */

    public boolean isMoreOrEquals(Vector3 other) {
        return x >= other.x && y >= other.y && z >= other.z;
    }

    /**
     * Zero all the components of the vector.
     */
    public void clear() {
        x = y = z = 0;
    }

    /**
     * Flips all the components of the vector.
     */
    public void invert() {
        x = -x;
        y = -y;
        z = -z;
    }


    public Vector3 clone() {
        return new Vector3(x, y, z);
    }

}
