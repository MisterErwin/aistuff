package gl.mc.mistererwin.galaxion.api;

//import gl.mc.mistererwin.galaxion.api.entity.IBaseEntity;

/**
 * Created by mistererwin on 01.05.2015.
 * In case you need it, ask me ;)
 */
public interface IEvent {

    long getDispatchTime();

    String getType();

    default Object getExtraInfo(){return null;}
//
//    public interface IEventDispatcher extends IBaseEntity {
//        public default void callEvent(IEvent event){
////            AIStuff.instance.getEntityRegistry().getAIEntities().forEach(new Consumer<IBaseEntity>() {
////                public void accept(IBaseEntity element) {
////                    if (element instanceof IEventReceiver && element.getWorld().equals(getWorld()))
////                        ((IEventReceiver)element).handleEvent(event);
////                }
////            });
//        }
//
//        public default void callRangedEvent(IEvent event, double rangeSquared){
////            AIStuff.instance.getEntityRegistry().getAIEntities().forEach(new Consumer<IBaseEntity>() {
////                public void accept(IBaseEntity element) {
////                    if (element instanceof IEventReceiver && element.getWorld().equals(getWorld()) && element.getLocation().distanceSquared(getLocation())<=rangeSquared)
////                        ((IEventReceiver)element).handleEvent(event);
////                }
////            });
//        }
//    }
//
//    public interface IEventReceiver{
//        public boolean handleEvent(IEvent event);
//    }
}
