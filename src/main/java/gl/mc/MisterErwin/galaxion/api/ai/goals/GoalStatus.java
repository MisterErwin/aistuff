package gl.mc.mistererwin.galaxion.api.ai.goals;

/**
 * Created by mistererwin on 03.05.2015.
 */
public enum GoalStatus {
    active, inactive, completed, failed
}