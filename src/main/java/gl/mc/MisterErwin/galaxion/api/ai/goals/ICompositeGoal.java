package gl.mc.mistererwin.galaxion.api.ai.goals;

import gl.mc.mistererwin.galaxion.api.IEvent;

/**
 * Created by mistererwin on 03.05.2015.
 */
public interface ICompositeGoal extends IGoal {


    void removeAllSubgoals();

    /**
     * This class should overwrite the super method so no exception gets thrown
     *
     * @see IGoal#addSubgoal(IGoal) ;
     */
    void addSubgoal(IGoal goal);

    // ---------------- ForwardMessageToFrontMostSubgoal
    // ---------------------------
    //
    // passes the message to the goal at the front of the queue
    // -----------------------------------------------------------------------------

    /**
     * passes the message to the goal at the front of the queue
     *
     * @param event
     * @return
     */
    boolean forwardEventToFrontMostSubgoal(IEvent event);


    /**
     * @return The first Goal of the queue or null
     */
    IGoal getFrontGoal();


}