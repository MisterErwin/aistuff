package gl.mc.mistererwin.galaxion.api.ai.goals;

import com.artemis.Entity;
import gl.mc.mistererwin.galaxion.api.IEvent;

public interface IGoal {

	// logic to run when the goal is activated.
	void activate();

	// logic to run each update-step
	GoalStatus process();

	// logic to run when the goal is satisfied. (typically used to switch
	// off any active steering behaviors)
	void terminate();

	// goals can handle events. Many don't though, so this defines a default
	// behavior
	default boolean handleEvent(IEvent event) {
		return false;
	}

	// a Goal is atomic and cannot aggregate subgoals yet we must implement
	// this method to provide the uniform interface required for the goal
	// hierarchy.
	default void addSubgoal(IGoal g) {
		throw new RuntimeException("Not implemented - Interface here");
	}

	GoalStatus getStatus();

	void setStatus(GoalStatus newStatus);


	default boolean isComplete() {
		return getStatus() == GoalStatus.completed;
	}

	default boolean isActive() {
		return getStatus() == GoalStatus.active;
	}

	default boolean isInactive() {
		return getStatus() == GoalStatus.inactive;
	}

	default boolean hasFailed() {
		return getStatus() == GoalStatus.failed;
	}

	String getType();
	Entity getAgent();

	/*
	 * the following methods were created to factor out some of the commonality
	 * in the implementations of the Process method()
	 */

	// if m_status = inactive this method sets it to active and calls Activate()
	default void activateIfInactive() {
		if (isInactive())
			activate();
	}

	// if m_status is failed this method sets it to inactive so that the goal
	// will be reactivated (and therefore re-planned) on the next update-step.
	default void reactivateIfFailed() {
		if (hasFailed())
			setStatus(GoalStatus.inactive);
	}


}
