package gl.mc.mistererwin.galaxion.api.ai.goals;

public interface IGoalAgent{}/* extends IEvent.IEventDispatcher, IEvent.IEventReceiver, IMovingEntity{


    void tick();

    public Goal_Think getBrain();

    default boolean isAIControlled() {
        return true;
    }

    default boolean handleEvent(IEvent event){return getBrain().handleEvent(event);}

    default double calculateTimeToReachPosition(PTile pPos){
        return new Vector(pPos.getAX(), pPos.getAY(), pPos.getAZ()).subtract(getLocation().toVector()).length() / (getMaxSpeed()*1);
    }
}
*/