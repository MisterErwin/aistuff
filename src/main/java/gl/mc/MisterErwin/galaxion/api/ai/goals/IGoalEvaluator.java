package gl.mc.mistererwin.galaxion.api.ai.goals;

import com.artemis.Entity;
import gl.mc.mistererwin.galaxion.implementation.ai.goals.Goal;

/**
 * Created by mistererwin on 04.05.2015.
 *
 * I suggest a singleton for this ;)
 */
public interface IGoalEvaluator<T extends Goal> {

    /**
     *
     * @return a value between (and including)  0 and 1 representing the desirability
     */
    double getDesirability(Entity pGoalAgent);

    /**
     * @param pGoalAgent The AI
     * @return a new instance of the proper goal
     */
    T getNewGoal(Entity pGoalAgent);


    /**
     *
     * @return The type of the goal
     */
    String getGoalType();

    /**
     *
     * @return false if the goal should be recraeted everytime its being evaluated and true if it should keep on running
     */
    default boolean getOnlyIfNotAlreadyPresent(){return true;}


}
