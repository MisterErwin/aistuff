package gl.mc.mistererwin.galaxion.api.ai.pathing;

import gl.mc.mistererwin.galaxion.api.ai.goals.IGoalAgent;

/**
 * Created by mistererwin on 05.05.2015.
 * In case you need it, ask me ;)
 */
public interface IPathPlannable extends IGoalAgent{
    IPathPlanner getPathPlanner();
}
