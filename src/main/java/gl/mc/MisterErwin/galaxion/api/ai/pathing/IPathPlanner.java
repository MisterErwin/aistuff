package gl.mc.mistererwin.galaxion.api.ai.pathing;

import org.bukkit.Location;

/**
 * Created by mistererwin on 05.05.2015.
 */
public interface IPathPlanner{

    /**
     * Creates a new path planning instance and calls an IPathPlanningFinishedEvent
     * @param pLoc
     * @param pAI
     * @return true
     */
    boolean requestPathToLocation( Location pLoc, IPathPlannable pAI);
}
