package gl.mc.mistererwin.galaxion.api.ai.pathing;

import gl.mc.mistererwin.galaxion.api.IEvent;

/**
 * Created by mistererwin on 05.05.2015.
 */
public class IPathPlanningEvents {

    public interface IPathPlanningEvent extends IEvent{
    }

    public interface IPathPlanningFinishedEvent extends IEvent{
        PathResult getResult();
        default PathResult.PathingResult getPathingResult(){
            return null;// getResult().getPathingResult();
        }
    }


}
