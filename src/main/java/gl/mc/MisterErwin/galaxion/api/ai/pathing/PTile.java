package gl.mc.mistererwin.galaxion.api.ai.pathing;

import gl.mc.mistererwin.galaxion.implementation.ai.pathing.astar.Tile;
import org.bukkit.Location;

public class PTile {

    private final int X, Y, Z;
    private final short a, b, c;
    private final Location start;

    private final Behavior behavior;

    public PTile(Location sp, Tile pt) {
        this(sp, pt, Behavior.normal);
    }


    public PTile(Location sp, Tile pt, Behavior pBehavior) {
        Tile p = pt.getParent();
        if (p != null) {
            a = (short) (pt.getX() - p.getX());
            b = (short) (pt.getY() - p.getY());
            c = (short) (pt.getZ() - p.getZ());
        } else
            a = b = c = 0;

        start = sp.getBlock().getLocation();
        X = pt.getX();
        Y = pt.getY();
        Z = pt.getZ();
        this.behavior = pBehavior;
    }

    public final short getA() {
        return this.a;
    }

    public final short getB() {
        return this.b;
    }

    public final short getC() {
        return this.c;
    }

    public final int getRX() {
        return this.X;
    }

    public final int getRY() {
        return this.Y;
    }

    public final int getRZ() {
        return this.Z;
    }

    public final Behavior getBehavior() {
        return this.behavior;
    }

    /**
     * @return the absolute X-Coordinate in the world
     */
    public final int getAX() {
        return this.X + start.getBlockX();
    }

    /**
     * @return the absolute Y-Coordinate in the world
     */
    public final int getAY() {
        return this.Y + start.getBlockY();
    }

    /**
     * @return the absolute Z-Coordinate in the world
     */
    public final int getAZ() {
        return this.Z + start.getBlockZ();
    }

    public final Location getStart() {
        return this.start;
    }

    public enum Behavior {
        swim, crouch, normal
    }

    public static String data(PTile tile){
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(tile.a).append("|").append(tile.b).append("|").append(tile.c).append("|").append(tile.behavior.name());
        return stringBuilder.toString();
    }
}