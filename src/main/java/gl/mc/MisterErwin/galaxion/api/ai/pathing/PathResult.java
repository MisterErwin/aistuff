package gl.mc.mistererwin.galaxion.api.ai.pathing;

import lombok.Data;

import java.util.LinkedList;

/**
 * Created by mistererwin on 05.05.2015.
 */
@Data
public class PathResult {

    private final PathingResult pathingResult;
    private final LinkedList<PTile> path;


    public enum PathingResult {

        SUCCESS(0), NO_PATH(-1);

        private final int ec;

        PathingResult(int ec) {
            this.ec = ec;
        }

        public int getEndCode() {
            return this.ec;
        }

    }

}
