package gl.mc.mistererwin.galaxion.api.animations;

import org.bukkit.util.EulerAngle;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class AnimationStage {
    long duration;
    double speed;
    private Map<String, EulerAngle> map = new HashMap<String, EulerAngle>();

    public AnimationStage(long duration, double speed) {
        this.duration = duration;
        this.speed = speed;
    }

    public AnimationStage(long duration) {
        this(duration, 1);
    }

    public EulerAngle get(String o) {
        return this.map.get(o);
    }

    public EulerAngle put(String Part, EulerAngle location) {
        return this.map.put(Part, location);
    }

    public Set<Map.Entry<String, EulerAngle>> entrySet() {
        return this.map.entrySet();
    }

    public long getDuration() {
        return this.duration;
    }

    public double getSpeed() {
        return this.speed;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }
}
