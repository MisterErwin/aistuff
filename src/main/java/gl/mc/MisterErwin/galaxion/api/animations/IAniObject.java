package gl.mc.mistererwin.galaxion.api.animations;

import org.bukkit.util.EulerAngle;

/**
 * Created by mistererwin on 04.05.2015.
 */
public interface IAniObject {
    IAniObject setTarget(EulerAngle t);

    IAniObject setSpeed(double s);

    void update();

    EulerAngle getCurrent();

    EulerAngle getTarget();

    void instant();
}
