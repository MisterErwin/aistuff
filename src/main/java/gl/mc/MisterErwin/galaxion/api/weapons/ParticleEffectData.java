package gl.mc.mistererwin.galaxion.api.weapons;

import gl.mc.mistererwin.galaxion.utils.ParticleEffect;
import org.bukkit.Location;

/**
 * Created by mistererwin on 22.05.2015.
 * In case you need it, ask me ;)
 */
public class ParticleEffectData {

    public ParticleEffectData(ParticleEffect packet, float offsetX, float offsetY, float offsetZ, float speed, double range, int amount) {
        this.packet = packet;
        this.offsetX = offsetX;
        this.offsetY = offsetY;
        this.offsetZ = offsetZ;
        this.speed = speed;
        this.range = range;
        this.amount = amount;
    }

    private final ParticleEffect packet;
    private final float offsetX, offsetY, offsetZ, speed;
    private final double range;
    private final int amount;


    public void play(Location loc) {
        packet.display(offsetX, offsetY, offsetZ, speed, amount, loc, range);
    }
}
