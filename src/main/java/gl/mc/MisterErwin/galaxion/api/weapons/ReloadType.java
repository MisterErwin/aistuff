package gl.mc.mistererwin.galaxion.api.weapons;

/**
 * Created by mistererwin on 22.05.2015.
 * In case you need it, ask me ;)
 */
public enum ReloadType {
    NORMAL, BOLT, PUMP, INDIVIDUAL_BULLET
}
