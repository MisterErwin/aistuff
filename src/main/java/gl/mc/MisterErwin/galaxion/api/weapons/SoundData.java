package gl.mc.mistererwin.galaxion.api.weapons;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

/**
 * Created by mistererwin on 22.05.2015.
 * In case you need it, ask me ;)
 */
public class SoundData {
    final Sound sound;
    final float pitch, volume;
    final double range;

    public SoundData(Sound sound, float pitch, float volume, double range) {
        this.sound = sound;
        this.pitch = pitch;

        this.volume = volume;
        this.range = range * range;
    }

    public void play(Location location) {
        if (range > 0) {
            for (Player p : Bukkit.getOnlinePlayers())
                if (p.getLocation().distanceSquared(location) <= range)
                    p.playSound(location, sound, volume, pitch);
        } else
            location.getWorld().playSound(location, sound, volume, pitch);
    }
}
