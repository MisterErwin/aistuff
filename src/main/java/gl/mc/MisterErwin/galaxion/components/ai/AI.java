package gl.mc.mistererwin.galaxion.components.ai;

import com.artemis.Component;
import com.artemis.Entity;
import gl.mc.mistererwin.galaxion.implementation.ai.goals.GoalEvaluatorProvider;
import gl.mc.mistererwin.galaxion.implementation.ai.goals.Goal_Think;

/**
 * Created by mistererwin on 23.05.2015.
 * In case you need it, ask me ;)
 */
public class AI extends Component {
    Goal_Think goal;

    public Goal_Think getGoal() {
        return goal;
    }

    public boolean AIControlled = true;

    public void initialize(Entity e) {
        goal = new Goal_Think(e);
        goal.addGoalEvaluator(GoalEvaluatorProvider.Wait_GoalEvaluator.getInstance());

    }
}
