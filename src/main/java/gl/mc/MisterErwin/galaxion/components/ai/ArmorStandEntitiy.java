package gl.mc.mistererwin.galaxion.components.ai;

import com.artemis.Component;
import org.bukkit.entity.ArmorStand;

/**
 * Created by mistererwin on 23.05.2015.
 * In case you need it, ask me ;)
 */
public class ArmorStandEntitiy extends Component {
    public ArmorStandEntitiy(ArmorStand entity) {
        this.entity = entity;
    }

    public ArmorStand entity;
}
