package gl.mc.mistererwin.galaxion.components.ai;

import com.artemis.Component;

/**
 * Created by mistererwin on 23.05.2015.
 * In case you need it, ask me ;)
 */
public class Jump extends Component {
    public int jumpTicks=0;
    public double jumpStrength = 0.4999999910593033D;
}
