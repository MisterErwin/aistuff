package gl.mc.mistererwin.galaxion.components.ai;

import com.artemis.Component;

/**
 * Created by mistererwin on 23.05.2015.
 * In case you need it, ask me ;)
 */
public class Moveable extends Component {

    public boolean updated = false;

    public boolean allowMoving = true;

    public boolean RunIfNotRotated = false;

    public Moveable( double defaultSpeed, double swimSpeed, double crouchSpeed, double runSpeed) {
        this.defaultSpeed = defaultSpeed;
        this.swimSpeed = swimSpeed;
        this.crouchSpeed = crouchSpeed;
        this.runSpeed = runSpeed;
        this.speed = this.defaultSpeed;
    }

    public final double defaultSpeed;
    public final double swimSpeed;
    public final double crouchSpeed;
    public final double runSpeed;

    public double speed;

}
