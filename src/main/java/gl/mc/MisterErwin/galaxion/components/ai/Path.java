package gl.mc.mistererwin.galaxion.components.ai;

import com.artemis.Component;
import gl.mc.mistererwin.galaxion.api.ai.pathing.PTile;
import gl.mc.mistererwin.galaxion.implementation.ai.pathing.astar.TickedAStar;
import org.bukkit.Location;

import java.util.LinkedList;

/**
 * Created by mistererwin on 23.05.2015.
 * In case you need it, ask me ;)
 */
public class Path extends Component {

    boolean requested = false;
    public Location target = null;
    public Location start = null;
    public TickedAStar worker;

    public LinkedList<PTile> path;

    public long m_StartTime;
    public long m_TimeExpected;



    public boolean updated = false;
    public boolean following = true;

    public boolean isRequested() {
        return requested;
    }

    public void requestTo(Location to, Location start) {
        worker = null;
        target = to;
        this.start = start;
        requested = true;
    }

    public void finishRequest(LinkedList<PTile> tiles) {
        requested = false;

        start = null;
        if (tiles != null) {
            path = tiles;
            updated = true;
        }
        worker = null;
    }

    public boolean isFinished(){
        return ((path == null || path.isEmpty() )&& !requested);
    }

    public void cancel(){
        path = null;
    }
}
