package gl.mc.mistererwin.galaxion.components.ai;

import com.artemis.Component;
import com.artemis.Entity;

/**
 * Created by mistererwin on 23.05.2015.
 * In case you need it, ask me ;)
 */
public class Target extends Component {

    public Entity target;

    public Target() {
    }

    public Target(Entity target) {
        this.target = target;
    }
}
