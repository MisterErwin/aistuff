package gl.mc.mistererwin.galaxion.components.ai;

import com.artemis.Component;

/**
 * Created by mistererwin on 23.05.2015.
 * In case you need it, ask me ;)
 */
public class Turn extends Component {
    public Turn(float maxTurnRate) {
        this.maxTurnRate = maxTurnRate;
    }

    public Turn() {
        this(20);
    }

    public float maxTurnRate;
}
