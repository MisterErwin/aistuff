package gl.mc.mistererwin.galaxion.components.animation;

import com.artemis.Component;
import gl.mc.mistererwin.galaxion.implementation.animations.AniObject;
import org.bukkit.entity.ArmorStand;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by mistererwin on 23.05.2015.
 * In case you need it, ask me ;)
 */
public class AS_Animatable extends Component {
    public Map<String, AniObject> objects;

    public AS_Animatable() {
        objects = new HashMap<>();
    }

    public static AS_Animatable forArmorStand(ArmorStand as){
        AS_Animatable r = new AS_Animatable();
        r.objects.put("Head", new AniObject());
        r.objects.put("Body", new AniObject());
        r.objects.put("LeftArm", new AniObject());
        r.objects.put("RightArm", new AniObject());
        r.objects.put("LeftLeg", new AniObject());
        r.objects.put("RightLeg", new AniObject());

        return r;
    }
}
