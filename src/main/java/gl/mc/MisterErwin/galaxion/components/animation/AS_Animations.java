package gl.mc.mistererwin.galaxion.components.animation;

import com.artemis.Component;
import gl.mc.mistererwin.galaxion.api.animations.AnimationStage;
import gl.mc.mistererwin.galaxion.systems.AnimationSystem;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by mistererwin on 23.05.2015.
 * In case you need it, ask me ;)
 */
public class AS_Animations extends Component {
    public List<AniData> anis;

    public AS_Animations() {
        anis = new LinkedList();
    }

    public AS_Animations(AniData ani) {
        this();
        add(ani);
    }

    public void add(AniData ani) {
        if (ani != null && canAlsoPlay(ani) && !alreadyRunning(ani.name))
            anis.add(ani);
    }

    public boolean canAlsoPlay(AniData ani) {
        for (String s1 : ani.usedParts)
            for (AniData d : anis)
                for (String s2 : d.usedParts)
                    if (s1.equals(s2))
                        return false;
        return true;
    }

    public boolean alreadyRunning(String name) {
        for (AniData d : anis)
            if (d.name.equals(name))
                return true;
        return false;
    }

    public void removeAndAdd(AniData ani) {
        if (alreadyRunning(ani.name))
            return;
        Iterator<AniData> ad = anis.iterator();
        while (ad.hasNext()) {
            AniData dd = ad.next();
            outerloop:
            for (String s1 : ani.usedParts)
                for (String s2 : dd.usedParts)
                    if (s1.equals(s2)) {
                        ad.remove();
                        break outerloop;
                    }

        }
        anis.add(ani);
    }

    public static class AniData {
        public final String name;
        public final String[] usedParts;
        public final AnimationStage[] stages;

        public int curStage = 0;
        public boolean active = false;
        public long ticks = 1;
        public final Runnable callback;

        public boolean con;
        public double multByWalking = 0;

        public AniData(String name, String[] usedParts, AnimationStage[] stages) {
            this(name, usedParts, stages, null);
        }

        public AniData(String name, String[] usedParts, AnimationStage[] stages, Runnable callback) {
            this.usedParts = usedParts;
            this.stages = stages;
            active = true;
            this.callback = callback;
            this.name = name;
        }

        public AniData clone() {
            return new AniData(name, usedParts, stages, callback);
        }

        public AniData setCon() {
            con = true;
            return this;
        }

        public AniData setCon(boolean b) {
            con = b;
            return this;
        }

        public AniData setMultByWalking(double d) {
            this.multByWalking = d;
            return this;
        }
    }

    public static void pauseAnimation(AS_Animations a, String name) {
        for (AniData d : a.anis)
            if (d.name.equals(name)) {
                d.active = false;
                return;
            }
    }

    public static void continueAnimation(AS_Animations a, String name) {
        for (AniData d : a.anis)
            if (d.name.equals(name)) {
                d.active = true;
                return;
            }
    }

    public static void stopAnimation(AS_Animations a, String name) {
        Iterator<AniData> it = a.anis.iterator();
        AniData d;
        while (it.hasNext()) {
            d = it.next();
            if (d.name.equals(name))
                it.remove();
        }
    }


    public static void resetAnimation(AS_Animations a, String name) {
        for (AniData d : a.anis)
            if (d.name.equals(name)) {
                d.curStage = -1;
                return;
            }


    }


    public static void addAnimation(AS_Animations a, String name) {
        a.add(AnimationSystem.getAniData(name));
    }


    public static void addForceAnimation(AS_Animations a, String name) {
        a.removeAndAdd(AnimationSystem.getAniData(name));
    }

    public static void addConAnimation(AS_Animations a, String name) {
        a.add(AnimationSystem.getAniData(name).setCon());
    }

    public static void addForceConAnimation(AS_Animations a, String name) {
        a.removeAndAdd(AnimationSystem.getAniData(name).setCon());
    }


}
