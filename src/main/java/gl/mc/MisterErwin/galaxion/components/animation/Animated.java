package gl.mc.mistererwin.galaxion.components.animation;

import com.artemis.Component;
import gl.mc.mistererwin.galaxion.implementation.animations.AnimChannel;
import gl.mc.mistererwin.galaxion.implementation.animations.AnimEventListener;


import java.util.ArrayList;
import java.util.List;

//import gl.mc.mistererwin.galaxion.implementation.animations.AnimChannel;
//import gl.mc.mistererwin.galaxion.implementation.animations.AnimEventListener;

/**
 * Created by mistererwin on 11.07.2015.
 * In case you need it, ask me ;)
 */
public class Animated extends Component {
    public transient List<AnimChannel> channels = new ArrayList<>();
    public transient ArrayList<AnimEventListener> listeners = new ArrayList<AnimEventListener>();

}
