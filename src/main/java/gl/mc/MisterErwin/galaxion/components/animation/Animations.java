package gl.mc.mistererwin.galaxion.components.animation;

import com.artemis.Component;
import gl.mc.mistererwin.galaxion.implementation.animations.Animation;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by mistererwin on 11.07.2015.
 * In case you need it, ask me ;)
 */
public class Animations extends Component {
    /**
     * List of animations
     */
    public Map<String, Animation> animationMap = new HashMap<>();
}
