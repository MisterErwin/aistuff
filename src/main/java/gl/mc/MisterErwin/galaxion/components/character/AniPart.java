package gl.mc.mistererwin.galaxion.components.character;

import gl.mc.mistererwin.galaxion.utils.math.Quaternion;
import gl.mc.mistererwin.galaxion.utils.math.Transform;
import gl.mc.mistererwin.galaxion.utils.math.Vector3f;
import org.bukkit.entity.ArmorStand;
import org.bukkit.inventory.ItemStack;

import java.util.Arrays;

/**
 * Created by mistererwin on 30.06.2015.
 * In case you need it, ask me ;)
 */
public class AniPart  {
    public final Transform localTransform = new Transform();
    public final Transform initialTransform = new Transform();
    public final Transform worldTransform = new Transform(); //cached
    public final String name;
    public AniPart[] children;
    public AniPart parent;
    public transient ArmorStand as;
    public ItemStack head;
    //Used to handle blending from one animation to another.
    public transient float currentWeightSum = 0;
    public boolean inverse = false;
    public boolean userControlled = false;

    public AniPart(String name, AniPart parent, AniPart[] children, ItemStack head) {
        this.name = name;
        this.children = children;
        this.parent = parent;
        this.head = head;
    }

    public AniPart(String name, AniPart parent, AniPart[] children, ItemStack head, Vector3f translation) {
        this(name, parent, children, head);
        initialTransform.setTranslation(translation);
    }

    public AniPart(String name, AniPart parent, AniPart[] children, ItemStack head, Quaternion rot) {
        this(name, parent, children, head);
        initialTransform.setRotation(rot);
    }

    public AniPart(String name, AniPart parent, AniPart[] children, ItemStack head, Vector3f translation, Quaternion rot) {
        this(name, parent, children, head);
        initialTransform.set(localTransform).setRotation(rot);
    }

    public AniPart(String name, AniPart parent, AniPart[] children, ItemStack head, Vector3f translation, boolean inverse) {
        this(name, parent, children, head,translation);
        this.inverse = inverse;
    }


    public AniPart(String name, AniPart parent, ItemStack head) {
        this(name, parent, new AniPart[0], head);
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AniPart aniPart = (AniPart) o;

        if (Float.compare(aniPart.currentWeightSum, currentWeightSum) != 0) return false;
        if (inverse != aniPart.inverse) return false;
        if (userControlled != aniPart.userControlled) return false;
        if (!worldTransform.equals(aniPart.worldTransform)) return false;
        if (!name.equals(aniPart.name)) return false;
        // Probably incorrect - comparing Object[] arrays with Arrays.equals
        if (!Arrays.equals(children, aniPart.children)) return false;
        if (!parent.equals(aniPart.parent)) return false;
        return head.equals(aniPart.head);

    }

    @Override
    public int hashCode() {
        int result = name.hashCode();
        result = 31 * result + Arrays.hashCode(children);
        return result;
    }
}
