package gl.mc.mistererwin.galaxion.components.character;

import com.artemis.Component;

/**
 * Created by mistererwin on 30.06.2015.
 * A BoneEntity is a hierarchy of bones
 */
public class BoneEntity extends Component {
    public AniPart[] roots;
}