package gl.mc.mistererwin.galaxion.components.character;

import com.artemis.Component;

import java.util.Arrays;

/**
 * Created by mistererwin on 14.07.2015.
 * In case you need it, ask me ;)
 */
public class Parted extends Component {
    public AniPart rootPart;
    public AniPart[] partList;

    /**
     * returns the part index of the given part
     *
     * @param part
     * @
     */
    public int getPartIndex(AniPart part) {
        for (int i = 0; i < partList.length; i++) {
            if (partList[i] == part) {
                return i;
            }
        }

        return -1;
    }

    /**
     * returns the part index of the given partname
     *
     * @param partName
     * @return
     */
    public int getPartIndex(String partName) {
        for (int i = 0; i < partList.length; i++) {
            if (partList[i].name.equals(partName)) {
                return i;
            }
        }
        return -1;
    }


    /**
     * returns the number of parts of this body
     * @return
     */
    public int getPartCount() {
        return partList.length;
    }

    /**
     * return a part for the given index
     * @param index
     * @return
     */
    public AniPart getPart(int index) {
        return partList[index];
    }

    /**
     * returns the bone with the given name
     * @param name
     * @return
     */
    public AniPart getPart(String name) {
        for (int i = 0; i < partList.length; i++) {
            if (partList[i].name.equals(name)) {
                return partList[i];
            }
        }
        return null;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Parted parted = (Parted) o;

        if (!rootPart.equals(parted.rootPart)) return false;
        // Probably incorrect - comparing Object[] arrays with Arrays.equals
        return Arrays.equals(partList, parted.partList);

    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(partList);
    }
}
