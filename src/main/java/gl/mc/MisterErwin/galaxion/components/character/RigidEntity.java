package gl.mc.mistererwin.galaxion.components.character;

import com.artemis.Component;
import gl.mc.mistererwin.galaxion.implementation.generic.RigidArmorStand;

/**
 * Created by mistererwin on 22.06.2015.
 * In case you need it, ask me ;)
 */
public class RigidEntity extends Component {
    public RigidArmorStand[] parts;

    public RigidEntity(RigidArmorStand[] parts) {
        this.parts = parts;
    }
}
