package gl.mc.mistererwin.galaxion.components.combat;

import com.artemis.Component;
import gl.mc.mistererwin.galaxion.utils.MyMaterial;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by mistererwin on 24.05.2015.
 * In case you need it, ask me ;)
 */
public class AmmoHolder extends Component {
    public boolean infinite;
    public Map<MyMaterial,Integer> ammo;

    public AmmoHolder(boolean infinite) {
        this.infinite = infinite;
        ammo = new HashMap<>();
    }

}
