package gl.mc.mistererwin.galaxion.components.combat;

/**
 * Created by mistererwin on 23.05.2015.
 * In case you need it, ask me ;)
 */

import com.artemis.Component;

public class Health extends Component {
    private double health;
    private double maxHealth;


    public Health(double health, double maxHealth) {
        this.health = health;
        this.maxHealth = maxHealth;
    }
    public double getHealth() {
        return health;
    }

    public void setHealth(double health) {
        this.health = health;
    }

    public double getMaxHealth() {
        return maxHealth;

    }

}
