package gl.mc.mistererwin.galaxion.components.combat;

import com.artemis.Component;
import gl.mc.mistererwin.galaxion.implementation.weapons.Weapon;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by mistererwin on 24.05.2015.
 * In case you need it, ask me ;)
 */
public class WeaponHolder extends Component {
    public List<Weapon> weaponList = new LinkedList<>();
    public int lastSlot =0;
    public Weapon currentlyUsing;
    private int ticks;

}
