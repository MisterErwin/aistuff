package gl.mc.mistererwin.galaxion.components.generic;

import com.artemis.Component;

/**
 * Created by mistererwin on 13.07.2015.
 * In case you need it, ask me ;)
 */
public class DespawnBehavior extends Component {
    public DespawnBehavior(boolean persistent) {
        this.persistent = persistent;
    }

    public final boolean persistent;

    public boolean isDespawned = false;
    public boolean requestRespawn = false;
}
