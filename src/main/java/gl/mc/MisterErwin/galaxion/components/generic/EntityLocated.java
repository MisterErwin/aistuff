package gl.mc.mistererwin.galaxion.components.generic;

import com.artemis.Component;
import org.bukkit.entity.Entity;

/**
 * Created by mistererwin on 20.06.2015.
 * In case you need it, ask me ;)
 */
public class EntityLocated extends Component{

    public EntityLocated(Entity entity) {
        this.entity = entity;
    }

    public org.bukkit.entity.Entity entity;
}
