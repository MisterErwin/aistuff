package gl.mc.mistererwin.galaxion.components.generic;

import com.artemis.Component;

/**
 * Created by mistererwin on 23.05.2015.
 * In case you need it, ask me ;)
 */
public class FOV extends Component {
    public double VerticalFOV = 180;
    public double HorizontalFOV = 180;

}
