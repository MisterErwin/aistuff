package gl.mc.mistererwin.galaxion.components.generic;

import com.artemis.Component;
import gl.mc.mistererwin.galaxion.utils.math.Quaternion;
import gl.mc.mistererwin.galaxion.utils.math.Transform;
import gl.mc.mistererwin.galaxion.utils.math.Vector3f;
import org.bukkit.Location;

/**
 * Created by mistererwin on 24.05.2015.
 * In case you need it, ask me ;)
 */
public class Located extends Component {
    public Location location;
    public final Transform transform;

    public Located(Location location) {
        this.location = location;
        transform = new Transform(new Vector3f((float)location.getX(),(float)location.getY(),(float)location.getZ()),
                new Quaternion(new float[]{0,(float)Math.toRadians(location.getYaw()), 0}));
    }



    public Vector3f toVector3f() {
        return new Vector3f((float)location.getX(),(float)location.getY(),(float)location.getZ());
    }

    public Vector3f toVector3f(Vector3f vector3f){
        return vector3f.set((float)location.getX(),(float)location.getY(),(float)location.getZ());
    }
}
