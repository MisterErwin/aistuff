package gl.mc.mistererwin.galaxion.components.scenario;

import com.artemis.Component;
import org.bukkit.scheduler.BukkitTask;

/**
 * Created by mistererwin on 25.05.2015.
 * In case you need it, ask me ;)
 */
public class Scenario extends Component {
    public BukkitTask ThreadID;

    public Scenario(BukkitTask threadID) {
        ThreadID = threadID;
    }
}
