package gl.mc.mistererwin.galaxion.components.web;

import com.artemis.Component;
import org.java_websocket.WebSocket;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by mistererwin on 06.06.2015.
 * In case you need it, ask me ;)
 */
public class WatchableEntity extends Component {
    public final String type;
    public List<WebSocket> watcher = new LinkedList<>();
    public Map<String,Object> lastV = new HashMap<>();


    public WatchableEntity(String type) {
        this.type = type;
    }

}
