package gl.mc.mistererwin.galaxion.implementation.ai;

import gl.mc.mistererwin.galaxion.api.ai.IRegulator;

/**
 * Created by mistererwin on 05.05.2015.
 * In case you need it, ask me ;)
 */
public class Regulator implements IRegulator {

    private final int m_updateTime;
    private int m_currentTick=0;

    public Regulator(int pUpdateTime){
        this.m_updateTime = pUpdateTime;
    }

    public boolean isReady(){
        m_currentTick--;
        if (m_currentTick<=0){
            m_currentTick=m_updateTime;
            return true;
        }
        return false;
    }
}
