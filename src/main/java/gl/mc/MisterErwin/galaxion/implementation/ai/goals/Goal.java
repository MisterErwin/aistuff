package gl.mc.mistererwin.galaxion.implementation.ai.goals;

import com.artemis.Entity;
import gl.mc.mistererwin.galaxion.api.IEvent;
import gl.mc.mistererwin.galaxion.api.ai.goals.GoalStatus;
import gl.mc.mistererwin.galaxion.api.ai.goals.ICompositeGoal;
import gl.mc.mistererwin.galaxion.api.ai.goals.IGoal;

import java.util.LinkedList;

public abstract class Goal implements IGoal{

	// Reference to the entity that owns this goal
	private Entity m_agent;

	// Status
	private GoalStatus m_status;

	// Type of the goal
	private String m_type;

	public Goal(Entity pOwner, String pType, GoalStatus pStatus) {
		this.m_agent = pOwner;
		this.m_status = pStatus;
		this.m_type = pType;
	}

	public Goal(Entity pOwner, String pType) {
		this(pOwner, pType, GoalStatus.inactive);
	}


	// a Goal is atomic and cannot aggregate subgoals yet we must implement
	// this method to provide the uniform interface required for the goal
	// hierarchy.
	public void addSubgoal(IGoal g) {
		throw new RuntimeException("Cannot add goals to atomic goals");
	}

	public boolean isComplete() {
		return m_status == GoalStatus.completed;
	}

	public boolean isActive() {
		return m_status == GoalStatus.active;
	}

	public boolean isInactive() {
		return m_status == GoalStatus.inactive;
	}

	public boolean hasFailed() {
		return m_status == GoalStatus.failed;
	}

	public String getType() {
		return m_type;
	}
    public Entity getAgent() {
        return m_agent;
    }

    public GoalStatus getStatus() {
        return m_status;
    }

    public void setStatus(GoalStatus pStatus) {
        m_status=pStatus;
    }

	public void activate(){
		setStatus(GoalStatus.active);
	}

	public abstract static class CompositeGoal extends Goal implements ICompositeGoal {

		private LinkedList<IGoal> m_subGoals;

		public CompositeGoal(Entity pOwner, String pType) {
			super(pOwner, pType);
            m_subGoals = new LinkedList<>();
		}
		
		public void removeAllSubgoals(){
			for (IGoal g : m_subGoals)
				g.terminate();
			m_subGoals.clear();
		}

		public GoalStatus process(){
			setStatus(processSubgoals());
			return getStatus();
		}

		public void terminate(){
			removeAllSubgoals();
			this.m_subGoals=null;
		}

		protected GoalStatus processSubgoals() {
//            System.out.println("Processing Subgoals of" + getClass().getSimpleName() + "//" + getType());
			// remove all completed and failed goals from the front
			while (!m_subGoals.isEmpty()
					&& (m_subGoals.getFirst().isComplete() || m_subGoals
							.getFirst().hasFailed())) {
				m_subGoals.getFirst().terminate();
				m_subGoals.removeFirst();
			}

			// if any subgoals remain, process the first one
			if (!m_subGoals.isEmpty()) {
//                System.out.println("(sub)Goal" + m_subGoals.getFirst().getType());

                GoalStatus statusOfSubGoals = m_subGoals.getFirst().process();

				// we have to test for the special case where the front-most
				// subgoal
				// reports 'completed' *and* the subgoal list contains
				// additional goals.When
				// this is the case, to ensure the parent keeps processing its
				// subgoal list
				// we must return the 'active' status.
				if (statusOfSubGoals == GoalStatus.completed
						&& m_subGoals.size() > 1)
					return GoalStatus.active;
				return statusOfSubGoals;
			} else
				// no more subgoals to process - return 'completed'
				return GoalStatus.completed;
			// i
		}

        @Override
		public void addSubgoal(IGoal g) {
			// return false if the message has not been handled
			m_subGoals.addFirst(g);
		}

        @Override
        public IGoal getFrontGoal(){
            return (m_subGoals.isEmpty())?null:m_subGoals.getFirst();
        }


        /**
         * Passes the message to the goal at the front of the queue
         * @param event the message
         * @return no idea xD - maybe for later
         */
		public boolean forwardEventToFrontMostSubgoal(IEvent event) {
			if (!m_subGoals.isEmpty())
				return m_subGoals.getFirst().handleEvent(event);
			// return false if the message has not been handled
			return false;
		}
	}

}