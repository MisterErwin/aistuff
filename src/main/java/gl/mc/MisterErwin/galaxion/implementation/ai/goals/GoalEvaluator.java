package gl.mc.mistererwin.galaxion.implementation.ai.goals;

import com.artemis.Entity;
import gl.mc.mistererwin.galaxion.api.ai.goals.IGoalEvaluator;

/**
 * Created by mistererwin on 04.05.2015.
 */
public abstract class GoalEvaluator<T extends Goal> implements IGoalEvaluator<T>{

    @Override
    public double getDesirability(Entity pGoalAgent) {
        return 0.001;
    }

    protected double getNormalizedValue(double current, double max){
        return current/max;
    }

}
