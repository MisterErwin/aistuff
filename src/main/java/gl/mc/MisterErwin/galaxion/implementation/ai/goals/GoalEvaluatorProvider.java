package gl.mc.mistererwin.galaxion.implementation.ai.goals;

import com.artemis.Entity;

/**
 * Created by mistererwin on 05.05.2015.
 * In case you need it, ask me ;)
 */
public class GoalEvaluatorProvider {

    public static class Wait_GoalEvaluator extends GoalEvaluator<Goal_Wait> {
        private static Wait_GoalEvaluator ourInstance = new Wait_GoalEvaluator();

        public static Wait_GoalEvaluator getInstance() {
            return ourInstance;
        }

        private Wait_GoalEvaluator() {
        }

        @Override
        public Goal_Wait getNewGoal(Entity pGoalAgent) {
            return new Goal_Wait(pGoalAgent);
        }

        @Override
        public String getGoalType() {
            return "Goal_Wait";
        }

        @Override
        public double getDesirability(Entity pGoalAgent) {
            return 0.000001;
        }
    }

//    public static class Follow_GoalEvaluator extends GoalEvaluator<Goal_Follow> {
//        private static Follow_GoalEvaluator ourInstance = new Follow_GoalEvaluator();
//
//        public static Follow_GoalEvaluator getInstance() {
//            return ourInstance;
//        }
//
//        private Follow_GoalEvaluator() {
//        }
//
//        @Override
//        public Goal_Follow getNewGoal(IGoalAgent pGoalAgent) {
//            return new Goal_Follow((IPathPlannable) pGoalAgent, getTarget(pGoalAgent, false));
//        }
//
//        @Override
//        public String getGoalType() {
//            return Goal_Follow.TYPE;
//        }
//
//        @Override
//        public double getDesirability(IGoalAgent pGoalAgent) {
//            return (getTarget(pGoalAgent, true) != null) ? 0.4 : 0;
//        }
//
//
//        private IBaseEntity getTarget(IGoalAgent pGoalAgent, boolean oneIsEnough1) {
//            int n = 0;
//            IBaseEntity best = null;
//            double dist = Double.MAX_VALUE;
//            double cdist = 0;
//            for (IBaseEntity e : AIStuff.instance.getEntityRegistry().getAIEntities()) {
//                if (n > 10) break;
//                if (e != pGoalAgent && e instanceof IArmored && e.getWorld().equals(pGoalAgent.getWorld()) &&
//                        ((IArmored) e).getHelmet() != null && ((IArmored) e).getHelmet().getType() == Material.DIAMOND_HELMET) {
//                    cdist = e.getLocation().distanceSquared(pGoalAgent.getLocation());
//                    if (cdist < 500 && n++ > 0 && AWorldUtils.isLOSOkay(pGoalAgent.getLocation(), e)) {
//                        if (cdist < 500 && cdist < dist) {
//                            best = e;
//                            dist = cdist;
//                            if (oneIsEnough1)
//                                return e;
//                        }
//
//                    }
//                }
//            }
//            return best;
//        }
//
//    }
}
