package gl.mc.mistererwin.galaxion.implementation.ai.goals;

/**
 * Created by mistererwin on 05.05.2015.
 * In case you need it, ask me ;)
 */
public class Goal_Follow {} /*extends Goal.CompositeGoal{
    private WeakReference<IBaseEntity> m_target;

    private double minDist = 5;

    public static final String TYPE = "Goal_Follow";

    private int reSearchTicks = 20;

    public Goal_Follow(Entity pOwner, IBaseEntity pTarget) {
        super(pOwner, TYPE);
        m_target=new WeakReference<IBaseEntity>(pTarget);
    }


    @Override
    public GoalStatus process(){
        IBaseEntity t = m_target.get();
        if (t == null){
            setStatus(GoalStatus.completed);
            return GoalStatus.completed;
        }
        setStatus(processSubgoals());



        if (reSearchTicks-- <= 0){
            reSearchTicks=20;
            removeAllSubgoals();
            addSubgoal(new Goal_MoveToPosition(getAgent(), t.getLocation().add(0, -1, 0) ));
        }

        if (isComplete())
            setStatus(GoalStatus.active);

        return getStatus();
    }

}
*/