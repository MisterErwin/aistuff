package gl.mc.mistererwin.galaxion.implementation.ai.goals;

import com.artemis.Entity;
import gl.mc.mistererwin.galaxion.api.ai.goals.GoalStatus;
import gl.mc.mistererwin.galaxion.api.ai.pathing.PTile;
import gl.mc.mistererwin.galaxion.components.ai.Path;

import java.util.LinkedList;

/**
 * Created by mistererwin on 05.05.2015.
 * In case you need it, ask me ;)
 */
public class Goal_Follow_Path extends Goal.CompositeGoal {

//    LinkedList<PTile> m_Path;

    public Goal_Follow_Path(Entity agent) {
        super(agent, "Goal_Follow_Path");
    }

    @Deprecated
    public Goal_Follow_Path(Entity agent, PTile[] pPath) {
        super(agent, "Goal_Follow_Path");
//        this.m_Path = new LinkedList<>();
//        for (PTile p : pPath)
//            this.m_Path.add(p);
    }

    @Deprecated
    public Goal_Follow_Path(Entity agent, LinkedList<PTile> pPath) {
        super(agent, "Goal_Follow_Path");
//        this.m_Path = pPath;
//        m_Path.removeFirst();
    }

    @Override
    public void activate() {
//        if (m_Path.isEmpty()){
//            setStatus(GoalStatus.completed);
//        }
        setStatus(GoalStatus.active);
//        //get a reference to the next edge
//        PTile first = m_Path.getFirst();
//        //remove the edge from the path
//        m_Path.removeFirst();
//
//        //TODO: ?
//        //some edges specify that the AI should use a specific behavior when
//        //following them. This switch statement queries the edge behavior flag and
//        //adds the appropriate goals/s to the subgoal list.
//        addSubgoal(new Goal_TraversePTile(getAgent(), first, m_Path.isEmpty()));
    }

    @Override
    public GoalStatus process() {
        activateIfInactive();

//        setStatus(processSubgoals());
        Path p = getAgent().getComponent(Path.class);
        if (p == null) {
            setStatus(GoalStatus.failed);
        } else if (p.isFinished()) {
            setStatus(GoalStatus.completed);
        } else if (!p.following)
            setStatus(GoalStatus.inactive);
        else
            setStatus(GoalStatus.active);

//        if (getStatus() == GoalStatus.completed)
//            if (!m_Path.isEmpty())
//                activate();
//            else if (getAgent() instanceof IAnimatable)
//                ((IAnimatable) getAgent()).stopAnimation("walk");
        return getStatus();
    }

}
