package gl.mc.mistererwin.galaxion.implementation.ai.goals;

import com.artemis.Entity;
import gl.mc.mistererwin.galaxion.api.IEvent;
import gl.mc.mistererwin.galaxion.api.ai.goals.GoalStatus;
import gl.mc.mistererwin.galaxion.components.ai.ArmorStandEntitiy;
import gl.mc.mistererwin.galaxion.components.ai.Path;
import org.bukkit.Location;

/**
 * Created by mistererwin on 05.05.2015.
 */
public class Goal_MoveToPosition extends Goal.CompositeGoal {

    private final Location m_Destination;

    private static final String TYPE = "Goal_MoveToPosition";

    private Goal_MoveToPosition() {
        super(null, null);
        throw new IllegalAccessError("Private");
    }


    public Goal_MoveToPosition(Entity pOwner, Location pDest) {
        super(pOwner, TYPE);
        m_Destination = pDest;
    }

    @Override
    public void activate() {
        removeAllSubgoals();
        super.activate();
        getAgent().getComponent(Path.class).requestTo(m_Destination, getAgent().getComponent(ArmorStandEntitiy.class).entity.getLocation().subtract(0,1,0));
//        ((IPathPlannable) getAgent()).getPathPlanner().requestPathToLocation(m_Destination, (IPathPlannable) getAgent());
//        addSubgoal(new Goal_Wait(getAgent()));
        addSubgoal(new Goal_Follow_Path(getAgent()));
        System.out.println("Awaiting");
    }

    @Override
    public GoalStatus process() {
        //if status is inactive, call Activate()
        activateIfInactive();

        //process the subgoals
        setStatus(processSubgoals());

        //if any of the subgoals have failed then this goal re-plans
        //Handled in the MovementSystem
//        if (hasFailed()){
//            setStatus(GoalStatus.active);
//            removeAllSubgoals();
//            Path p = getAgent().getComponent(Path.class);
//            p.requestTo(m_Destination);
//                    ((IPathPlannable) getAgent()).getPathPlanner().requestPathToLocation(m_Destination, (IPathPlannable) getAgent());
//            addSubgoal(new Goal_Wait(getAgent()));
//        }

        return getStatus();
    }

    @Override
    public boolean handleEvent(IEvent event) {
        boolean handled = forwardEventToFrontMostSubgoal(event);
        if (!handled) {
//            if (event instanceof IPathPlanningEvents.IPathPlanningFinishedEvent) {
//                if (((IPathPlanningEvents.IPathPlanningFinishedEvent) event).getPathingResult() == PathResult.PathingResult.SUCCESS) {
//                    if (getAgent() instanceof IAnimatable)
//                        ((IAnimatable)getAgent()).startContinueingAnimation(new AnimationProvider.YAMLAnimation((IAnimatable)getAgent(),"walk",1,1));
//
//                    removeAllSubgoals();
//                    addSubgoal(new Goal_Follow_Path(getAgent(), ((IPathPlanningEvents.IPathPlanningFinishedEvent) event).getResult().getPath()));
//                    return true; //msg handled;
//                } else if (((IPathPlanningEvents.IPathPlanningFinishedEvent) event).getPathingResult() == PathResult.PathingResult.NO_PATH) {
//                    setStatus(GoalStatus.failed);
//                    return true; //msg handled
//                }
//            }
        }
        return handled;
    }
}
