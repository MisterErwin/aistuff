package gl.mc.mistererwin.galaxion.implementation.ai.goals;

import com.artemis.Entity;
import gl.mc.mistererwin.galaxion.api.IEvent;
import gl.mc.mistererwin.galaxion.api.ai.goals.GoalStatus;
import gl.mc.mistererwin.galaxion.components.ai.AI;
import org.apache.logging.log4j.core.helpers.Assert;
import org.bukkit.Location;

import java.util.LinkedList;
import java.util.List;

/**
 * Main Goal
 * <p>
 * Created by mistererwin on 04.05.2015.
 */
public class Goal_Think extends Goal.CompositeGoal {

    private List<GoalEvaluator<?>> m_Evaluators = new LinkedList<>();


    public Goal_Think(Entity agent) {
        super(agent, "Goal_Think");
    }

    public void addGoalEvaluator(GoalEvaluator pGoalEvaluator) {
        m_Evaluators.add(pGoalEvaluator);
    }

    @Override
    public void activate() {
        AI a = getAgent().getComponent(AI.class);
        if (a != null && a.AIControlled)
            arbitrate();

        setStatus(GoalStatus.active);
    }

    @Override
    public GoalStatus process() {
        activateIfInactive();

        GoalStatus subgoalStatus = processSubgoals();

        if (subgoalStatus == GoalStatus.completed || subgoalStatus == GoalStatus.failed) {
            AI a = getAgent().getComponent(AI.class);
            if (a != null && a.AIControlled)
                arbitrate();
            setStatus(GoalStatus.inactive);
        }

        return getStatus();
    }


    public void arbitrate() {
        double best = 0;
        GoalEvaluator MostDesirable = null;

        //iterate through all the evaluators to see which produces the highest score
        for (GoalEvaluator curDes : m_Evaluators) {
            double desirabilty = curDes.getDesirability(getAgent());

            if (desirabilty >= best) {
                best = desirabilty;
                MostDesirable = curDes;
            }
        }
        Assert.isNotNull(MostDesirable, "Goal_Think#arbitrate() - no evaluator selected");
        if ((best >= 0.001||getFrontGoal() ==null) &&
                !(MostDesirable.getOnlyIfNotAlreadyPresent()
                        && getFrontGoal() != null
                        && getFrontGoal().getType().equals(MostDesirable.getGoalType()))) {
            System.out.println("getOnlyIfNotAlreadyPresent" + MostDesirable.getOnlyIfNotAlreadyPresent());
            System.out.println("getFrontGoal() != null" + (getFrontGoal() != null));
            if (getFrontGoal() != null)
                System.out.println("Changing goal to " + MostDesirable.getGoalType() + "/" + getFrontGoal().getType() + "--" + MostDesirable.getGoalType().equals(getFrontGoal().getType()));
            else
                System.out.println("Changing goal to " + MostDesirable.getGoalType() + "/NULL");
            removeAllSubgoals();
            addSubgoal(MostDesirable.getNewGoal(getAgent()));
            System.out.println("Changed to " + getFrontGoal().getType() + "--------------------------------------------");

        }
    }

    @Override
    public void terminate() {

    }

    @Override
    public boolean handleEvent(IEvent event) {
//        System.out.println("Event: " + event.getType() + " //   " + event.getClass().getSimpleName());
        boolean handled = this.forwardEventToFrontMostSubgoal(event);
        if (!handled) {
//            if (event instanceof IPathPlanningEvents.IPathPlanningFinishedEvent) {
//                if (((IPathPlanningEvents.IPathPlanningFinishedEvent) event).getPathingResult() == PathResult.PathingResult.SUCCESS) {
//                    addSubgoal(new Goal_Follow_Path(getAgent(), ((IPathPlanningEvents.IPathPlanningFinishedEvent) event).getResult().getPath()));
//                    handled = true;
//                }
//            }
        }
        return handled;
    }

    public void moveTo(Location loc) {
//        if (getAgent() instanceof IPathPlannable)
            addSubgoal(new Goal_MoveToPosition(getAgent(), loc));
//        else
//            throw new RuntimeException("No Pathfinding goal found");
    }
}
