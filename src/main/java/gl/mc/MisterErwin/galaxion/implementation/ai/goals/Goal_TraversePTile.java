package gl.mc.mistererwin.galaxion.implementation.ai.goals;

import com.artemis.Entity;
import gl.mc.mistererwin.galaxion.api.ai.goals.GoalStatus;
import gl.mc.mistererwin.galaxion.api.ai.pathing.PTile;

/**
 * Created by mistererwin on 05.05.2015.
 */
public class Goal_TraversePTile extends Goal {

    final PTile m_next;
    final boolean m_LastInPath;
    long m_TimeExpected;
    long m_StartTime;

    private static final long MarginOfError = 1000;
    private static final boolean RunIfNotRotated = false;

    public Goal_TraversePTile(Entity pBot, PTile pNext, boolean pLastInPath) {
        super(pBot, "Goal_TraversePTile");
        this.m_next = pNext;
        this.m_LastInPath = pLastInPath;
        this.m_StartTime = System.currentTimeMillis();
    }

    @Override
    public void activate() {
//        setStatus(GoalStatus.active);
//        switch (m_next.getBehavior()) {
//            case swim:
//                getAgent().setMaxSpeed(getAgent().getMaxSpeed()); //ToDO!
//                break;
//            case crouch:
//                break;
//            default:
//
//                break;
//        }
//
//        //record the time the bot starts this goal
//        m_StartTime = System.currentTimeMillis();
//
//        //calculate the expected time required to reach the this waypoint. This value
//        //is used to determine if the bot becomes stuck
//        m_TimeExpected = (long) getAgent().calculateTimeToReachPosition(m_next) + MarginOfError;
    }

    @Override
    public void terminate() {
    }

    @Override
    public GoalStatus process() {
        setStatus(GoalStatus.failed);
//        activateIfInactive();
//        int i = (int) Math.floor(getAgent().getLocation().getY() + 0.5);
//
//        double dX = m_next.getAX()+.5 - getAgent().getLocation().getX();
//        double dZ = m_next.getAZ() +.5- getAgent().getLocation().getZ();
//        double ddY = m_next.getAY()+1 - i;
//        double dY = m_next.getAY() - getAgent().getLocation().getY() + 1;
//
//        double dist = dX * dX + dZ * dZ + dY * dY;
//
//        if (isStuck()) {
////            System.out.println("Stuck => Failed");
//            setStatus(GoalStatus.failed);
//        } else if (dist < 0.3) { //2.500000277905201E-007D) {
//
//            setStatus(GoalStatus.completed);
//        } else {
//            setStatus(GoalStatus.active);
//            float f = (float) (Math.atan2(dY, dX) * 180.0D / 3.141592741012573D) - 90.0F;
//            float yaw = a(getAgent().getLocation().getYaw(), f, 30.0F);
//            float yawchange = getAgent().getLocation().getYaw() - yaw;
//            if (Math.abs(yawchange) > getAgent().getMaxTurnRate()) {
//
//                yaw = trimTo(yaw, yawchange, getAgent().getMaxTurnRate());
//
//                setYaw(yaw);
//                if (RunIfNotRotated) {
//                    //TODO: Trim according to yaw
//                    move(dX, ddY, dZ);
//                }
//            } else {
//                move(dX, ddY, dZ);
//            }
//
//        }
        return getStatus();
    }

//    private void move(double dX, double dY, double dZ) {
//        if (getAgent() instanceof IJumpable)
//            moveJumpAble(dX, dY, dZ);
//        else
//            moveFlyAble(dX, dY, dZ);
//
//    }
//
//    private void moveJumpAble(double dX, double dY, double dZ) {
////        System.out.println("HDist: " + dX * dX + dZ * dZ + "// DY " + dY + "||" + ((IJumpable) getAgent()).getJumpTicks());
//        boolean shouldJump = ((dY > 0.0D) && (dX * dX + dZ * dZ) < 1.0D);
//        ((IJumpable) getAgent()).decrementJumpTicks();
//        Vector v = new Vector(dX, 0, dZ).normalize().multiply(getAgent().getMaxSpeed());
//        v.setY(getAgent().getVelocity().getY());
//
//        if (shouldJump && getAgent().isOnGround()) {
//            if (getAgent() instanceof IAnimatable) {
//                ((IAnimatable) getAgent()).resetAnimation("walk");
//                ((IAnimatable) getAgent()).pauseAnimation("walk");
//            }
//            if (((IJumpable) getAgent()).getJumpTicks() == 0) {
//                v.setY(v.getY() + ((IJumpable) getAgent()).getJumpStrength());
//                ((IJumpable) getAgent()).setJumpTicks(10);
//                if (getAgent() instanceof IAnimatable)
//                    ((IAnimatable) getAgent()).continueAnimation("walk");
//
//            }
//        }
//
//        getAgent().setVelocity(v);
//    }
//
//    private void moveFlyAble(double dX, double dY, double dZ) {
//        Vector v = new Vector(dX, dY, dZ).normalize().multiply(getAgent().getMaxSpeed());
//        getAgent().setVelocity(v);
//    }
//
//
//    private float trimTo(float value, float change, float maxAbs) {
//        if (change > 0)
//            return value - (maxAbs - change);
//        return value + (maxAbs - change);
//
////        return (value > 0) ? maxAbs : maxAbs * -1;
//    }
//
//    public boolean isStuck() {
//        double TimeTaken = System.currentTimeMillis() - m_StartTime;
//        return TimeTaken > m_TimeExpected;
//    }
//
//    private void setYaw(float y) {
//        Location t = getAgent().getLocation();
//        t.setYaw(y);
//        getAgent().setLocation(t);
//    }
//
//    private float a(float f, float f1, float f2) {
//        float f3 = g(f1 - f);
//
//        if (f3 > f2) {
//            f3 = f2;
//        }
//
//        if (f3 < -f2) {
//            f3 = -f2;
//        }
//
//        float f4 = f + f3;
//
//        if (f4 < 0.0F)
//            f4 += 360.0F;
//        else if (f4 > 360.0F) {
//            f4 -= 360.0F;
//        }
//
//        return f4;
//    }
//
//    private float g(float paramFloat) {
//        paramFloat %= 360.0F;
//        if (paramFloat >= 180.0F) {
//            paramFloat -= 360.0F;
//        }
//        if (paramFloat < -180.0F) {
//            paramFloat += 360.0F;
//        }
//        return paramFloat;
//    }
}
