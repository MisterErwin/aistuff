package gl.mc.mistererwin.galaxion.implementation.ai.goals;

import com.artemis.Entity;
import gl.mc.mistererwin.galaxion.api.ai.goals.GoalStatus;

/**
 * This goal does exactly nothing!
 * Created by mistererwin on 05.05.2015.
 *
 */
public class Goal_Wait extends Goal {


    public Goal_Wait(Entity pBot) {
        super(pBot, "Goal_Wait");
    }

    @Override
    public void activate() {
        super.activate();
    }

    @Override
    public void terminate() {
    }

    @Override
    public GoalStatus process() {
        activateIfInactive();
        return getStatus();
    }

}
