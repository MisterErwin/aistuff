package gl.mc.mistererwin.galaxion.implementation.animations;

import gl.mc.mistererwin.galaxion.api.animations.IAniObject;
import org.bukkit.util.EulerAngle;

public class AniObject implements IAniObject {
	private EulerAngle target = new EulerAngle(0, 0, 0);
	private EulerAngle current = new EulerAngle(0, 0, 0);

	private static final double PI2 = Math.PI*2;
	private static final double speed = Math.PI / 90 / 3 ;

	private int xDir, yDir, zDir;

	private double cSpeed = 1;
	@Override
	public AniObject setTarget(EulerAngle t) {
		this.target = null;
		this.target = lower(t);
		return this;
	}
	
	@Override
	public AniObject setSpeed(double s){
		this.cSpeed = s;
		return this;
	}

	private EulerAngle lower(EulerAngle a) {
		return new EulerAngle(mod(a.getX(),PI2), mod(a.getY(),PI2), mod(a.getZ(),PI2));
	}

	private int dir(double currentAngle, double targetAngle) {
		double D= targetAngle-currentAngle;
		return  ( (D>0) ^ (Math.abs(D)>Math.PI))?1:-1;
	}

	private void calc() {
		xDir = 0;
		yDir = 0;
		zDir = 0;

		if (current.getX()
				!= target.getX())
			xDir = dir(current.getX(), target.getX());
		if (current.getY()
				!= target.getY())
			yDir = dir(current.getY(), target.getY());

		if (current.getZ() 
				!= target.getZ())
			zDir = dir(current.getZ(), target.getZ());


	}

	@Override
	public void update() {
		if (this.target == null)
			this.target = new EulerAngle(0, 0, 0);
		this.calc();
		double s = speed*cSpeed;
		double nx = mod(current.getX()+s*xDir,PI2);
		double ny = mod(current.getY()+s*yDir,PI2);
		double nz = mod(current.getZ()+s*zDir,PI2);

		if (Math.abs(nx-target.getX())<0.01 || isBetween(current.getX(), nx, target.getX()))nx=target.getX();
		if (Math.abs(ny-target.getY())<0.01 || isBetween(current.getY(), ny, target.getY()))ny=target.getY();
		if (Math.abs(nz-target.getZ())<0.01 || isBetween(current.getZ(), nz, target.getZ()))nz=target.getZ();
	
		
		current = new EulerAngle(nx,
				ny, 
				nz);
	}

	public void update(double d) {
		if (this.target == null)
			this.target = new EulerAngle(0, 0, 0);
		this.calc();
		double s = speed*d;
		double nx = mod(current.getX()+s*xDir,PI2);
		double ny = mod(current.getY()+s*yDir,PI2);
		double nz = mod(current.getZ()+s*zDir,PI2);

		if (Math.abs(nx-target.getX())<0.01 || isBetween(current.getX(), target.getX(), nx))nx=target.getX();
		if (Math.abs(ny-target.getY())<0.01 || isBetween(current.getY(), target.getY(), ny))ny=target.getY();
		if (Math.abs(nz-target.getZ())<0.01 || isBetween(current.getZ(), target.getZ(), nz))nz=target.getZ();


		current = new EulerAngle(nx,
				ny,
				nz);
	}


	@Override
	public EulerAngle getCurrent() {
		return this.current;
	}
	
	@Override
	public EulerAngle getTarget() {
		return this.target;
	}

	private static boolean isBetween(double a, double b, double c) {
		return false;
//		return (a < b && b < c) || (a > b && b > c);
//	    return b > a ? c > a && c < b : c > b && c < a;
	}
	
	private static double mod(double a, double b)
	    {
	    	if (a < 0)
	    		return b + (a % b);
	    	else
	    		return a % b;
	    }

	@Override
	public void instant() {
		this.current = this.target.add(0,0,0);
		
	}

}
