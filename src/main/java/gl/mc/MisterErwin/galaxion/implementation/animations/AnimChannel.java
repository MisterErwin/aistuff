package gl.mc.mistererwin.galaxion.implementation.animations;

/**
 * Created by mistererwin on 14.07.2015.
 * In case you need it, ask me ;)
 */

import com.artemis.Entity;
import gl.mc.mistererwin.galaxion.components.animation.Animated;
import gl.mc.mistererwin.galaxion.components.animation.Animations;
import gl.mc.mistererwin.galaxion.components.character.AniPart;
import gl.mc.mistererwin.galaxion.components.character.Parted;
import gl.mc.mistererwin.galaxion.systems.physics.PartSystem;
import gl.mc.mistererwin.galaxion.utils.TempVars;
import gl.mc.mistererwin.galaxion.utils.math.FastMath;

import java.util.BitSet;

/**
 * <code>AnimChannel</code> provides controls, such as play, pause,
 * fast forward, etc, for an animation. The animation
 * channel may influence the entire model or specific bones of the model's
 * skeleton. A single model may have multiple animation channels influencing
 * various parts of its body. For example, a character model may have an
 * animation channel for its feet, and another for its torso, and
 * the animated for each channel are controlled independently.
 */
public class AnimChannel {

    private static final float DEFAULT_BLEND_TIME = 0.15f;

    private Animation animation;
    private Animation blendFrom;

    private BitSet affectedParts;

    private final Parted parted;
    private final Entity entity;

    private float time;
    private float speed;
    private float timeBlendFrom;
    private float blendTime;
    private float speedBlendFrom;
    private boolean notified = false;

    private LoopMode loopMode, loopModeBlendFrom;

    private float blendAmount = 1f;
    private float blendRate = 0;

    public AnimChannel(Parted control, Entity entityC) {
        parted = control;
        entity=entityC;
    }

    /**
     * @return The name of the currently playing animation, or null if
     * none is assigned.
     * @see AnimChannel#setAnim(java.lang.String, Animations)
     */
    public String getAnimationName() {
        return animation != null ? animation.getName() : null;
    }

    /**
     * @return The loop mode currently set for the animation. The loop mode
     * determines what will happen to the animation once it finishes
     * playing.
     * <p>
     * For more information, see the LoopMode enum class.
     * @see LoopMode
     * @see AnimChannel#setLoopMode(LoopMode)
     */
    public LoopMode getLoopMode() {
        return loopMode;
    }

    /**
     * @param loopMode Set the loop mode for the channel. The loop mode
     *                 determines what will happen to the animation once it finishes
     *                 playing.
     *                 <p>
     *                 For more information, see the LoopMode enum class.
     * @see LoopMode
     */
    public void setLoopMode(LoopMode loopMode) {
        this.loopMode = loopMode;
    }

    /**
     * @return The speed that is assigned to the animation channel. The speed
     * is a scale value starting from 0.0, at 1.0 the animation will play
     * at its default speed.
     * @see AnimChannel#setSpeed(float)
     */
    public float getSpeed() {
        return speed;
    }

    /**
     * @param speed Set the speed of the animation channel. The speed
     *              is a scale value starting from 0.0, at 1.0 the animation will play
     *              at its default speed.
     */
    public void setSpeed(float speed) {
        this.speed = speed;
        if (blendTime > 0) {
            this.speedBlendFrom = speed;
            blendTime = Math.min(blendTime, animation.getLength() / speed);
            blendRate = 1 / blendTime;
        }
    }

    /**
     * @return The time of the currently playing animation. The time
     * starts at 0 and continues on until getAnimMaxTime().
     * @see AnimChannel#setTime(float)
     */
    public float getTime() {
        return time;
    }

    /**
     * @param time Set the time of the currently playing animation, the time
     *             is clamped from 0 to {@link #getAnimMaxTime()}.
     */
    public void setTime(float time) {
        this.time = FastMath.clamp(time, 0, getAnimMaxTime());
    }

    /**
     * @return The length of the currently playing animation, or zero
     * if no animation is playing.
     * @see AnimChannel#getTime()
     */
    public float getAnimMaxTime() {
        return animation != null ? animation.getLength() : 0f;
    }


    /**
     * Set the current animation that is played by this AnimChannel.
     * <p>
     * This resets the time to zero, and optionally blends the animation
     * over <code>blendTime</code> seconds with the currently playing animation.
     * Notice that this method will reset the control's speed to 1.0.
     *
     * @param name      The name of the animation to play
     * @param blendTime The blend time over which to blend the new animation
     *                  with the old one. If zero, then no blending will occur and the new
     *                  animation will be applied instantly.
     */
    public void setAnim(String name, float blendTime, Animations animations) {
        if (name == null)
            throw new IllegalArgumentException("name cannot be null");

        if (blendTime < 0f)
            throw new IllegalArgumentException("blendTime cannot be less than zero");

//        Animation anim = control.animationMap.get(name);
        Animation anim = animations.animationMap.get(name);
        if (anim == null)
            throw new IllegalArgumentException("Cannot find animation named: '" + name + "'");

        for (AnimEventListener listener:entity.getComponent(Animated.class).listeners)
            listener.onAnimChange(entity,this,name);
//        control.notifyAnimChange(this, name);

        if (animation != null && blendTime > 0f) {
            this.blendTime = blendTime;
            // activate blending
            blendTime = Math.min(blendTime, anim.getLength() / speed);
            blendFrom = animation;
            timeBlendFrom = time;
            speedBlendFrom = speed;
            loopModeBlendFrom = loopMode;
            blendAmount = 0f;
            blendRate = 1f / blendTime;
        } else {
            blendFrom = null;
        }

        animation = anim;
        time = 0;
        speed = 1f;
        loopMode = LoopMode.Loop;
        notified = false;
    }

    /**
     * Set the current animation that is played by this AnimChannel.
     * <p>
     * See {@link #setAnim(java.lang.String, float, Animations)}.
     * The blendTime argument by default is 150 milliseconds.
     *
     * @param name The name of the animation to play
     */
    public void setAnim(String name, Animations animations) {
        setAnim(name, DEFAULT_BLEND_TIME, animations);
    }

    /**
     * Add all the bones of the model's skeleton to be
     * influenced by this animation channel.
     */
    public void addAllParts() {
        affectedParts = null;
    }

    /**
     * Add a single bone to be influenced by this animation channel.
     */
    public void addPart(String name) {
        addPart(parted.getPart(name));
    }

    /**
     * Add a single part to be influenced by this animation channel.
     */
    public void addPart(AniPart part) {
        int boneIndex = parted.getPartIndex(part);//control.getSkeleton().getBoneIndex(bone);
        if (affectedParts == null) {
            affectedParts = new BitSet(parted.getPartCount());
        }
        affectedParts.set(boneIndex);
    }

    /**
     * Add bones to be influenced by this animation channel starting from the
     * given bone name and going toward the root bone.
     */
    public void addToRootPart(String name) {
        addToRootPart(parted.getPart(name));
    }

    /**
     * Add bones to be influenced by this animation channel starting from the
     * given bone and going toward the root bone.
     */
    public void addToRootPart(AniPart part) {
        addPart(part);
        while (part.parent != null) {
            part = part.parent;
            addPart(part);
        }
    }

    /**
     * Add bones to be influenced by this animation channel, starting
     * from the given named bone and going toward its children.
     */
    public void addFromRootPart(String name) {
        addFromRootPart(parted.getPart(name));
    }

    /**
     * Add bones to be influenced by this animation channel, starting
     * from the given bone and going toward its children.
     */
    public void addFromRootPart(AniPart bone) {
        addPart(bone);
        if (bone.children == null)
            return;
        for (AniPart childBone : bone.children) {
            addPart(childBone);
            addFromRootPart(childBone);
        }
    }

    BitSet getAffectedParts() {
        return affectedParts;
    }

    public void update(float tpf, TempVars vars, Parted parted, PartSystem system) {
        if (animation == null)
            return;

        if (blendFrom != null && blendAmount != 1.0f) {
            // The blendFrom anim is set, the actual animation
            // playing will be set
//            blendFrom.setTime(timeBlendFrom, 1f, control, this, vars);
            blendFrom.setTime(timeBlendFrom, 1f - blendAmount, parted, this, system, vars);

            timeBlendFrom += tpf * speedBlendFrom;
            timeBlendFrom = AnimationUtils.clampWrapTime(timeBlendFrom,
                    blendFrom.getLength(),
                    loopModeBlendFrom);
            if (timeBlendFrom < 0) {
                timeBlendFrom = -timeBlendFrom;
                speedBlendFrom = -speedBlendFrom;
            }

            blendAmount += tpf * blendRate;
            if (blendAmount > 1f) {
                blendAmount = 1f;
                blendFrom = null;
            }
        }

        animation.setTime(time, blendAmount, parted, this, system, vars);

        if (animation.getLength() > 0) {
            if (!notified && (time >= animation.getLength() || time < 0)) {
                if (loopMode == LoopMode.DontLoop) {
                    // Note that this flag has to be set before calling the notify
                    // since the notify may start a new animation and then unset
                    // the flag.
                    notified = true;
                }
                for (AnimEventListener listener : entity.getComponent(Animated.class).listeners)
                    listener.onAnimCycleDone(system,entity,this,animation.getName());
//                system.notifyAnimCycleDone(this, animation.getName());
            }
        }
        time += tpf * speed;
        time = AnimationUtils.clampWrapTime(time, animation.getLength(), loopMode);
        if (time < 0) {
            // Negative time indicates that speed should be inverted
            // (for cycle loop mode only)
            time = -time;
            speed = -speed;
        }
    }

}
