package gl.mc.mistererwin.galaxion.implementation.animations;

import gl.mc.mistererwin.galaxion.components.character.Parted;
import gl.mc.mistererwin.galaxion.systems.physics.PartSystem;
import gl.mc.mistererwin.galaxion.utils.SafeArrayList;
import gl.mc.mistererwin.galaxion.utils.TempVars;

import java.util.Collections;
import java.util.stream.Collectors;

/**
 * Created by mistererwin on 14.07.2015.
 * In case you need it, ask me ;)
 */
public class Animation {

    /**
     * The name of the animation.
     */
    private String name;
    /**
     * The length of the animation.
     */
    private float length;
    /**
     * The tracks of the animation.
     */
    private SafeArrayList<PartTrack> tracks = new SafeArrayList<>(PartTrack.class);

    /**
     * Creates a new <code>Animation</code> with the given name and length.
     *
     * @param name The name of the animation.
     * @param length Length in seconds of the animation.
     */
    public Animation(String name, float length) {
        this.name = name;
        this.length = length;
    }

    /**
     * The name of the bone animation
     * @return name of the bone animation
     */
    public String getName() {
        return name;
    }

    /**
     * Returns the length in seconds of this animation
     *
     * @return the length in seconds of this animation
     */
    public float getLength() {
        return length;
    }

    public SafeArrayList<PartTrack> getTracksList() {
        return tracks;
    }

    /**
     * Set the {@link PartTrack}s to be used by this animation.
     *
     * @param tracksArray The tracks to set.
     */
    public void setTracks(PartTrack[] tracksArray) {
        Collections.addAll(tracks, tracksArray);
    }

    /**
     * Adds a track to this animation
     * @param track the track to add
     */
    public void addTrack(PartTrack track) {
        tracks.add(track);
    }

    /**
     * removes a track from this animation
     * @param track the track to remove
     */
    public void removeTrack(PartTrack track) {
        tracks.remove(track);
//        if (track instanceof ClonableTrack) {
//            ((ClonableTrack) track).cleanUp();
//        }
    }

    /**
     * Returns the tracks set in {@link #setTracks(PartTrack[]) }.
     *
     * @return the tracks set previously
     */
    public PartTrack[] getTracks() {
        return tracks.getArray();
    }

    /**
     * This method creates a clone of the current object.
     * @return a clone of the current object
     */
    @Override
    public Animation clone() {
        try {
            Animation result = (Animation) super.clone();
            result.tracks = new SafeArrayList<>(PartTrack.class);
            result.tracks.addAll(tracks.stream().map(PartTrack::clone).collect(Collectors.toList()));
            return result;
        } catch (CloneNotSupportedException e) {
            throw new AssertionError();
        }
    }

    /**
     * This method sets the current time of the animation.
     * This method behaves differently for every known track type.
     * Override this method if you have your own type of track.
     *
     * @param time the time of the animation
     * @param blendAmount the blend amount factor
     * @param channel the animation channel
     */
    void setTime(float time, float blendAmount, Parted parted, AnimChannel channel, PartSystem partSystem, TempVars vars) {
        if (tracks == null) {
            return;
        }

        for (PartTrack track : tracks) {
            track.setTime(time, blendAmount, channel, parted,partSystem, vars);
        }
    }



}
