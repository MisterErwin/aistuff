package gl.mc.mistererwin.galaxion.implementation.animations;

import gl.mc.mistererwin.galaxion.components.character.AniPart;
import gl.mc.mistererwin.galaxion.components.character.Parted;
import gl.mc.mistererwin.galaxion.systems.physics.PartSystem;
import gl.mc.mistererwin.galaxion.utils.TempVars;
import gl.mc.mistererwin.galaxion.utils.math.Quaternion;
import gl.mc.mistererwin.galaxion.utils.math.Vector3f;
import gl.mc.mistererwin.galaxion.utils.math.compact.CompactQuaternionArray;
import gl.mc.mistererwin.galaxion.utils.math.compact.CompactVector3Array;

import java.util.BitSet;

/**
 * Created by mistererwin on 14.07.2015.
 * In case you need it, ask me ;)
 */
public class PartTrack {
    private int targetBoneIndex;

    private CompactVector3Array translations;
    private CompactQuaternionArray rotations;
    private float[] times;

    /**
     * Creates a part track for the given bone index
     * @param targetBoneIndex the part id
     * @param times a float array with the time of each frame
     * @param translations the translation of the bone for each frame
     * @param rotations the rotation of the bone for each frame
     */
    public PartTrack(int targetBoneIndex, float[] times, Vector3f[] translations, Quaternion[] rotations) {
        this.targetBoneIndex = targetBoneIndex;
        this.setKeyframes(times, translations, rotations);
    }


    /**
     * Creates a part track for the given bone index
     * @param targetBoneIndex the bone's index
     */
    public PartTrack(int targetBoneIndex) {
        this.targetBoneIndex = targetBoneIndex;
    }

    /**
     * return the array of rotations of this track
     * @return
     */
    public Quaternion[] getRotations() {
        return rotations.toObjectArray();
    }


    public int getTargetBoneIndex() {
        return targetBoneIndex;
    }

    /**
     * returns the arrays of time for this track
     * @return
     */
    public float[] getTimes() {
        return times;
    }

    /**
     * returns the array of translations of this track
     * @return
     */
    public Vector3f[] getTranslations() {
        return translations.toObjectArray();
    }


    public void setKeyframes(float[] times, Vector3f[] translations, Quaternion[] rotations) {
        if (times.length == 0) {
            throw new RuntimeException("PartTrack with no keyframes!");
        }

        assert times.length == translations.length && times.length == rotations.length;

        this.times = times;
        this.translations = new CompactVector3Array();
        this.translations.add(translations);
        this.translations.freeze();
        this.rotations = new CompactQuaternionArray();
        this.rotations.add(rotations);
        this.rotations.freeze();
    }


    /**
     * This method creates a clone of the current object.
     * @return a clone of the current object
     */
    @Override
    public PartTrack clone() {
        int tablesLength = times.length;

        float[] times = this.times.clone();
        Vector3f[] sourceTranslations = this.getTranslations();
        Quaternion[] sourceRotations = this.getRotations();

        Vector3f[] translations = new Vector3f[tablesLength];
        Quaternion[] rotations = new Quaternion[tablesLength];
        Vector3f[] scales = new Vector3f[tablesLength];
        for (int i = 0; i < tablesLength; ++i) {
            translations[i] = sourceTranslations[i].clone();
            rotations[i] = sourceRotations[i].clone();
        }

        // Need to use the constructor here because of the final fields used in this class
        return new PartTrack(targetBoneIndex, times, translations, rotations);
    }

    /**
     *
     * Modify the bone which this track modifies in the skeleton to contain
     * the correct animation transforms for a given time.
     * The transforms can be interpolated in some method from the keyframes.
     *
     * @param time the current time of the animation
     * @param weight the weight of the animation
     * @param channel
     * @param vars
     */
    public void setTime(float time, float weight, AnimChannel channel, Parted parted, PartSystem system, TempVars vars) {
        BitSet affectedBones = channel.getAffectedParts();
        if (affectedBones != null && !affectedBones.get(targetBoneIndex)) {
            return;
        }

        AniPart target = parted.partList[targetBoneIndex];

        Vector3f tempV = vars.vect1;
        Vector3f tempS = vars.vect2;
        Quaternion tempQ = vars.quat1;
        Vector3f tempV2 = vars.vect3;
        Vector3f tempS2 = vars.vect4;
        Quaternion tempQ2 = vars.quat2;

        int lastFrame = times.length - 1;
        if (time < 0 || lastFrame == 0) {
            rotations.get(0, tempQ);
            translations.get(0, tempV);
//            if (scales != null) {
//                scales.get(0, tempS);
//            }
        } else if (time >= times[lastFrame]) {
            rotations.get(lastFrame, tempQ);
            translations.get(lastFrame, tempV);
//            if (scales != null) {
//                scales.get(lastFrame, tempS);
//            }
        } else {
            int startFrame = 0;
            int endFrame = 1;
            // use lastFrame so we never overflow the array
            int i;
            for (i = 0; i < lastFrame && times[i] < time; i++) {
                startFrame = i;
                endFrame = i + 1;
            }

            float blend = (time - times[startFrame])
                    / (times[endFrame] - times[startFrame]);

            rotations.get(startFrame, tempQ);
            translations.get(startFrame, tempV);
//            if (scales != null) {
//                scales.get(startFrame, tempS);
//            }
            rotations.get(endFrame, tempQ2);
            translations.get(endFrame, tempV2);
//            if (scales != null) {
//                scales.get(endFrame, tempS2);
//            }
            tempQ.nlerp(tempQ2, blend);
            tempV.interpolateLocal(tempV2, blend);
            tempS.interpolateLocal(tempS2, blend);
        }

//        if (weight != 1f) {

        system.blendAnimTransforms(target, tempV, tempQ,  weight);
//        } else {
//            target.setAnimTransforms(tempV, tempQ, scales != null ? tempS : null);
//        }
    }
}
