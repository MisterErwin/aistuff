package gl.mc.mistererwin.galaxion.implementation.generic;

import com.artemis.Component;

/**
 * Created by mistererwin on 15.07.2015.
 * In case you need it, ask me ;)
 */
public class Fallable extends Component {
    public float fallSpeed = 0.3f;
}
