package gl.mc.mistererwin.galaxion.implementation.generic;

import gl.mc.mistererwin.galaxion.utils.math.Quaternion2;
import gl.mc.mistererwin.galaxion.utils.math.Vector3;
import org.bukkit.entity.ArmorStand;
import org.bukkit.inventory.ItemStack;

/**
 * Created by mistererwin on 22.06.2015.
 * In case you need it, ask me ;)
 */
    public class RigidArmorStand extends RigidBody {

    public ArmorStand as;
    public RigidArmorStand[] children;

    public RigidArmorStand(Vector3 position, Quaternion2 orientation, double inverseMass, ItemStack item) {
        super(position, orientation, inverseMass);
    }
}
