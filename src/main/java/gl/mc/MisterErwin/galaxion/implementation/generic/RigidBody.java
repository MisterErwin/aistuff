package gl.mc.mistererwin.galaxion.implementation.generic;

import gl.mc.mistererwin.galaxion.utils.math.Matrix3;
import gl.mc.mistererwin.galaxion.utils.math.Matrix4;
import gl.mc.mistererwin.galaxion.utils.math.Quaternion2;
import gl.mc.mistererwin.galaxion.utils.math.Vector3;

/**
 * Created by mistererwin on 21.06.2015.
 * In case you need it, ask me ;)
 */
public class RigidBody{

    public RigidBody(Vector3 position, Quaternion2 orientation, double inverseMass) {
        this.position = position;
        this.orientation = orientation;
        this.inverseMass = inverseMass;

        transformMatrix=new Matrix4();

        this.canSleep=true;
        this.isAwake=false;


        this.motion = 0;
        this.rotation = new Vector3(0,0,0);
        this.velocity = new Vector3(0,0,0);

        this.acceleration = new Vector3(0,0,0);
        this.lastFrameAcceleration = new Vector3(0,0,0);
        this.torqueAccum = new Vector3(0,0,0);
        this.forceAccum = new Vector3(0,0,0);

//        this.inverseInertiaTensor =
//          this.inverseInertiaTensorWorld=
//        this.linearDamping =
//        this.angularDamping=
    }

    //linear position in world space
    public Vector3 position;
    //angular orientation in world space
    public Quaternion2 orientation;
    /**
     * Holds a localTransform matrix for converting body space into
     * world space and vice versa. This can be achieved by calling
     * the getPointIn*Space functions.
     *
     * @see gl.mc.mistererwin.galaxion.systems.physics.RigidBodySystem#getPointInLocalSpace
     * @see gl.mc.mistererwin.galaxion.systems.physics.RigidBodySystem#getPointInWorldSpace
     * @see #transformMatrix
     */
    final public Matrix4 transformMatrix;


    /**
     * Holds the inverse of the mass of the rigid body. It
     * is more useful to hold the inverse mass because
     * integration is simpler, and because in real time
     * simulation it is more useful to have bodies with
     * infinite mass (immovable) than zero mass
     * (completely unstable in numerical simulation).
     */
    public double inverseMass;
    /**
     * Holds the inverse of the body's inertia tensor. The
     * inertia tensor provided must not be degenerate
     * (that would mean the body had zero inertia for
     * spinning along one axis). As long as the tensor is
     * finite, it will be invertible. The inverse tensor
     * is used for similar reasons to the use of inverse
     * mass.
     * <p>
     * The inertia tensor, unlike the other variables that
     * define a rigid body, is given in body space.
     *
     * @see #inverseMass
     */
    public Matrix3 inverseInertiaTensor;

    /**
     * Holds the amount of damping applied to linear
     * motion.  Damping is required to remove energy added
     * through numerical instability in the integrator.
     */
    public double linearDamping;

    /**
     * Holds the amount of damping applied to angular
     * motion.  Damping is required to remove energy added
     * through numerical instability in the integrator.
     */
    public double angularDamping;

    /**
     * Holds the inverse inertia tensor of the body in world
     * space. The inverse inertia tensor member is specified in
     * the body's local space.
     *
     * @see #inverseInertiaTensor
     */
    public Matrix3 inverseInertiaTensorWorld;

    /**
     * Holds the amount of motion of the body. This is a recency
     * weighted mean that can be used to put a body to sleap.
     */
    public double motion;

    /**
     * Holds the linear velocity of the rigid body in
     * world space.
     */
    public Vector3 velocity;

    /**
     * Holds the angular velocity, or rotation, or the
     * rigid body in world space.
     */
    public Vector3 rotation;

    /**
     * A body can be put to sleep to avoid it being updated
     * by the integration functions or affected by collisions
     * with the world.
     */
    public boolean isAwake;

    /**
     * Some bodies may never be allowed to fall asleep.
     * User controlled bodies, for example, should be
     * always awake.
     */
    public boolean canSleep;

    /**
     * Holds the accumulated force to be applied at the next
     * integration step.
     */
    public Vector3 forceAccum;

    /**
     * Holds the accumulated torque to be applied at the next
     * integration step.
     */
    public Vector3 torqueAccum;

    /**
     * Holds the acceleration of the rigid body.  This value
     * can be used to set acceleration due to gravity (its primary
     * use), or any other ant acceleration.
     */
    public Vector3 acceleration;

    /**
     * Holds the linear acceleration of the rigid body, for the
     * previous frame.
     */
    public Vector3 lastFrameAcceleration;



}
