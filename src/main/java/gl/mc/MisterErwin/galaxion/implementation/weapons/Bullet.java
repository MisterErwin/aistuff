package gl.mc.mistererwin.galaxion.implementation.weapons;

import com.artemis.Component;
import com.artemis.Entity;
import gl.mc.mistererwin.galaxion.AIStuff;
import gl.mc.mistererwin.galaxion.utils.AWorldUtils;
import org.bukkit.Location;
import org.bukkit.entity.*;
import org.bukkit.inventory.ItemStack;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.util.Vector;

/**
 * Created by mistererwin on 24.05.2015.
 * In case you need it, ask me ;)
 */
public class Bullet extends Component {
    public int ticks;
    public int releaseTime;
    public boolean dead;
    public boolean active;
    public boolean released;
    public boolean destroyed;
    public boolean destroyNextTick;

    public org.bukkit.entity.Entity projectile;
    public Vector velocity;
    public Location lastLocation;
    public Location startLocation;
    public Entity shooter;
    public Weapon shotFrom;


    public Bullet(Entity pShooter, Weapon pShotFrom, Vector pVelocity) {
        this.shooter = pShooter;
        this.shotFrom = pShotFrom;
        this.velocity = pVelocity;
        active = true;

        if (shotFrom.stats.isThrowable) {
            ItemStack thrown = shotFrom.stats.gun.newItemStack(1);
            org.bukkit.entity.LivingEntity e = (org.bukkit.entity.LivingEntity) AWorldUtils.getEntity(shooter);

            this.projectile = e.getWorld().dropItem(e.getEyeLocation(), thrown);
            ((Item) this.projectile).setPickupDelay(9999999);
            this.startLocation = this.projectile.getLocation();
        } else {
            Class mclass = Snowball.class;
            String check = shotFrom.stats.projType.toLowerCase().replaceAll("_", "").replaceAll(" ", "");
            switch (check) {
                case "arrow":
                    mclass = Arrow.class;
                    break;
                case "egg":
                    mclass = Egg.class;
                    break;
                case "enderpearl":
                    mclass = EnderPearl.class;
                    break;
                case "fireball":
                    mclass = Fireball.class;
                    break;
                case "fish":
                case "fishhook":
                    mclass = FishHook.class;
                    break;
                case "largefireball":
                    mclass = LargeFireball.class;
                    break;
                case "smallfireball":
                    mclass = SmallFireball.class;
                    break;
                case "thrownexpbottle":
                    mclass = ThrownExpBottle.class;
                    break;
                case "thrownpotion":
                    mclass = ThrownPotion.class;
                    break;
                case "witherskull":
                    mclass = WitherSkull.class;
                    break;
            }
            org.bukkit.entity.LivingEntity be = (org.bukkit.entity.LivingEntity) AWorldUtils.getEntity(shooter);
            this.projectile = be.launchProjectile(mclass);
            ((Projectile) this.projectile).setShooter(be);
            this.projectile.setMetadata("scenarioName", new FixedMetadataValue(AIStuff.instance, be.getMetadata("scenarioName").get(0).asString()));

            this.startLocation = this.projectile.getLocation();
        }

        if (shotFrom.stats.releaseTime == -1) {
            this.releaseTime = (80 + (shotFrom.stats.isThrowable ? 0 : 1) * 400);
        } else {
            this.releaseTime = shotFrom.stats.releaseTime;
        }
    }



}
