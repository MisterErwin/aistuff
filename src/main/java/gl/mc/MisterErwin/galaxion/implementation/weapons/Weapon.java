package gl.mc.mistererwin.galaxion.implementation.weapons;

import com.artemis.Entity;
import com.artemis.managers.PlayerManager;
import com.artemis.utils.EntityBuilder;
import gl.mc.mistererwin.galaxion.api.weapons.SoundData;
import gl.mc.mistererwin.galaxion.components.combat.Aim;
import gl.mc.mistererwin.galaxion.components.combat.AmmoHolder;
import gl.mc.mistererwin.galaxion.components.generic.PlayerEntity;
import gl.mc.mistererwin.galaxion.utils.AWorldUtils;
import gl.mc.mistererwin.galaxion.utils.InventoryUtils;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

import java.util.Random;

/**
 * Created by mistererwin on 24.05.2015.
 * In case you need it, ask me ;)
 */
public class Weapon {

    public final WeaponStats stats;

    public int gunReloadTimer;
    public int timer;
    public int lastFired;
    public int ticks;
    public int heldDownTicks;
    public int bulletsShot;
    public int roundsFired;
    public boolean firing = false;
    public boolean reloading;
    public boolean changed = false;


    public final Entity holder;

    public void shoot() {
        if (holder == null /* or is dead/offline*/ || this.reloading)
            return;
        if (stats.ammoAmtNeeded == 0 || hasAmmo(holder)) {
            removeAmmo(holder);
            if (this.roundsFired >= stats.maxClipSize && stats.hasClip) {
                reloadGun();
                return;
            }
            doRecoil(holder);

            this.changed = true;
            this.roundsFired++;
            Location pLoc = AWorldUtils.getLocation(holder);

            // Sounds
            if (stats.firedSound != null)
                for (SoundData sd : stats.firedSound)
                    sd.play(pLoc);

            double accuracy = stats.accuracy;
            Aim aim;
            if (stats.accuracy_aimed > -1.0D && ((aim = holder.getComponent(Aim.class)) != null) && aim.isAiming)
                accuracy = stats.accuracy_aimed;

            int acc = (int) (accuracy * 1000.0D);

            if (acc <= 0)
                acc = 1;
            Random rand = new Random();
            double dir = -pLoc.getYaw() - 90.0F;
            double pitch = -pLoc.getPitch();

            for (int i = 0; i < stats.bulletsPerClick; i++) {
                double xwep = (rand.nextInt(acc) - rand.nextInt(acc) + 0.5D) / 1000.0D;
                double ywep = (rand.nextInt(acc) - rand.nextInt(acc) + 0.5D) / 1000.0D;
                double zwep = (rand.nextInt(acc) - rand.nextInt(acc) + 0.5D) / 1000.0D;
                double xd = Math.cos(Math.toRadians(dir))
                        * Math.cos(Math.toRadians(pitch)) + xwep;
                double yd = Math.sin(Math.toRadians(pitch)) + ywep;
                double zd = -Math.sin(Math.toRadians(dir))
                        * Math.cos(Math.toRadians(pitch)) + zwep;
                Vector vec = new Vector(xd, yd, zd);
                vec.multiply(this.stats.bulletSpeed);
                Bullet b = new Bullet(this.holder, this, vec);
                new EntityBuilder(holder.getWorld()).with(b).UUID(b.projectile.getUniqueId()).build();
            }
            if (this.roundsFired >= stats.maxClipSize && stats.hasClip)
                reloadGun();
        } else {
            if (holder.getComponent(PlayerEntity.class) != null) {
                Player p = Bukkit.getPlayerExact(holder.getWorld().getManager(PlayerManager.class).getPlayer(holder));
                p.playSound(p.getLocation(), Sound.ITEM_BREAK, 20.0F, 20.0F);
                p.sendMessage(stats.outOfAmmoMessage);
            }
            finishShooting();
        }
    }

    private boolean hasAmmo(Entity e) {

        AmmoHolder ah = e.getComponent(AmmoHolder.class);
        if (ah != null) {
            if (ah.infinite)
                return true;
            Integer n = ah.ammo.get(stats.amm);
            return (n != null && n >= stats.ammoAmtNeeded);
        }
        if (e.getComponent(PlayerEntity.class) != null) {
            Player p = Bukkit.getPlayerExact(e.getWorld().getManager(PlayerManager.class).getPlayer(e));
            return (InventoryUtils.amount(p, stats.amm) >= stats.ammoAmtNeeded);
        }
        return false;
    }

    private void removeAmmo(Entity e) {
        if (stats.ammoAmtNeeded <= 0) return;
        AmmoHolder ah = e.getComponent(AmmoHolder.class);
        if (ah != null) {
            if (ah.infinite)
                return;
            Integer n = ah.ammo.get(stats.amm);
            if (n != null)
                ah.ammo.put(stats.amm, Math.max(0, n - stats.ammoAmtNeeded));
        } else if (e.getComponent(PlayerEntity.class) != null) {
            Player p = Bukkit.getPlayerExact(e.getWorld().getManager(PlayerManager.class).getPlayer(e));
            InventoryUtils.remove(p, stats.amm, stats.ammoAmtNeeded);
        }
    }

    private void doRecoil(Entity gh) {
        if (stats.recoil != 0.0D) {
            org.bukkit.entity.Entity e = AWorldUtils.getEntity(gh);
            Location pLoc = e.getLocation();
            double dir = -pLoc.getYaw() - 90.0F;
            double pitch = -pLoc.getPitch() - 180.0F;
            double xd = Math.cos(Math.toRadians(dir))
                    * Math.cos(Math.toRadians(pitch));
            double yd = Math.sin(Math.toRadians(pitch));
            double zd = -Math.sin(Math.toRadians(dir))
                    * Math.cos(Math.toRadians(pitch));
            Vector vec = new Vector(xd, yd, zd);
            vec.multiply(stats.recoil / 2.0D).setY(0);
            e.setVelocity(e.getVelocity().add(vec));
        }
    }

    public void doKnockback(org.bukkit.entity.Entity entity, Vector speed) {
        if (stats.knockback > 0.0D) {
            speed.normalize().setY(0.6D).multiply(stats.knockback / 4.0D);
            entity.setVelocity(speed);
        }
    }

    public void finishReloading() {
        this.bulletsShot = 0;
        this.roundsFired = 0;
        this.changed = false;
        this.gunReloadTimer = 0;
    }

    private void finishShooting() {
        this.bulletsShot = 0;
        this.timer = stats.bulletDelayTime;
        this.firing = false;
    }

    public void reloadGun() {
        this.reloading = true;
        this.gunReloadTimer = stats.reloadTime;
    }

    private void gunSounds() {
        if (this.reloading) {
            int amtReload = stats.reloadTime - this.gunReloadTimer;
            if (stats.reloadType.equalsIgnoreCase("bolt")) {
                if (amtReload == 6)
                    playSoundPlayer(holder,
                            Sound.DOOR_OPEN, 2.0F, 1.5F);
                if (amtReload == stats.reloadTime - 4)
                    playSoundPlayer(holder,
                            Sound.DOOR_CLOSE, 1.0F, 1.5F);
            } else if ((stats.reloadType.equalsIgnoreCase("pump"))
                    || (stats.reloadType.equals("INDIVIDUAL_BULLET"))) {
                int rep = (stats.reloadTime - 10) / stats.maxClipSize;
                if ((amtReload >= 5) && (amtReload <= stats.reloadTime - 5)
                        && (amtReload % rep == 0)) {
                    playSoundPlayer(holder,
                            Sound.NOTE_STICKS, 1.0F, 1.0F);
                    playSoundPlayer(holder,
                            Sound.NOTE_SNARE_DRUM, 1.0F, 2.0F);
                }

                if (amtReload == stats.reloadTime - 3)
                    playSoundPlayer(holder,
                            Sound.PISTON_EXTEND, 1.0F, 2.0F);
                if (amtReload == stats.reloadTime - 1)
                    playSoundPlayer(holder,
                            Sound.PISTON_RETRACT, 1.0F, 2.0F);
            } else {
                if (amtReload == 6) {
                    playSoundPlayer(holder,
                            Sound.FIRE_IGNITE, 2.0F, 2.0F);
                    playSoundPlayer(holder,
                            Sound.DOOR_OPEN, 1.0F, 2.0F);
                }
                if (amtReload == stats.reloadTime / 2)
                    playSoundPlayer(holder,
                            Sound.PISTON_RETRACT, 0.33F, 2.0F);
                if (amtReload == stats.reloadTime - 4) {
                    playSoundPlayer(holder,
                            Sound.FIRE_IGNITE, 2.0F, 2.0F);
                    playSoundPlayer(holder,
                            Sound.DOOR_CLOSE, 1.0F, 2.0F);
                }
            }
        } else {
            if (stats.reloadType.equalsIgnoreCase("pump")) {
                if (this.timer == 8)
                    playSoundPlayer(holder,
                            Sound.PISTON_EXTEND, 1.0F, 2.0F);
                if (this.timer == 6) {
                    playSoundPlayer(holder,
                            Sound.PISTON_RETRACT, 1.0F, 2.0F);
                }
            }
            if (stats.reloadType.equalsIgnoreCase("bolt")) {
                if (this.timer == stats.bulletDelayTime - 4)
                    playSoundPlayer(holder,
                            Sound.DOOR_OPEN, 2.0F, 1.25F);
                if (this.timer == 6)
                    playSoundPlayer(holder,
                            Sound.DOOR_CLOSE, 1.0F, 1.25F);
            }
        }
    }

    public void tick() {
        this.ticks += 1;
        this.lastFired += 1;
        this.timer -= 1;
        this.gunReloadTimer -= 1;

        if (this.gunReloadTimer < 0) {
            if (this.reloading) {
                finishReloading();
            }
            this.reloading = false;
        }

        gunSounds();

        if (this.lastFired > 6) {
            this.heldDownTicks = 0;
        }
        if (((this.heldDownTicks >= 2) && (this.timer <= 0))
                || ((this.firing) && (!this.reloading))) {
            if (stats.roundsPerBurst > 1) {
                if (this.ticks % stats.bulletDelay == 0) {
                    this.bulletsShot += 1;
                    if (this.bulletsShot <= stats.roundsPerBurst)
                        shoot();
                    else
                        finishShooting();
                }
            } else {
                shoot();
                finishShooting();
            }
        }

        if (this.reloading)
            this.firing = false;
    }


    public Weapon(WeaponStats stats, Entity pHolder) {
        this.stats = stats;
        this.holder = pHolder;
    }
//
//    public Weapon copy() {
//        Weapon w = new Weapon(stats);
//        return w;
//    }

    private static void playSoundPlayer(Entity holder, Sound s, float v, float p) {
        org.bukkit.entity.Entity e = AWorldUtils.getEntity(holder);
        if (e instanceof Player)
            ((Player) e).playSound(e.getLocation(), s, v, p);
    }

    private static void playSoundAtPlayer(Entity holder, Sound s, float v, float p) {
        Location loc = AWorldUtils.getLocation(holder);
        if (loc != null)
            loc.getWorld().playSound(loc, s, v, p);
    }
}
