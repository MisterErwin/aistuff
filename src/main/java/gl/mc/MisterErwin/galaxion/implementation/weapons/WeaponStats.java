package gl.mc.mistererwin.galaxion.implementation.weapons;

import gl.mc.mistererwin.galaxion.api.weapons.ParticleEffectData;
import gl.mc.mistererwin.galaxion.api.weapons.SoundData;
import gl.mc.mistererwin.galaxion.utils.MyMaterial;

/**
 * Created by mistererwin on 24.05.2015.
 * In case you need it, ask me ;)
 */
public class WeaponStats  {
    public String gunID;

    public boolean canHeadShot;
    public boolean isThrowable = false;
    public boolean hasSmokeTrail;
    public boolean localGunSound;
    public boolean canAimLeft;
    public boolean canAimRight;
    public boolean canGoPastMaxDistance;
    public MyMaterial amm;
    public  MyMaterial gun;
    public int ammoAmtNeeded;
    public double gunDamage;
    public double explosionDamage = -1;
    public int roundsPerBurst;
    public int reloadTime;
    public int maxDistance;
    public int bulletsPerClick;
    public int bulletDelay = 2;
    public int armorPenetration;
    public int releaseTime = -1;
    public double bulletSpeed;
    public double accuracy;
    public double accuracy_aimed = -1.0D;
    public double explodeRadius;
    public double fireRadius;
    public double flashRadius;
    public double knockback;
    public double recoil;
    public String gunName;
    public String projType = "";
    public ParticleEffectData[] trailParticles;
    public ParticleEffectData[] hitParticles;
    public SoundData[] firedSound;
    public SoundData[] hitSound;
    public String outOfAmmoMessage = "";
    public boolean canClickRight;
    public boolean canClickLeft;
    public boolean hasClip = true;
    public boolean ignoreItemData = false;
    public boolean reloadGunOnDrop = true;
    public int maxClipSize = 30;
    public int bulletDelayTime = 10;
    public String node;
    public String reloadType = "NORMAL";
    public double headshootmultiplier = 1.5;
    public String explosionType;

    public WeaponStats(String ID) {
        this.gunID = ID;
    }


}
