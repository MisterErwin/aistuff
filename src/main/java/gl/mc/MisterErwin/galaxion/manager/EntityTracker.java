package gl.mc.mistererwin.galaxion.manager;

import com.artemis.Component;
import com.artemis.Entity;
import com.artemis.Manager;
import com.artemis.utils.Bag;
import gl.mc.mistererwin.galaxion.components.ai.ArmorStandEntitiy;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by mistererwin on 23.05.2015.
 * In case you need it, ask me ;)
 */
public class EntityTracker extends Manager {

    private Bag<Entity> entities = new Bag<>();

    @Override
    public void added(Entity e) {
        entities.add(e);
    }

    @Override
    public void deleted(Entity e) {
        entities.remove(e);
        if (e.getComponent(ArmorStandEntitiy.class) != null)
            e.getComponent(ArmorStandEntitiy.class).entity.remove();

    }

    public int size(){
        return entities.size();
    }

    public Entity[] getEntities() {
        ArrayList<Entity> ret = new ArrayList<>(entities.size());
        for (Entity e : entities)
            ret.add(e);
        return ret.toArray(new Entity[ret.size()]);
    }

    public Entity getEntity(int i) {
        return entities.get(i);
    }


    public LinkedList<Entity> getWithComponent(Class<? extends Component> type) {
        LinkedList<Entity> ret = new LinkedList<>();
        for (Entity e : entities)
            if (e.getComponent(type) != null)
                ret.add(e);
        return ret;
    }

    public <T extends Component> List<T> getComWithComponent(Class<T> type) {
        LinkedList<T> ret = new LinkedList<>();
        T c;
        for (Entity e : entities)
            if ((c = e.getComponent(type)) != null)
                ret.add(c);
        return ret;
    }

    public <T extends Component> List<AbstractMap.SimpleEntry<Entity, T>> getBothWithComponent(Class<T> type) {
        LinkedList<AbstractMap.SimpleEntry<Entity, T>> ret = new LinkedList<>();
        T c;
        for (Entity e : entities)
            if ((c = e.getComponent(type)) != null)
                ret.add(new AbstractMap.SimpleEntry<Entity, T>(e, c));
        return ret;
    }


    @SafeVarargs
    public final LinkedList<Entity> getWithComponents(Class<? extends Component>... type) {
        LinkedList<Entity> ret = new LinkedList<>();
        for (Entity e : entities) {
            boolean ok = true;
            for (Class<? extends Component> t : type)
                if (e.getComponent(t) == null) {
                    ok = false;
                    break;
                }
            if (ok)
                ret.add(e);

        }
        return ret;
    }


    public void removeAll() {
        for (Entity e : entities) {
//            if (e.getComponent(ArmorStandEntitiy.class) != null)
//                e.getComponent(ArmorStandEntitiy.class).entity.remove();
//            removeAP(e.getComponent(AniPart.class));


            e.deleteFromWorld();
        }
    }

//    private void removeAP(AniPart ap){
//        if (ap == null)return;
//        ap.as.remove();
//        if (ap.children!=null)
//            for (AniPart ip : ap.children)
//                removeAP(ip);
//    }


}
