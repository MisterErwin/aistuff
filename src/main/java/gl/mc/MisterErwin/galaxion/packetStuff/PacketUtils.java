package gl.mc.mistererwin.galaxion.packetStuff;

import gl.mc.mistererwin.galaxion.AIStuff;
import net.minecraft.server.v1_8_R2.*;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_8_R2.entity.CraftEntity;
import org.bukkit.craftbukkit.v1_8_R2.entity.CraftPlayer;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.lang.reflect.AccessibleObject;
import java.lang.reflect.Field;
import java.util.List;

//import com.comphenix.protocol.wrappers.WrappedDataWatcher;

public class PacketUtils {
	public static void sendPacket(Player player, Packet packet) {
		if (player == null || packet == null)
			return;
		EntityPlayer entityPlayer = ((CraftPlayer) player).getHandle();
		entityPlayer.playerConnection.sendPacket(packet);
	}

	public static void resendEntityLocation(Entity e){
		PacketPlayOutEntityTeleport packetPlayOutEntityTeleport = new PacketPlayOutEntityTeleport(((CraftEntity)e).getHandle());
		Bukkit.getOnlinePlayers().stream().filter(p -> p.getLocation().distanceSquared(e.getLocation()) < 22500).forEach(p -> sendPacket(p, packetPlayOutEntityTeleport));
	}

	public static Field getField(Class<?> cl, String field_name) {
		try {
			Field field = cl.getDeclaredField(field_name);
			return field;
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (NoSuchFieldException e) {
			e.printStackTrace();
		}
		return null;
	}

	// public static PacketPlayOutEntityTeleport getTeleportEntityPacket(int
	// entityID, Location loc) {
	// return new PacketPlayOutEntityTeleport(entityID,
	// MathHelper.floor(loc.getX()*32), MathHelper.floor(loc.getY()*32),
	// MathHelper.floor(loc.getZ()*32), ((byte)(int)(loc.getYaw() * 256.0F /
	// 360.0F)), ((byte)(int)(loc.getPitch() * 256.0F / 360.0F)), false, false);
	// }

	public static PacketPlayOutEntity.PacketPlayOutRelEntityMove getRelMoveEntityPacket(
			int entityID, double x, double y, double z) {
		return new PacketPlayOutEntity.PacketPlayOutRelEntityMove(entityID,
				(byte) (MathHelper.floor(x * 32)),
				(byte) (MathHelper.floor(y * 32)),
				(byte) (MathHelper.floor(z * 32)), false);
	}

	// Accessing packets
	@SuppressWarnings("deprecation")
	public static PacketPlayOutSpawnEntityLiving getMobPacket(Location loc,
			int ENTITY_ID, int entityID, DataWatcher watcher) {
		PacketPlayOutSpawnEntityLiving mobPacket = new PacketPlayOutSpawnEntityLiving();

		try {
			Field a = getField(mobPacket.getClass(), "a");
			a.setAccessible(true);
			a.set(mobPacket, ENTITY_ID);

			Field b = getField(mobPacket.getClass(), "b");
			b.setAccessible(true);
			b.set(mobPacket, (byte) entityID);

			Field c = getField(mobPacket.getClass(), "c");
			c.setAccessible(true);
			c.set(mobPacket, (int) Math.floor(loc.getBlockX() * 32.0D));

			Field d = getField(mobPacket.getClass(), "d");
			d.setAccessible(true);
			d.set(mobPacket, (int) Math.floor(loc.getBlockY() * 32.0D));

			Field e = getField(mobPacket.getClass(), "e");
			e.setAccessible(true);
			e.set(mobPacket, (int) Math.floor(loc.getBlockZ() * 32.0D));

			Field f = getField(mobPacket.getClass(), "f");
			f.setAccessible(true);
			f.set(mobPacket, (byte) 0);

			Field g = getField(mobPacket.getClass(), "g");
			g.setAccessible(true);
			g.set(mobPacket, (byte) 0);

			Field h = getField(mobPacket.getClass(), "h");
			h.setAccessible(true);
			h.set(mobPacket, (byte) 0);

			Field i = getField(mobPacket.getClass(), "i");
			i.setAccessible(true);
			i.set(mobPacket, (byte) 0);

			Field j = getField(mobPacket.getClass(), "j");
			j.setAccessible(true);
			j.set(mobPacket, (byte) 0);

			Field k = getField(mobPacket.getClass(), "k");
			k.setAccessible(true);
			k.set(mobPacket, (byte) 0);

		} catch (IllegalArgumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IllegalAccessException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		// DataWatcher watcher = getWatcher(text, 300);

		try {
			Field t = PacketPlayOutSpawnEntityLiving.class
					.getDeclaredField("l");
			t.setAccessible(true);
			t.set(mobPacket, watcher);
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return mobPacket;
	}

	public static PacketPlayOutSpawnEntityLiving getLocatedMobPacket(
			Location loc, int ENTITY_ID, int TypeID, DataWatcher watcher) {
		PacketPlayOutSpawnEntityLiving mobPacket = new PacketPlayOutSpawnEntityLiving();

		try {
			Field a = getField(mobPacket.getClass(), "a");
			a.setAccessible(true);
			a.set(mobPacket, ENTITY_ID);

			Field b = getField(mobPacket.getClass(), "b");
			b.setAccessible(true);
			b.set(mobPacket, (byte) TypeID);

			Field c = getField(mobPacket.getClass(), "c");
			c.setAccessible(true);
			c.set(mobPacket, (int) Math.floor(loc.getX() * 32.0D));

			Field d = getField(mobPacket.getClass(), "d");
			d.setAccessible(true);
			d.set(mobPacket, (int) Math.floor(loc.getY() * 32.0D));

			Field e = getField(mobPacket.getClass(), "e");
			e.setAccessible(true);
			e.set(mobPacket, (int) Math.floor(loc.getZ() * 32.0D));

			Field f = getField(mobPacket.getClass(), "f"); // MOT
			f.setAccessible(true);
			f.set(mobPacket, (byte) 0);

			Field g = getField(mobPacket.getClass(), "g"); // MOT
			g.setAccessible(true);
			g.set(mobPacket, (byte) 0);

			Field h = getField(mobPacket.getClass(), "h"); // MOT
			h.setAccessible(true);
			h.set(mobPacket, (byte) 0);

			Field i = getField(mobPacket.getClass(), "i"); // jaw
			i.setAccessible(true);
			i.set(mobPacket, (byte) ((int) (loc.getYaw() * 256.0F / 360.0F)));

			Field j = getField(mobPacket.getClass(), "j"); // pitch
			j.setAccessible(true);
			j.set(mobPacket, (byte) ((int) (loc.getPitch() * 256.0F / 360.0F)));

			Field k = getField(mobPacket.getClass(), "k");
			k.setAccessible(true);
			k.set(mobPacket, (byte) 0);

		} catch (IllegalArgumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IllegalAccessException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		// DataWatcher watcher = getWatcher(text, 300);

		try {
			Field t = PacketPlayOutSpawnEntityLiving.class
					.getDeclaredField("l");
			t.setAccessible(true);
			t.set(mobPacket, watcher);
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return mobPacket;
	}

	public static PacketPlayOutEntityMetadata getArmorStandMetaDataMobPacket(
			int ENTITY_ID, List<DataWatcher.WatchableObject> watcherList) {
		PacketPlayOutEntityMetadata packet = new PacketPlayOutEntityMetadata();

		try {
			Field a = getField(packet.getClass(), "a"); // EntityID ?
			a.setAccessible(true);
			a.set(packet, ENTITY_ID);
		} catch (IllegalArgumentException e1) {
			e1.printStackTrace();
		} catch (IllegalAccessException e1) {
			e1.printStackTrace();
		}
		try {
			Field b = getField(packet.getClass(), "b"); // List
			b.setAccessible(true);
			b.set(packet, watcherList);
		} catch (IllegalArgumentException e1) {
			e1.printStackTrace();
		} catch (IllegalAccessException e1) {
			e1.printStackTrace();
		}
		return packet;

	}

	public static PacketPlayOutEntityDestroy getDestroyEntityPacket(
			int ENTITY_ID) {
		PacketPlayOutEntityDestroy packet = new PacketPlayOutEntityDestroy();

		Field a = getField(packet.getClass(), "a");
		a.setAccessible(true);
		try {
			a.set(packet, new int[] { ENTITY_ID });
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}

		return packet;
	}

	public static PacketPlayOutEntityMetadata getMetadataPacket(
			DataWatcher watcher, int ENTITY_ID) {
		PacketPlayOutEntityMetadata metaPacket = new PacketPlayOutEntityMetadata();

		Field a = getField(metaPacket.getClass(), "a");
		a.setAccessible(true);
		try {
			a.set(metaPacket, ENTITY_ID);
		} catch (IllegalArgumentException e1) {
			e1.printStackTrace();
		} catch (IllegalAccessException e1) {
			e1.printStackTrace();
		}

		try {
			Field b = PacketPlayOutEntityMetadata.class.getDeclaredField("b");
			b.setAccessible(true);
			b.set(metaPacket, watcher.c());
		} catch (Exception e) {
			e.printStackTrace();
		}

		return metaPacket;
	}

	public static PacketPlayInClientCommand getRespawnPacket() {
		PacketPlayInClientCommand packet = new PacketPlayInClientCommand();

		Field a = getField(packet.getClass(), "a");
		a.setAccessible(true);
		try {
			a.set(packet, 1);
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}

		return packet;
	}

	public static DataWatcher getWatcher(String text, int health) {
		DataWatcher watcher = new DataWatcher(null);

		watcher.a(0, (byte) 0x20); // Flags, 0x20 = invisible
		watcher.a(6, (float) health);
		watcher.a(10, text); // Entity name
		watcher.a(11, (byte) 1); // Show name, 1 = show, 0 = don't show
		// watcher.a(16, (Integer) (int) health); //Wither health, 300 = full
		// health

		return watcher;
	}

	// Other methods
	public static void showBeam(Location a, Location b, final Player p,
			long remotime) {
		DataWatcher gwatcher = new DataWatcher(null);
		gwatcher.a(0, (byte) 0x20); // Flags, 0x20 = invisibl
		gwatcher.a(6, (float) 2); // HP
		gwatcher.a(17, 1234); // SQUID ID

		PacketPlayOutSpawnEntityLiving gpacket = getMobPacket(a, 1233, 68,
				gwatcher); // 68 => Guardian

		DataWatcher swatcher = new DataWatcher(null);
		swatcher.a(0, (byte) 0x20); // Flags, 0x20 = invisibl
		swatcher.a(6, (float) 2); // HP

		PacketPlayOutSpawnEntityLiving spacket = getMobPacket(b, 1234, 94,
				swatcher); // 94 => Squid

		sendPacket(p, spacket);
		sendPacket(p, gpacket);

		if (remotime > 0)
			new BukkitRunnable() {
				@Override
				public void run() {
					sendPacket(p, getDestroyEntityPacket(1233));
					sendPacket(p, getDestroyEntityPacket(1234));
				}
			}.runTaskLater(AIStuff.instance, remotime);
	}

	// public static WrappedDataWatcher getDefaultWatcher(org.bukkit.World
	// world, EntityType type) {
	// Entity entity = world.spawnEntity(new Location(world, 0, 256, 0), type);
	// WrappedDataWatcher watcher =
	// WrappedDataWatcher.getEntityWatcher(entity).deepClone();
	//
	// entity.remove();
	// return watcher;
	// }
	//
	public static <T extends AccessibleObject> AccessibleObject setAccessible(
			Class<T> objectType, AccessibleObject object, boolean accessible) {
		object.setAccessible(accessible);
		return object;
	}

}
