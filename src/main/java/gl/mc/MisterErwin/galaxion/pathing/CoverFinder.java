package gl.mc.mistererwin.galaxion.pathing;

@Deprecated
public class CoverFinder {//extends BukkitRunnable{
	
//
//	private static final BlockFace[] faces = new BlockFace[]{BlockFace.NORTH,BlockFace.NORTH_EAST, BlockFace.EAST,BlockFace.SOUTH_EAST,BlockFace.SOUTH,BlockFace.SOUTH_WEST,BlockFace.WEST,BlockFace.NORTH_WEST};
//	private final int radius = 10;
//	private int cr = 1;
//	private final Block hideFrom;
//	private final Block center;
//	private final int cx,cy,cz;
//
//	private final List<Block> covers;
//
//
//	private int status =0;
//	private List<CoverData> rData = new LinkedList<CoverData>();
//	private List<CoverData> rBadData = new LinkedList<CoverData>();
//
//	private final CoverFinderCallback callback;
//
//	public CoverFinder (Block start, CoverFinderCallback callback){
//		this.center = start;
//		covers = new LinkedList<Block>();
//		cx = center.getX();
//		cy = center.getY();
//		cz = center.getZ();
//
//		this.callback = callback;
//		hideFrom = center.getRelative(BlockFace.SELF);
//
//		this.runTaskAsynchronously(AIStuff.instance);
//
//	}
//
//	@Override
//	public void run(){
//		while (work());
//	}
//	public boolean work() {
//		if (status == 0){
//			if (cr == radius){
//				cr = 0;
//				status = 1;
//				return true;
//			}
//			for (int z = -cr; z< cr;z++){
//				if (isCover(-cr, z))
//					setCover(center.getWorld().getBlockAt(cx-cr, cy, cz+z) );
//				if (isCover(cr, z))
//					setCover(center.getWorld().getBlockAt(cx+cr, cy, cz+z) );
//
//			}
//
//			for (int x = -cr; x< cr;x++){
//				if (isCover(x, -cr))
//					setCover(center.getWorld().getBlockAt(cx+x, cy, cz-cr) );
//				if (isCover(x, cr))
//					setCover(center.getWorld().getBlockAt(cx+x, cy, cz+cr) );
//
//			}
//			cr++;
//		}else {
//			if (cr == covers.size()){
//				if (callback != null)
//					callback.done(this.rData, this.rBadData);
//				return false;
//			}
//			Block co = covers.get(cr);
//			Map<BlockFace,Boolean> pos = new HashMap<BlockFace, Boolean>();
//			for (BlockFace face : faces)
//				if (isPossible(co, face))
//					pos.put(face, isOpen(co, face));
//
//			if (pos.size() > 1 && pos.containsValue(false)){
//				BlockFace cS=null, s=null;
//
//				for (BlockFace face : pos.keySet())
//					if (!pos.get(face))
//						s = face;
//					else if (pos.get(face) )
//						cS = face;
////				sendBlockChange(co.getRelative(s).getRelative(BlockFace.UP),Material.WOOL,(byte)3);
////				sendBlockChange(co.getRelative(cS),Material.WOOL,(byte)1);
//				final CoverData cd = new CoverData(co,s,cS);
//				try {
//					// create our pathfinder
//					AStar pathFinder = new AStar(center.getLocation(), cd.coverBlock.getRelative(cd.safe).getLocation(), (int) (cd.coverBlock.getRelative(cd.safe).getLocation().distance(center.getLocation())*1.5));
//					// get the list of nodes to walk to as a Tile object
//					pathFinder.iterate();
//					// get the result of the path trace
//					PathingResult res = pathFinder.getPathingResult();
//					if (res == PathingResult.SUCCESS)
//						if (cd.canShoot != null)
//							rData .add(cd);
//						else
//							rBadData.add(cd);
//
//				} catch (InvalidPathException ex) {
//
//
//				}
//			}else{
//
//			}
//
//			cr++;
//
//		}
//		return true;
//	}
//
////	private void sendBlockChange(Block b, Material m, byte d){
////		for (Player p :Bukkit.getOnlinePlayers())
////			p.sendBlockChange(b.getLocation(), m, d);
////
////	}
//
//	private void setCover(Block b){
//		covers.add(b);
////
////		for (Player p :Bukkit.getOnlinePlayers()){
////			p.sendBlockChange(b.getLocation(), Material.WOOL, (byte) 7);
////		}
//	}
//
//	private boolean isCover(int x, int z){
////		for (Player p :Bukkit.getOnlinePlayers())
////			p.sendBlockChange(center.getWorld().getBlockAt(cx+x, cy, cz+z).getLocation(), Material.WOOL, (byte) 4);
//
//		Block a = center.getWorld().getBlockAt(cx+x, cy+1, cz+z);
//		Block b = center.getWorld().getBlockAt(cx+x, cy+2, cz+z);
//
//		return (a.getType().isSolid() && b.getType().isOccluding()&& b.getType().isSolid());
//	}
//
//	private boolean isPossible(Block b, BlockFace face){
//		return (!b.getRelative(face).getRelative(BlockFace.UP).getType().isSolid()
//				&& !b.getRelative(face).getRelative(BlockFace.UP,2).getType().isSolid());
//	}
//
//	private boolean isOpen(Block b, BlockFace face){
//		Vector s = b.getRelative(BlockFace.UP,2).getRelative(face).getLocation().toVector();
//		Vector d = hideFrom.getRelative(BlockFace.UP,2).getLocation().toVector().subtract(s);
//		if (d.lengthSquared() == 0)
//			return false;
//		BlockIterator it = new BlockIterator(b.getWorld(),
//				s,
//				d,
//				0d,
//				(int)d.length());
//		while (it.hasNext()){
//			Block b2 = it.next();
////			sendBlockChange(b2, Material.STAINED_GLASS,(byte) Arrays.asList(BlockFace.values()).indexOf(face));
//			if (b2.getType().isSolid()){
////				Bukkit.broadcastMessage(face.name() + " obstructed by " + b2.getType().name());
//				return false;
//			}
//		}
//		return true;
//	}
//
//	public static class CoverData{
//		public CoverData(Block b, BlockFace safe, BlockFace canShoot) {
//			this.coverBlock = b;
//			this.safe = safe;
//			this.canShoot = canShoot;
//		}
//		public final Block coverBlock;
//		public final BlockFace safe;
//		public final BlockFace canShoot;
//
//
//
//	}
//
//	public static abstract class CoverFinderCallback{
//		public abstract void done(List<CoverData> data, List<CoverData> passiveData);
//	}
}
