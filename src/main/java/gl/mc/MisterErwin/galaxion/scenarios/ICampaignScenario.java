package gl.mc.mistererwin.galaxion.scenarios;

/**
 * Created by mistererwin on 25.05.2015.
 * In case you need it, ask me ;)
 */
public interface ICampaignScenario extends IMultipleScenario {

    /**
     *
     * @return the campaign name
     */
    String getCampaignName();


}
