package gl.mc.mistererwin.galaxion.scenarios;

/**
 * Created by mistererwin on 25.05.2015.
 * In case you need it, ask me ;)
 * <p>
 * Represents a Scenario that can have multiple clones (at different Locations)
 */
public interface IMultipleScenario extends IScenario {

    /**
     * @return The real name
     */
    String getScenarioName();


}
