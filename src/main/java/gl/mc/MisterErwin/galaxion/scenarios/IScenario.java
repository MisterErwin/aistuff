package gl.mc.mistererwin.galaxion.scenarios;

import com.artemis.World;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;

import java.util.List;
import java.util.UUID;

/**
 * Created by mistererwin on 25.05.2015.
 * In case you need it, ask me ;)
 */
public interface IScenario {

    /**
     * @return The unique name of this scenario
     */
    String getUniqueName();

    /**
     * @return The (artemis) world
     */
    World getWorld();

    /**
     * @return A List of Listeners to be registered
     */
    List<Listener> getListener();


    /**
     * @return A list of UUID player
     */
    List<UUID> getPlayers();

    /**
     * Fired when a player wants to join the Scenario
     *
     * @param p The player
     * @return True if success, false if denied
     */
    boolean onJoin(Player p);

    /**
     * Fired when a player leaves the Scenario
     *
     * @param p The player
     */
    void onLeave(Player p);

    /**
     * Fired when the scenario starts
     */
    void onStart();

    /**
     * Fired when the scenario stops
     */
    void onStop();

    /**
     * @return is the scenario is running & ticking
     */
    boolean isRunning();

}
