package gl.mc.mistererwin.galaxion.scenarios;

import com.artemis.World;
import gl.mc.mistererwin.galaxion.manager.EntityTracker;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

/**
 * Created by mistererwin on 25.05.2015.
 * In case you need it, ask me ;)
 */
public abstract class Scenario implements IScenario {
    private final World world;
    private final List<Listener> listeners;
    private final String uniqueName;
    private final List<UUID> players;
    private boolean isRunning;

    public Scenario(World world, String uniqueName, List<Listener> listeners) {
        this.world = world;
        this.uniqueName = uniqueName;
        this.listeners = listeners;
        players = new ArrayList<>();
    }

    public Scenario(World world, String uniqueName) {
        this.world = world;
        this.uniqueName = uniqueName;
        this.listeners = new LinkedList<>();
        players = new ArrayList<>();
    }

    @Override
    public World getWorld() {
        return world;
    }

    @Override
    public List<UUID> getPlayers() {
        return this.players;
    }

    @Override
    public List<Listener> getListener() {
        return this.listeners;
    }

    @Override
    public String getUniqueName() {
        return this.uniqueName;
    }

    @Override
    public boolean isRunning() {
        return isRunning;
    }

    @Override
    public void onStart() {
        this.isRunning = true;
    }

    @Override
    public void onStop() {
        this.isRunning = false;
        EntityTracker et = world.getManager(EntityTracker.class);
        if (et != null)
            et.removeAll();
        world.dispose();
    }

    @Override
    public boolean onJoin(Player p) {
        players.add(p.getUniqueId());
        return true;
    }

    @Override
    public void onLeave(Player p) {
        players.remove(p.getUniqueId());
    }
}
