package gl.mc.mistererwin.galaxion.scenarios;

import com.artemis.Entity;
import com.artemis.World;
import com.artemis.managers.GroupManager;
import com.artemis.managers.PlayerManager;
import com.artemis.managers.TagManager;
import com.artemis.managers.UuidEntityManager;
import gl.mc.mistererwin.galaxion.AIStuff;
import gl.mc.mistererwin.galaxion.components.combat.WeaponHolder;
import gl.mc.mistererwin.galaxion.components.generic.PlayerEntity;
import gl.mc.mistererwin.galaxion.implementation.weapons.Weapon;
import gl.mc.mistererwin.galaxion.manager.EntityTracker;
import gl.mc.mistererwin.galaxion.systems.*;
import gl.mc.mistererwin.galaxion.systems.physics.FallPhysics;
import gl.mc.mistererwin.galaxion.systems.physics.PartSystem;
import gl.mc.mistererwin.galaxion.utils.AWorldUtils;
import gl.mc.mistererwin.galaxion.utils.InventoryUtils;
import gl.mc.mistererwin.galaxion.utils.libs.EntityLib;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by mistererwin on 25.05.2015.
 * In case you need it, ask me ;)
 */
public class ScenarioLib {

    public static Scenario testScen() {
        World world = new World();
        List<Listener> listener = new LinkedList<>();

        world.setManager(new UuidEntityManager());

        world.setManager(new GroupManager());
        world.setManager(new TagManager());
        world.setManager(new PlayerManager());

        world.setSystem(new AnimationSystem());
        world.setSystem(new PathPlanningSystem());

        world.setSystem(new AIProcessSystem());
        world.setSystem(new AIArbitrateSystem());

        world.setSystem(new MovementSystem());

        world.setSystem(new WeaponSystem());
        world.setSystem(new BulletSystem());

        world.setSystem(new WatcherSystem());

        world.setSystem(new PartSystem());
        world.setSystem(new FallPhysics());

//        world.setSystem(new RotateDemoSystem());

        world.setManager(new RemoveManager());

        listener.add(world.getSystem(WeaponSystem.class));
        listener.add(world.getSystem(BulletSystem.class));

        return new TestScen(world, "testScen", listener);

    }

    private static class TestScen extends Scenario {

        public TestScen(World world, String uniqueName, List<Listener> listeners) {
            super(world, uniqueName, listeners);
        }

        @Override
        public void onLeave(Player p) {
            super.onLeave(p);
            getWorld().getManager(PlayerManager.class).getEntitiesOfPlayer(p.getName()).forEach(Entity::deleteFromWorld);
        }

        @Override
        public boolean onJoin(Player p) {
             System.out.print("TestScen#onJoin");
            if (getPlayers().contains(p.getUniqueId()))
                return false;
            if (!isRunning())
                AIStuff.instance.getScenarioManager().startScenario(this);
            EntityLib.addPlayer(p,this);
            super.onJoin(p);
            return true;
        }


        @Override
        public void onStop() {
            try {
                //Remove the weapons
                for (Entity e : getWorld().getManager(EntityTracker.class).getWithComponents(WeaponHolder.class, PlayerEntity.class)) {
                    WeaponHolder wh = e.getComponent(WeaponHolder.class);
                    Player p = AWorldUtils.getPlayerByEntity(e);
                    if (p == null || wh == null)
                        return;
                    for (Weapon w : wh.weaponList) {
                        if (w.stats.amm != null)
                            InventoryUtils.remove(p, w.stats.amm, InventoryUtils.amount(p, w.stats.amm));
                        InventoryUtils.remove(p, w.stats.gun, InventoryUtils.amount(p, w.stats.gun));

                    }
                }
//                getWorld().getMa
            }catch (Exception|Error ex){}
            super.onStop();

        }
    }
}
