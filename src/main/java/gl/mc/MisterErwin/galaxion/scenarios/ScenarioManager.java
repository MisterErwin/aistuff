package gl.mc.mistererwin.galaxion.scenarios;

import com.artemis.World;
import com.artemis.systems.VoidEntitySystem;
import com.google.common.collect.ImmutableList;
import gl.mc.mistererwin.galaxion.AIStuff;
import gl.mc.mistererwin.galaxion.manager.EntityTracker;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitTask;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * Created by mistererwin on 25.05.2015.
 * In case you need it, ask me ;)
 */
public class ScenarioManager {
    HashMap<String, IScenario> scenarios;
    Map<UUID, IScenario> player;

    private final AIStuff plugin;

    public ScenarioManager(AIStuff pPlugin) {
        this.plugin = pPlugin;
        scenarios = new HashMap<>();
        player = new HashMap<>();
    }

    public void disable() {
        scenarios.keySet().forEach(this::stopScenario);
        scenarios.clear();
        player.clear();
        scenarios = null;
        player = null;
    }

    public void addScenario(IScenario scenario) {
        if (scenarios.containsKey(scenario.getUniqueName()))
            throw new IllegalArgumentException("A scenario with the name " + scenario.getUniqueName() + " already exists");
        if (scenario.getWorld().getManager(EntityTracker.class)==null)
           scenario.getWorld().setManager(new EntityTracker());
        scenarios.put(scenario.getUniqueName(), scenario);
    }

    public IScenario getScenario(Player p){
        return player.get(p.getUniqueId());
    }

    public IScenario getScenario(String scenarioName){
        return scenarios.get(scenarioName);
    }

    public void joinScenario(Player p, String name) {
        IScenario s = getScenario(p);
        if (s != null)
            s.onLeave(p);
        s = scenarios.get(name);
        if (s != null && s.onJoin(p))
            player.put(p.getUniqueId(), s);
    }

    public void leaveScenario(Player p) {
        IScenario sc = player.get(p.getUniqueId());
        if (sc != null) {
            sc.onLeave(p);
        }
    }


    public void stopScenario(String name) {
        try {
            IScenario sc = scenarios.get(name);
            if (sc == null) return;
            sc.onStop();
            sc.getWorld().getManager(EntityTracker.class).removeAll();

            ScenarioSystem s = sc.getWorld().getSystem(ScenarioSystem.class);
            if (s == null || s.Thread == null)
                return;
            s.Thread.cancel();
            s.Thread = null;

            sc.getWorld().process();

        }catch (Exception ex){
            ex.printStackTrace();

        }
    }

    public void startScenario(IScenario scenario) {
        System.out.println("Starting scenario " + scenario.getUniqueName());
        ScenarioSystem scenarioSystem = scenario.getWorld().getSystem(ScenarioSystem.class);
        if (scenarioSystem == null) { //First start
            System.out.println("First start");
            scenario.getListener().forEach(listener -> {Bukkit.getPluginManager().registerEvents(listener, plugin);System.out.println(listener.getClass().getSimpleName());});

            scenario.getWorld().setSystem(new ScenarioSystem(Bukkit.getScheduler().runTaskTimer(plugin, () -> tickWorld(scenario.getWorld()), 1, 1),scenario.getUniqueName()));
            scenario.getWorld().initialize();

            scenario.onStart();
        } else if (scenarioSystem.Thread == null) { //Restart
            System.out.println("Restart");
            scenarioSystem.Thread = Bukkit.getScheduler().runTaskTimer(plugin, () -> tickWorld(scenario.getWorld()), 1, 1);
            scenario.onStart();
        }
        System.out.println("!Started scenario " + scenario.getUniqueName());

    }

    private void tickWorld(World w) {
        ScenarioSystem s = w.getSystem(ScenarioSystem.class);
        if (--s.cticks > 0)
            return;

        s.cticks = s.tickrate;
        w.setDelta(s.tickrate);
        w.process();
    }

    public ImmutableList<IScenario> getScenarios(){
        return ImmutableList.copyOf(scenarios.values());
    }

    /**
     * Class/System to mark a world with a scenario
     */
    public static class ScenarioSystem extends VoidEntitySystem {

        private BukkitTask Thread;
        public final String scenarioName;
        public int tickrate = 1;
        public int cticks = 0;

        private ScenarioSystem(BukkitTask t, String n) {
            this.Thread = t;
            this.scenarioName = n;
            setPassive(true);
        }

        @Override
        protected void processSystem() {

        }
    }
}
