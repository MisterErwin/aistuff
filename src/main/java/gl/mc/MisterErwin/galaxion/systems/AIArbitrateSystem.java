package gl.mc.mistererwin.galaxion.systems;

import com.artemis.Aspect;
import com.artemis.Entity;
import com.artemis.systems.IntervalEntityProcessingSystem;
import gl.mc.mistererwin.galaxion.components.ai.AI;

/**
 * Created by mistererwin on 23.05.2015.
 * In case you need it, ask me ;)
 */
public class AIArbitrateSystem extends IntervalEntityProcessingSystem {
    public AIArbitrateSystem() {
        super(Aspect.getAspectForAll(AI.class), 20);
    }

    @Override
    protected void process(Entity entity) {
        AI ai = entity.getComponent(AI.class);
        //if the bot is under AI control but not scripted
        if (ai.AIControlled)
                ai.getGoal().arbitrate();
    }
}
