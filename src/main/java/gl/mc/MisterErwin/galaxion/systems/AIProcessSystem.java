package gl.mc.mistererwin.galaxion.systems;

import com.artemis.Aspect;
import com.artemis.Entity;
import com.artemis.systems.IntervalEntityProcessingSystem;
import gl.mc.mistererwin.galaxion.components.ai.AI;

/**
 * Created by mistererwin on 23.05.2015.
 * In case you need it, ask me ;)
 */
public class AIProcessSystem extends IntervalEntityProcessingSystem {
    public AIProcessSystem() {
        super(Aspect.getAspectForAll(AI.class), 20);
    }

    @Override
    protected void process(Entity entity) {
        AI ai = entity.getComponent(AI.class);
        //process the currently active goal. Note this is required even if the bot
        //is under user control. This is because a goal is created whenever a user
        //clicks on an area of the map that necessitates a path planning request.
        ai.getGoal().process();
    }
}
