package gl.mc.mistererwin.galaxion.systems;

import com.artemis.Aspect;
import com.artemis.Entity;
import com.artemis.systems.IntervalEntityProcessingSystem;
import gl.mc.mistererwin.galaxion.AIStuff;
import gl.mc.mistererwin.galaxion.api.animations.AnimationStage;
import gl.mc.mistererwin.galaxion.components.ai.ArmorStandEntitiy;
import gl.mc.mistererwin.galaxion.components.animation.AS_Animatable;
import gl.mc.mistererwin.galaxion.components.animation.AS_Animations;
import gl.mc.mistererwin.galaxion.implementation.animations.AniObject;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.util.EulerAngle;

import java.io.File;
import java.io.IOException;
import java.util.*;

/**
 * Created by mistererwin on 23.05.2015.
 * In case you need it, ask me ;)
 */
public class AnimationSystem extends IntervalEntityProcessingSystem {

    private static Map<String, AS_Animations.AniData> loaded = new HashMap<>();


    public AnimationSystem() {
        super(Aspect.getAspectForAll(AS_Animations.class, AS_Animatable.class), 1);
    }

    @Override
    protected void process(Entity e) {
        AS_Animations a = e.getComponent(AS_Animations.class);
        AS_Animatable aa = e.getComponent(AS_Animatable.class);
        Iterator<AS_Animations.AniData> it = a.anis.iterator();
        AS_Animations.AniData ad;
        while (it.hasNext()) {

            ad = it.next();
            if (!ad.active) continue;
            ad.ticks -= world.delta;
            if (ad.curStage == -1) {
                ad.curStage = 0;
                ad.ticks = ad.stages[0].getDuration();
            }
            if (ad.ticks-- <= 0) {
                if (ad.curStage >= ad.stages.length)
                    if (ad.con)
                        ad.curStage = 0;
                    else {
                        it.remove();
                        return;
                    }

                AnimationStage stage = ad.stages[ad.curStage];
                for (Map.Entry<String, EulerAngle> entry : stage.entrySet())
                    aa.objects.get(entry.getKey()).setSpeed(stage.getSpeed()).setTarget(entry.getValue());
                ad.ticks = stage.getDuration();
                ad.curStage++;

            }


        }
        renderAnimations(e, aa);


    }

    private void renderAnimations(Entity e, AS_Animatable a) {
        ArmorStandEntitiy ase = e.getComponent(ArmorStandEntitiy.class);
        if (ase != null)
            renderAnimationsASE(a, ase);
    }

    private void renderAnimationsASE(AS_Animatable a, ArmorStandEntitiy e) {
        for (Map.Entry<String, AniObject> en : a.objects.entrySet()) {
            en.getValue().update();
            if (en.getKey().equals("Head"))
                e.entity.setHeadPose(en.getValue().getCurrent());
            else if (en.getKey().equals("Body"))
                e.entity.setBodyPose(en.getValue().getCurrent());
            else if (en.getKey().equals("LeftArm"))
                e.entity.setLeftArmPose(en.getValue().getCurrent());
            else if (en.getKey().equals("RightArm"))
                e.entity.setRightArmPose(en.getValue().getCurrent());
            else if (en.getKey().equals("LeftLeg"))
                e.entity.setLeftLegPose(en.getValue().getCurrent());
            else if (en.getKey().equals("RightLeg"))
                e.entity.setRightLegPose(en.getValue().getCurrent());
        }
    }


    public static AS_Animations.AniData getAniData(String name) {
        if (loaded.containsKey(name)) {
            AS_Animations.AniData d = loaded.get(name);
            return (d != null) ? d : null;
        }
        AS_Animations.AniData ret = loadFromFile(name, new File(new File(AIStuff.instance.getDataFolder(), "animations"), name + ".ani.yml"));
        loaded.put(name, ret);
        return ret;
    }

    private static AS_Animations.AniData loadFromFile(String name, File f) {
        if (!f.exists()) {
            try {
                f.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }

        YamlConfiguration conf = YamlConfiguration.loadConfiguration(f);
        boolean con = conf.getBoolean("continued", false);
        ConfigurationSection stage;
        ConfigurationSection parts;
        ConfigurationSection s = conf.getConfigurationSection("stages");

            /*
              stages:
                a:
                  duration: 10
                  speed: 1
                  parts:
                    head: x | y  z
             */
        List<AnimationStage> ls = new ArrayList<>();

        List<String> usedParts = new LinkedList<>();

        for (String key : s.getKeys(false)) {
            stage = s.getConfigurationSection(key);
            AnimationStage st = new AnimationStage(stage.getLong("duration", 10), stage.getDouble("speed", 1));
            parts = stage.getConfigurationSection("parts");

            for (String partName : parts.getKeys(false)) {
                String[] v = stage.getConfigurationSection("parts").getString(partName).split("\\|");
                if (!usedParts.contains(partName))
                    usedParts.add(partName);
//                for (String p : v)
                st.put(partName, new EulerAngle(Double.parseDouble(v[0]), Double.parseDouble(v[1]), Double.parseDouble(v[2])));
            }
            ls.add(st);
        }
//        ret.stages = ls.toArray(new AnimationStage[ls.size()]);
        return new AS_Animations.AniData(name, usedParts.toArray(new String[usedParts.size()]), ls.toArray(new AnimationStage[ls.size()])).setCon(con).setMultByWalking(conf.getDouble("multByWalking",0d));

    }

    public static void reload() {
        loaded.clear();
    }
}

