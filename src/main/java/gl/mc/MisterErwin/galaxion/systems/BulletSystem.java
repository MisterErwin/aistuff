package gl.mc.mistererwin.galaxion.systems;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.annotations.Wire;
import com.artemis.managers.UuidEntityManager;
import com.artemis.systems.EntityProcessingSystem;
import gl.mc.mistererwin.galaxion.AIStuff;
import gl.mc.mistererwin.galaxion.components.combat.Health;
import gl.mc.mistererwin.galaxion.components.generic.PlayerEntity;
import gl.mc.mistererwin.galaxion.implementation.weapons.Bullet;
import gl.mc.mistererwin.galaxion.scenarios.IScenario;
import gl.mc.mistererwin.galaxion.utils.AWorldUtils;
import org.bukkit.*;
import org.bukkit.entity.Firework;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.inventory.meta.FireworkMeta;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by mistererwin on 24.05.2015.
 * In case you need it, ask me ;)
 */

@Wire
public class BulletSystem extends EntityProcessingSystem implements Listener {
    Random rand;

    List<Color> colors;

    ComponentMapper<Bullet> bulletMapper;


    public BulletSystem() {
        super(Aspect.getAspectForAll(Bullet.class));
        rand = new Random();
        colors = new ArrayList<>();
        colors.add(Color.RED);
        colors.add(Color.RED);
        colors.add(Color.RED);
        colors.add(Color.ORANGE);
        colors.add(Color.ORANGE);
        colors.add(Color.ORANGE);
        colors.add(Color.BLACK);
        colors.add(Color.GRAY);

    }

    @Override
    protected void process(Entity entity) {
        Bullet b = bulletMapper.get(entity);
        if (!b.dead) {
            b.ticks++;
            if (b.projectile != null) {
                b.lastLocation = b.projectile.getLocation();

                if (b.ticks > b.releaseTime) {
                    b.dead = true;
                    return;
                }

                if (b.shotFrom.stats.hasSmokeTrail)
                    b.lastLocation.getWorld().playEffect(b.lastLocation, Effect.SMOKE, 0);

                if (b.shotFrom.stats.isThrowable && b.ticks == 90) {
                    remove(entity, b);
                    return;
                }

                if (b.active) {
                    double dis = b.lastLocation.distanceSquared(b.startLocation);
                    if (dis > b.shotFrom.stats.maxDistance * b.shotFrom.stats.maxDistance) {
                        b.active = false;
                        if (!b.shotFrom.stats.isThrowable && !b.shotFrom.stats.canGoPastMaxDistance)
                            b.velocity.multiply(0.25D);
                    }
                    b.projectile.setVelocity(b.velocity);
                }

            } else {
                b.dead = true;
            }
            if (b.ticks > 200)
                b.dead = true;
        } else {
            remove(entity, b);
        }

        if (b.destroyNextTick)
            b.dead = true;
    }

    private void remove(Entity entity, Bullet b) {
        entity.deleteFromWorld();
        b.dead = true;
        onHit(entity, b);

        if (b.projectile != null)
            b.projectile.remove();
        destroy(b);

    }

    private void destroy(Bullet b) {
        b.destroyed = true;
        b.projectile = null;
        b.velocity = null;
        b.shotFrom = null;
        b.shooter = null;
    }

    private void onHit(Entity entity, Bullet b) {
        if (b.released)
            return;
        b.released = true;
        if (b.projectile != null) {
            b.lastLocation = b.projectile.getLocation();
            if (b.shotFrom != null) {
                int rad = (int) b.shotFrom.stats.explodeRadius;
                int rad2 = rad;
                if (b.shotFrom.stats.fireRadius > rad) {
                    rad = (int) b.shotFrom.stats.fireRadius;
                    rad2 = 2;
                    int radSQ = rad * rad;
                    for (int i = -rad; i <= rad; i++)
                        for (int ii = -rad2 / 2; ii <= rad2 / 2; ii++) {
                            for (int iii = -rad; iii <= rad; iii++) {
                                Location nloc = b.lastLocation.clone().add(i, ii, iii);
                                if ((nloc.distanceSquared(b.lastLocation) <= radSQ) && (rand.nextInt(5) == 1))
                                    nloc.getWorld().playEffect(nloc, Effect.MOBSPAWNER_FLAMES, 2);

                            }
                        }


                } else if (rad > 0) {
                    int radSQ = rad * rad;


                    for (int i = -rad; i <= rad; i++) {
                        for (int ii = -rad2 / 2; ii <= rad2 / 2; ii++) {
                            for (int iii = -rad; iii <= rad; iii++) {
                                Location nloc = b.lastLocation.clone().add(i, ii, iii);
                                if ((nloc.distanceSquared(b.lastLocation) <= radSQ) && (rand.nextInt(10) == 1)) {
                                    explosion(b, nloc);
                                }
                            }
                        }

                    }

                    explosion(b, b.lastLocation);
                    explosionDamage(entity, b);

                }
                fireSpread(entity, b);
                flash(entity, b);

            }
        }
    }


    private  void fireSpread(Entity e, Bullet b) {
        //TODO: Anything to say?
    }

    private  void flash(Entity e, Bullet b) {
        //TODO: Anything to say?

    }


    private  void explosion(Bullet b, Location location) {
        if (b.shotFrom.stats.explosionType.equals("TNT")) {
            double x = location.getX();
            double y = location.getY();
            double z = location.getZ();

            location.getWorld().createExplosion(x, y, z, (float) b.shotFrom.stats.explodeRadius, false, false);
        } else {
            World world = location.getWorld();
            Firework firework = world.spawn(location, Firework.class);

            FireworkMeta meta = firework.getFireworkMeta();
            meta.addEffect(getFireworkEffect());
            meta.setPower(1);

            firework.setFireworkMeta(meta);
            try {
                firework.detonate();
            } catch (Throwable ex) {
            }
        }
    }

    private FireworkEffect getFireworkEffect() {


        Random rand = new Random();
        FireworkEffect.Type type = FireworkEffect.Type.BALL_LARGE;
        if (rand.nextInt(2) == 0) {
            type = FireworkEffect.Type.BURST;
        }

        FireworkEffect effect = FireworkEffect.builder().flicker(true).withColor(colors).withFade(colors).with(type).trail(true).build();

        return effect;
    }

    private void explosionDamage(Entity e, Bullet b) {
        //TODO: Anything to say?

//        double damage;
//        if (b.shotFrom.stats.explodeRadius > 0.0D) {
//            b.lastLocation.getWorld().createExplosion(b.lastLocation, 0.0F);
//
//            if (b.shotFrom.stats.isThrowable) {
//                b.projectile.teleport(b.projectile.getLocation().add(0.0D, 1.0D, 0.0D));
//            }
//
//            damage = b.shotFrom.stats.explosionDamage;
//            if (damage <= 0.0D) {
//                damage = b.shotFrom.stats.gunDamage;
//            }
//            if (damage > 0.0D) {
//                double rad = b.shotFrom.stats.explodeRadius;
//                List<Entity> es = e.getWorld().getManager(EntityTracker.class).getWithComponents(Health.class, Located.class);
//                double radSQ = rad*rad;
//                for (Entity ee : es){
//                    Location loc = AWorldUtils.getLocation(ee);
//                    if (loc.distanceSquared(b.lastLocation) < radSQ){
//                        Health h = ee.getComponent(Health.class);
//                        if (h.getHealth() > 0)
//                    }
//                }
//                List<org.bukkit.entity.Entity> entities = b.projectile.getNearbyEntities(rad, rad, rad);
//                for (org.bukkit.entity.Entity entity : entities) {
//                    if ((entity.isValid()) && ((entity instanceof LivingEntity))) {
//                        LivingEntity lentity = (LivingEntity) entity;
//                        if (lentity.getHealth() > 0.0D) {
//                            EntityDamageByEntityEvent event = getDamageEvent(this.shooter.getPlayer(), lentity, EntityDamageEvent.DamageCause.ENTITY_EXPLOSION, damage);
//
//                            this.plugin.getServer().getPluginManager().callEvent(event);
//                            if (!event.isCancelled()) {
//                                if (lentity.hasLineOfSight(this.projectile)) {
//                                    lentity.damage(damage, this.shooter.getPlayer());
//                                }
//                            }
//                        }
//                    }
//                }
//            }
//        }
    }

//    private void tickBullet(Entity entity, Bullet b) {
//        if ((b.dead) || (b.destroyNextTick)) {
//            remove(entity, b);
//            return;
//        }
//
//        if (b.projectile == null) {
//            remove(entity, b);
//            return;
//        }
//
//        if (b.shooter == null) {
//            remove(entity, b);
//            return;
//        }
//
//        if (b.projectile.getLocation().getY() <= 3.0D) {
//            remove(entity, b);
//            return;
//        }
//
//        b.ticks += 1;
//        b.lastLocation = b.projectile.getLocation();
//
//        if (b.ticks > b.releaseTime) {
//            ParticleEffectData[] eff = b.shotFrom.stats.hitParticles;
//            if (eff != null)
//                for (ParticleEffectData ped : eff)
//                    ped.play(b.lastLocation);
//
//            remove(entity, b);
//            return;
//        }
//
//        if (b.shotFrom.stats.hasSmokeTrail)
//            b.lastLocation.getWorld().playEffect(b.lastLocation, Effect.SMOKE, 0);
//        //Trail particles
//        ParticleEffectData[] eff = b.shotFrom.stats.trailParticles;
//        if (eff != null)
//            for (ParticleEffectData ped : eff)
//                ped.play(b.lastLocation);
//
//
//        if ((b.shotFrom.stats.isThrowable) && (b.ticks == 90)) {
//            remove(entity, b);
//            return;
//        }
//
//        if (b.active) {
//            if (b.lastLocation.getWorld().getUID() == b.startLocation.getWorld().getUID()) {
//                double dis = b.lastLocation.distance(b.startLocation);
//                if (dis > b.shotFrom.stats.maxDistance) {
//                    b.active = false;
//                    if (!b.shotFrom.stats.isThrowable && (!b.shotFrom.stats.canGoPastMaxDistance)) {
//                        b.velocity.multiply(0.25D);
//                    }
//                }
//            }
//
//            b.projectile.setVelocity(b.velocity);
//        }
//
//        if (b.ticks > 200) {
//            remove(entity, b);
//        }
//    }


    /*
       Entity listener
    */
    @EventHandler
    public void onProjectileHit(ProjectileHitEvent event) {
        Projectile check = event.getEntity();
        IScenario is = AWorldUtils.getScenarioByBukkitEntity(event.getEntity());
        if (is == null)
            return;

        Entity e = is.getWorld().getManager(UuidEntityManager.class).getEntity(event.getEntity().getUniqueId());
        if (e != null) {
            Bullet bullet = bulletMapper.get(e);
            if (bullet != null) {
                onHit(e, bullet);
                bullet.destroyNextTick = true;
            }
        }
    }

    @EventHandler
    public void onEntityDamageByEntity(EntityDamageByEntityEvent event) {
        if (event.isCancelled())
            return;

        if (event.getDamager() instanceof Projectile) {
            Location eye = event.getEntity().getLocation();
            if (event.getEntity() instanceof LivingEntity)
                eye = ((LivingEntity) event.getEntity()).getEyeLocation();


            //Scenario check
            String isn = getScenarioName(event.getEntity());
            if (isn == null || isn != getScenarioName(event.getDamager()))
                return;


            IScenario scenario = AIStuff.instance.getScenarioManager().getScenario(isn);


            Entity be = AWorldUtils.getEntityByBukktiEntity(scenario.getWorld(), event.getDamager());
            if (be == null) return;


            Bullet b = bulletMapper.get(be);
            if (b == null) return;


            Entity damaged = AWorldUtils.getEntityByBukktiEntity(scenario.getWorld(), event.getEntity());
            if (damaged == null || damaged.getComponent(Health.class) == null) return;


            double damage = b.shotFrom.stats.gunDamage;
            double mult = 1.0D;
            // HeadShot
            if (b.shotFrom.stats.canHeadShot && isNear(event.getDamager().getLocation(), eye,
                    0.26D)) {
                eye.getWorld().playEffect(eye, Effect.ZOMBIE_DESTROY_DOOR, 3);
                mult = b.shotFrom.stats.headshootmultiplier;
                if (b.shotFrom.holder.getComponent(PlayerEntity.class) != null) {
                    Player p = AWorldUtils.getPlayerByEntity(be);
                    if (p != null)
                        p.playSound(p.getEyeLocation(), Sound.FIZZ, 1, 0);
                }
            }


            b.shotFrom.doKnockback(event.getEntity(), b.velocity);
            remove(be, b);
        }

    }

    private boolean isNear(Location location, Location eyeLocation, double d) {
        return Math.abs(location.getY() - eyeLocation.getY()) <= d;
    }

    private String getScenarioName(org.bukkit.entity.Entity e) {
        if (!e.hasMetadata("scenarioName"))
            return null;
        return e.getMetadata("scenarioName").get(0).asString();
    }

}
