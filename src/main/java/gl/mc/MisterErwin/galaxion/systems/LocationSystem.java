package gl.mc.mistererwin.galaxion.systems;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.annotations.Wire;
import com.artemis.systems.IntervalEntityProcessingSystem;
import gl.mc.mistererwin.galaxion.components.generic.EntityLocated;
import gl.mc.mistererwin.galaxion.components.generic.Located;
import org.bukkit.Location;

/**
 * Created by mistererwin on 24.05.2015.
 * In case you need it, ask me ;)
 */
@Wire
public class LocationSystem extends IntervalEntityProcessingSystem {

    ComponentMapper<Located> lEntityMapper;
    ComponentMapper<EntityLocated> eLEntityMapper;


    public LocationSystem() {
        super(Aspect.getAspectForAll(EntityLocated.class, Located.class), 1);
    }

    @Override
    protected void process(Entity entity) {
        lEntityMapper.get(entity).location=eLEntityMapper.get(entity).entity.getLocation();
    }

    public Location getLocation(Entity e){
        return lEntityMapper.get(e).location;
    }



}
