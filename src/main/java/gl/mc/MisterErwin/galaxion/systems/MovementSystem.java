package gl.mc.mistererwin.galaxion.systems;

import com.artemis.Aspect;
import com.artemis.Entity;
import com.artemis.systems.IntervalEntityProcessingSystem;
import gl.mc.mistererwin.galaxion.api.ai.pathing.PTile;
import gl.mc.mistererwin.galaxion.components.ai.*;
import gl.mc.mistererwin.galaxion.components.animation.AS_Animatable;
import gl.mc.mistererwin.galaxion.components.animation.AS_Animations;
import gl.mc.mistererwin.galaxion.utils.AWorldUtils;
import org.bukkit.Location;
import org.bukkit.util.Vector;

import java.text.DecimalFormat;

/**
 * Created by mistererwin on 23.05.2015.
 * In case you need it, ask me ;)
 */
public class MovementSystem extends IntervalEntityProcessingSystem {

    public MovementSystem() {
        super(Aspect.getAspectForAll(Moveable.class), 1);
    }

    @Override
    protected void process(Entity e) {
        Moveable m = e.getComponent(Moveable.class);

        if (!m.allowMoving)
            return;

        AS_Animations a = e.getComponent(AS_Animations.class);
        //Strafe etc


        //Path following
        Path p = e.getComponent(Path.class);
        if (p.following && p.path != null) {
            if (p.path.isEmpty()) {
                if (a != null)
                    AS_Animations.addForceConAnimation(a, "idle");
                p.path = null;
            } else {
                PTile next = p.path.getFirst();
                if (p.updated) {
                    p.m_TimeExpected = (long) (new Vector(next.getAX(), next.getAY(), next.getAZ()).subtract(AWorldUtils.getLocation(e).toVector()).length() / (m.speed * 0.001));
                    p.m_StartTime = System.currentTimeMillis();

                    applySpeed(m, a, next.getBehavior(), p.path.size() > 2);
                    p.updated = false;
                }
                if (isStuck(p)) {
                    p.cancel();
                    p.requestTo(p.target, AWorldUtils.getLocation(e).subtract(0, 1, 0));
                    return;
                }
                Location loc = AWorldUtils.getLocation(e);
                int i = (int) Math.floor(loc.getY() + 0.5);

                double dX = next.getAX() + .5 - loc.getX();
                double dZ = next.getAZ() + .5 - loc.getZ();
                double ddY = next.getAY() + 1 - i;
                double dY = next.getAY() - loc.getY() + 1;

                double dist = dX * dX + dZ * dZ + dY * dY;

                ArmorStandEntitiy aes = e.getComponent(ArmorStandEntitiy.class);
                if (aes != null) {
                    aes.entity.setCustomName(new DecimalFormat("#0.00000").format(dist));
                    aes.entity.setCustomNameVisible(true);
                }

                if (dist < 0.3) {
                    p.path.removeFirst();
                    p.updated = true;
                } else {
                    Turn t = e.getComponent(Turn.class);
                    if (t != null) {
                        float f = (float) (Math.atan2(dY, dX) * 180.0D / 3.141592741012573D) - 90.0F;
                        float yaw = a(loc.getYaw(), f, 30.0F);
                        float yawchange = loc.getYaw() - yaw;
                        if (Math.abs(yawchange) > t.maxTurnRate) {
                            yaw = trimTo(yaw, yawchange, t.maxTurnRate);
                            if (m.RunIfNotRotated) {
                                //TODO: Trim according to yaw
                                move(e, m, a, dX, ddY, dZ);
                            }
                        } else
                            move(e, m, a, dX, ddY, dZ);
                        setYaw(e, yaw);


                    } else
                        move(e, m, a, dX, ddY, dZ);

                }
            }
        }
    }

    private void move(Entity e, Moveable m, AS_Animations a, double dX, double dY, double dZ) {
        if (a != null)
            for (AS_Animations.AniData d : a.anis)
                if (d.multByWalking>0) {
                    double dist = Math.sqrt(dX*dX+dZ*dZ);
                    for (String s : d.usedParts)
                        e.getComponent(AS_Animatable.class).objects.get(s).update(d.multByWalking*dist);
                }
        Jump j = e.getComponent(Jump.class);
        if (j != null)
            moveJumpAble(m, j, e.getComponent(ArmorStandEntitiy.class).entity, a, dX, dY, dZ);
//        else
//            moveFlyAble(dX, dY, dZ);
    }

    private void moveJumpAble(Moveable m, Jump j, org.bukkit.entity.Entity e, AS_Animations a, double dX, double dY, double dZ) {
        boolean shouldJump = ((dY > 0.0D) && (dX * dX + dZ * dZ) < 1.0D);
        j.jumpTicks = Math.max(0, j.jumpTicks - 1);
        Vector v = new Vector(dX, 0, dZ).normalize().multiply(m.speed);
        v.setY(e.getVelocity().getY());

        if (shouldJump && e.isOnGround()) {
            if (a != null) {
                AS_Animations.addForceAnimation(a, "preJump");
                AS_Animations.stopAnimation(a, "walk");
                AS_Animations.stopAnimation(a, "run");
            }
            if (j.jumpTicks == 0) {
                AS_Animations.addForceAnimation(a, "jump");

                v.setY(v.getY() + j.jumpStrength);
                j.jumpTicks = 10;
                if (a != null) {
                    AS_Animations.addForceConAnimation(a, "walk");
                }
            }
        }

        e.setVelocity(v);
    }



    private void setYaw(Entity e, float yaw) {
        ArmorStandEntitiy ex = e.getComponent(ArmorStandEntitiy.class);
        if (ex != null) {
            Location t = ex.entity.getLocation();
            t.setYaw(yaw);
            ex.entity.teleport(t);
        }

    }

    private boolean isStuck(Path p) {
        double TimeTaken = System.currentTimeMillis() - p.m_StartTime;
        return TimeTaken > p.m_TimeExpected;
    }


    private void applySpeed(Moveable m, AS_Animations a, PTile.Behavior b, boolean fast) {
        switch (b) {

            default:
                m.speed = (fast) ? m.runSpeed : m.defaultSpeed;
                AS_Animations.addForceConAnimation(a, (fast) ? "run" : "walk");
                break;
        }
    }

    private float a(float f, float f1, float f2) {
        float f3 = g(f1 - f);

        if (f3 > f2) {
            f3 = f2;
        }

        if (f3 < -f2) {
            f3 = -f2;
        }

        float f4 = f + f3;

        if (f4 < 0.0F)
            f4 += 360.0F;
        else if (f4 > 360.0F) {
            f4 -= 360.0F;
        }

        return f4;
    }

    private float g(float paramFloat) {
        paramFloat %= 360.0F;
        if (paramFloat >= 180.0F) {
            paramFloat -= 360.0F;
        }
        if (paramFloat < -180.0F) {
            paramFloat += 360.0F;
        }
        return paramFloat;
    }

    private float trimTo(float value, float change, float maxAbs) {
        if (change > 0)
            return value - (maxAbs - change);
        return value + (maxAbs - change);
    }

}
