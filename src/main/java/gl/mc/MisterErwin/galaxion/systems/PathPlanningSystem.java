package gl.mc.mistererwin.galaxion.systems;

import com.artemis.Aspect;
import com.artemis.Entity;
import com.artemis.systems.EntityProcessingSystem;
import gl.mc.mistererwin.galaxion.api.ai.pathing.PTile;
import gl.mc.mistererwin.galaxion.components.ai.ArmorStandEntitiy;
import gl.mc.mistererwin.galaxion.components.ai.Path;
import gl.mc.mistererwin.galaxion.implementation.ai.pathing.astar.TickedAStar;
import gl.mc.mistererwin.galaxion.implementation.ai.pathing.astar.Tile;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;

/**
 * Created by mistererwin on 23.05.2015.
 * In case you need it, ask me ;)
 */
public class PathPlanningSystem extends EntityProcessingSystem {
    public PathPlanningSystem() {
        super(Aspect.getAspectForAll(Path.class, ArmorStandEntitiy.class));
    }

    LinkedList<Path> toWork = new LinkedList<>();

    @Override
    public void end() {
        int i= 20; //maxticks
        while (i > 0 && !toWork.isEmpty())
            i = tickWorker(i);
        toWork.clear();
    }

    private int tickWorker(int i) {
        Iterator<Path> it = toWork.iterator();
        while (it.hasNext() && i-- > 0)
            if (tickWorker(it.next()))
                it.remove();
        return i;
    }


    @Override
    protected void process(Entity entity) {
        Path p = entity.getComponent(Path.class);
        if (p.isRequested()) {

            if (p.target == null || p.start == null){
                System.out.println("null");
                if (p.target==null)
                  System.out.println("target");
                if (p.start==null)
                    System.out.println("start");

                p.finishRequest(null);
                return;
            }

            if (p.worker == null) {
                p.start = entity.getComponent(ArmorStandEntitiy.class).entity.getLocation().subtract(0, 1, 0);
                try {
                    p.worker = new TickedAStar(p.start, p.target, Math.max(30, (int) p.target.distance(p.start) * 3));
                    p.worker.preStart();
                } catch (TickedAStar.InvalidPathException e) {
                    System.out.println(e.getErrorReason());
                    p.finishRequest(null);
                    return;
                }
            }
            if (!tickWorker(p))
              toWork.add(p);
        }
    }

    private boolean tickWorker(Path p) {

        ArrayList<Tile> ret = p.
                worker.
                tick();
        if (ret != null) {

            LinkedList<PTile> l = new LinkedList<>();
            for (Tile t : ret)
                l.add(new PTile(p.start, t));
            p.finishRequest(l);
            return true;
        }
        return false;
    }
}
