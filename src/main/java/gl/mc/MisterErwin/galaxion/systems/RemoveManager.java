package gl.mc.mistererwin.galaxion.systems;

import com.artemis.Entity;
import com.artemis.Manager;
import gl.mc.mistererwin.galaxion.components.ai.ArmorStandEntitiy;
import gl.mc.mistererwin.galaxion.components.character.AniPart;
import gl.mc.mistererwin.galaxion.components.character.Parted;

/**
 * Created by mistererwin on 01.07.2015.
 * In case you need it, ask me ;)
 */
public class RemoveManager extends Manager {


    @Override
    protected void dispose() {
//        Field entitiesF = getWorld().getEntityManager().getClass().getDeclaredField("entities");
//        entitiesF.setAccessible(true);
//        Bag<Entity> entityBag = entitiesF.get(getWorld().getEntityManager());
//        for (Entity e : entityBag)
//         e.deleteFromWorld();
//        getWorld().getEntityManager().entities;
    }

    @Override
    public void deleted(Entity e) {
        super.deleted(e);
        removeRender(e);
    }

    /**
     * Removes all bukkit entities from the world
     *
     * @param e The entity
     */
    public void removeRender(Entity e) {
        removeAS(e.getComponent(ArmorStandEntitiy.class));
        removePart(e.getComponent(Parted.class));
    }

    private void removePart(Parted p) {
        if (p == null || p.partList == null) return;
        for (AniPart aniPart : p.partList) {
            if (aniPart.as != null) aniPart.as.remove();
            aniPart.as = null;
        }
        p.partList = null;
        p.rootPart = null;
    }


    private void removePart(AniPart p) {
        if (p == null) return;
        if (p.as != null) p.as.remove();
        p.as = null;
        if (p.children != null)
            for (AniPart pi : p.children)
                removePart(pi);
    }

    private void removeAS(ArmorStandEntitiy ase) {
        if (ase == null) return;
        if (ase.entity != null)
            ase.entity.remove();
        ase.entity = null;
    }
}
