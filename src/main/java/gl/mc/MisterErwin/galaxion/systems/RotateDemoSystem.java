package gl.mc.mistererwin.galaxion.systems;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.annotations.Wire;
import com.artemis.systems.IntervalEntityProcessingSystem;
import gl.mc.mistererwin.galaxion.components.character.AniPart;
import gl.mc.mistererwin.galaxion.components.character.Parted;
import gl.mc.mistererwin.galaxion.components.generic.Located;
import gl.mc.mistererwin.galaxion.utils.math.Quaternion;

/**
 * Created by mistererwin on 13.07.2015.
 * In case you need it, ask me ;)
 */
@Wire
public class RotateDemoSystem extends IntervalEntityProcessingSystem {

    private static final int X = 1, Y = 2, Z = 4;


    ComponentMapper<Located> locatedComponentMapper;
    ComponentMapper<Parted> partedComponentMapper;

    private static final float step = 0.01745329251f*2;

    static Quaternion x = new Quaternion(new float[]{step, 0, 0});
    static Quaternion y = new Quaternion(new float[]{0, step, 0});
    static Quaternion z = new Quaternion(new float[]{0, 0, step});

    private int steps = 0, flags=Z;

    public RotateDemoSystem() {
        super(Aspect.getAspectForAll(Parted.class, Located.class), 1);
    }

    @Override
    protected void process(Entity entity) {
        AniPart root = partedComponentMapper.get(entity).partList[7];
        root.userControlled=true;
        Quaternion rot = root.localTransform.getRotation();//locatedComponentMapper.get(entity).transform.getRotation();
        if ((flags & X)==X)
            x.mult(rot,rot);
        if ((flags & Y)==Y)
            y.mult(rot,rot);
        if ((flags & Z)==Z)
            z.mult(rot,rot);
        if (steps++ >= 359/2){
            steps=0;
            rot.set(0,0,0,1);
            flags=next();
            entity.getComponent(Parted.class).rootPart.as.setCustomNameVisible(true);
            entity.getComponent(Parted.class).rootPart.as.setCustomName(getName());
        }
        rot.normalizeLocal();
        //locatedComponentMapper.get(entity).transform.getRotation().multLocal(q);
    }

    private String getName(){
        StringBuilder sb = new StringBuilder();
        if ((flags&X)==X)sb.append("X");
        if ((flags&Y)==Y)sb.append("Y");
        if ((flags&Z)==Z)sb.append("Z");

        return sb.toString();
    }

    private int next(){
        if (true)return flags;
        switch (flags){
            case X:case Y:
                return flags*2;
//            case Z:
//                return X|Y;
//            case X|Y:
//                return X|Z;
//            case X|Z:
//                return Y|Z;
//            case Y|Z:
//                return X|Y|Z;
            default:
                return X;
        }

    }
}
