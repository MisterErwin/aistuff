package gl.mc.mistererwin.galaxion.systems;

import com.artemis.Aspect;
import com.artemis.Component;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.annotations.Wire;
import com.artemis.systems.EntityProcessingSystem;
import com.artemis.utils.Bag;
import gl.mc.mistererwin.galaxion.api.ai.goals.ICompositeGoal;
import gl.mc.mistererwin.galaxion.api.ai.goals.IGoal;
import gl.mc.mistererwin.galaxion.api.ai.pathing.PTile;
import gl.mc.mistererwin.galaxion.components.ai.*;
import gl.mc.mistererwin.galaxion.components.animation.AS_Animatable;
import gl.mc.mistererwin.galaxion.components.animation.AS_Animations;
import gl.mc.mistererwin.galaxion.components.web.WatchableEntity;
import gl.mc.mistererwin.galaxion.implementation.ai.goals.Goal;
import gl.mc.mistererwin.galaxion.implementation.animations.AniObject;
import org.apache.commons.lang3.StringUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.util.*;

/**
 * Created by mistererwin on 06.06.2015.
 * In case you need it, ask me ;)
 */
@Wire
public class WatcherSystem extends EntityProcessingSystem {

    ComponentMapper<WatchableEntity> entityMapper;
//    ComponentMapper<AI> AIMapper;
//    ComponentMapper<Jump> JumpMapper;
//    ComponentMapper<Moveable> MoveableMapper;
//    ComponentMapper<Path> PathMapper;
//    ComponentMapper<Target> TargetMapper;

    public WatcherSystem() {
        super(Aspect.getAspectForAll(WatchableEntity.class));
    }

    @Override
    protected void process(Entity entity) {
        WatchableEntity we = entityMapper.get(entity);
        if (we.watcher.isEmpty())
            return;
        JSONObject send = new JSONObject();

        //AI Processing
        processAI(send, entity, we);

        processAni(send, entity, we);

//        processCombat(send, entity, we);
        if (!we.lastV.containsKey("components")){
            List<String> components = new LinkedList<>();
            for (Component c : entity.getComponents(new Bag<Component>()))
                components.add(c.getClass().getSimpleName());

            we.lastV.put("components",components);
            send.put("components",components);
        }

        if (send.isEmpty())//Nothing change
            return;

        String s = send.toJSONString();
        we.watcher.forEach(is -> is.send(s));
    }

    private void processAni(JSONObject send, Entity entity, WatchableEntity we) {
        AS_Animatable ani = entity.getComponent(AS_Animatable.class);
        if (ani == null)
            return;
        Map<String, Object> m = new LinkedHashMap<>();

        for (Map.Entry<String, AniObject> e : ani.objects.entrySet()) {
            Map<String, Object> m2 = new LinkedHashMap<>();
            String s = e.getValue().getTarget().toString();
            if (!pE(we.lastV.get("ani.objects." + e.getKey() + ".t"), s)) {
                we.lastV.put("ani.objects." + e.getKey() + ".t", s);
                JSONArray m3 = new JSONArray();
                m3.add(e.getValue().getTarget().getX());
                m3.add(e.getValue().getTarget().getY());
                m3.add(e.getValue().getTarget().getZ());
                m2.put("target", m3);
            }
            s = e.getValue().getCurrent().toString();
            if (!pE(we.lastV.get("ani.objects." + e.getKey() + ".c"), s)) {
                we.lastV.put("ani.objects." + e.getKey() + ".c", s);
                JSONArray m3 = new JSONArray();
                m3.add(e.getValue().getCurrent().getX());
                m3.add(e.getValue().getCurrent().getY());
                m3.add(e.getValue().getCurrent().getZ());
                m2.put("current", m3);
            }

            if (!m2.isEmpty())
                m.put(e.getKey(), m2);
        }
        if (!m.isEmpty())
            send.put("ani", m);

        AS_Animations anis = entity.getComponent(AS_Animations.class);
        if (anis != null) {
            m = new LinkedHashMap<>();
            for (AS_Animations.AniData a : anis.anis) {
                Map<String, Object> m2 = new LinkedHashMap<>();
                if (!pE(we.lastV.get("anis." + a.name + ".active"), a.active)) {
                    we.lastV.put("anis." + a.name + ".active", a.active);
                    m2.put("active", a.active);
                }
                String s = a.curStage + "/" + (a.stages.length - 1);
                if (!pE(we.lastV.get("anis." + a.name + ".status"), s)) {
                    we.lastV.put("anis." + a.name + ".status", s);
                    m2.put("status", s);
                }
                if (!m2.isEmpty())
                    m.put(a.name, m2);
            }

            if (!m.isEmpty())
                send.put("anis", m);
        }

    }


    private void processAI(JSONObject send, Entity entity, WatchableEntity we) {
        AI ai = entity.getComponent(AI.class);
        if (ai == null)
            return;
        Map<String, Object> m = new LinkedHashMap<>();
        if (!pE(we.lastV.get("ai.AIControlled"), ai.AIControlled)) {
            we.lastV.put("ai.AIControlled", ai.AIControlled);
            m.put("AIControlled", ai.AIControlled);
        }
        if (ai.AIControlled) {
            String s = getGoalString(ai.getGoal());
            if (!pE(we.lastV.get("ai.goals"), s)) {
                we.lastV.put("ai.goals", s);
                m.put("goals", s);
            }
        }

        if (!m.isEmpty())
            send.put("ai", m);

        Jump jump = entity.getComponent(Jump.class);
        if (jump != null) {
            m = new LinkedHashMap<>();
            if (!pE(we.lastV.get("jump.jumpStrength"), jump.jumpStrength)) {
                we.lastV.put("jump.jumpStrength", jump.jumpStrength);
                m.put("jumpStrength", jump.jumpStrength);
            }
            if (!pE(we.lastV.get("jump.jumpTicks"), jump.jumpTicks)) {
                we.lastV.put("jump.jumpTicks", jump.jumpTicks);
                m.put("jumpTicks", jump.jumpTicks);
            }

            if (!m.isEmpty())
                send.put("jump", m);
        }

        Moveable mov = entity.getComponent(Moveable.class);
        if (mov != null) {
            m = new LinkedHashMap<>();
            if (!pE(we.lastV.get("mov.allowMoving"), mov.allowMoving)) {
                we.lastV.put("mov.allowMoving", mov.allowMoving);
                m.put("allowMoving", mov.allowMoving);
            }
            if (!pE(we.lastV.get("mov.defaultSpeed"), mov.defaultSpeed)) {
                we.lastV.put("mov.defaultSpeed", mov.defaultSpeed);
                m.put("defaultSpeed", mov.defaultSpeed);
            }
            if (!pE(we.lastV.get("mov.runSpeed"), mov.runSpeed)) {
                we.lastV.put("mov.runSpeed", mov.runSpeed);
                m.put("runSpeed", mov.runSpeed);
            }

            if (!m.isEmpty())
                send.put("mov", m);
        }

        Path path = entity.getComponent(Path.class);
        if (path != null) {
            m = new LinkedHashMap<>();
            if (!pE(we.lastV.get("path.following"), path.following)) {
                we.lastV.put("path.following", path.following);
                m.put("following", path.following);
            }
            if (!pE(we.lastV.get("path.isRequested"), path.isRequested())) {
                we.lastV.put("path.isRequested", path.isRequested());
                m.put("isRequested", path.isRequested());
            }
            if (!pE(we.lastV.get("path.m_StartTime"), path.m_StartTime)) {
                we.lastV.put("path.m_StartTime", path.m_StartTime);
                m.put("m_StartTime", path.m_StartTime);
            }
            if (!pE(we.lastV.get("path.m_TimeExpected"), path.m_TimeExpected)) {
                we.lastV.put("path.m_TimeExpected", path.m_TimeExpected);
                m.put("m_TimeExpected", path.m_TimeExpected);
            }

            if (path.path != null) {
                ArrayList l = new ArrayList(path.path.size());
                for (PTile pt : path.path)
                    l.add(PTile.data(pt));
                if (!pE(we.lastV.get("path.path"), l)) {
                    we.lastV.put("path.path", l);
                    m.put("path", l);
                }
            } else if (!we.lastV.containsKey("path.path")) {
                we.lastV.remove("path.path");
                m.put("path", null);


            }


            if (!m.isEmpty())
                send.put("path", m);
        }

        Target target = entity.getComponent(Target.class);
        if (target != null) {
            m = new LinkedHashMap<>();
            String s = (target.target != null) ? target.target.getUuid().toString() : "none";
            if (!pE(we.lastV.get("target.target"), s)) {
                we.lastV.put("target.target", s);
                send.put("target", s);
            }
        }

    }

    private String getGoalString(Goal g) {
        StringBuilder sb = new StringBuilder();
        getGoalString(sb, g, 0);
        return sb.toString();
    }

    private void getGoalString(StringBuilder current, IGoal goal, int n) {
        if (goal == null)
            return;
        current.append("-->").append(StringUtils.repeat(" ", n)).append(goal.getType()).append("[").append(goal.getStatus().name()).append("]\n");
        if (goal instanceof ICompositeGoal)
            getGoalString(current, ((ICompositeGoal) goal).getFrontGoal(), n + 1);
    }

    private boolean pE(Object o, Object b) {
        return (o != null && o.equals(b));
    }

    private boolean pEar(Object o, Object b) {
        return (o == null || o.equals(b));
    }

//    private boolean pE(Object o, boolean b){
//
//    }
}