package gl.mc.mistererwin.galaxion.systems;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.annotations.Wire;
import com.artemis.managers.PlayerManager;
import com.artemis.systems.EntityProcessingSystem;
import gl.mc.mistererwin.galaxion.components.combat.Aim;
import gl.mc.mistererwin.galaxion.components.combat.Health;
import gl.mc.mistererwin.galaxion.components.combat.WeaponHolder;
import gl.mc.mistererwin.galaxion.components.generic.PlayerEntity;
import gl.mc.mistererwin.galaxion.implementation.weapons.Weapon;
import gl.mc.mistererwin.galaxion.implementation.weapons.WeaponStats;
import gl.mc.mistererwin.galaxion.scenarios.IScenario;
import gl.mc.mistererwin.galaxion.utils.AWorldUtils;
import gl.mc.mistererwin.galaxion.utils.libs.WeaponLib;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractAtEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by mistererwin on 24.05.2015.
 * In case you need it, ask me ;)
 */
@Wire
public class WeaponSystem extends EntityProcessingSystem implements Listener {

    Map<String, WeaponStats> weapons;

    ComponentMapper<WeaponHolder> weaponHolderMapper;


    public WeaponSystem() {
        super(Aspect.getAspectForAll(WeaponHolder.class, PlayerEntity.class));

        weapons = new HashMap<>();

        WeaponStats ws = WeaponLib.getShotgun();
        weapons.put(ws.gunID, ws);
        ws = WeaponLib.getRifle();
        weapons.put(ws.gunID, ws);
        ws = WeaponLib.getOP();
        weapons.put(ws.gunID, ws);

    }

    @Override
    protected void process(Entity entity) {
        WeaponHolder wh = weaponHolderMapper.get(entity);
        Player p = Bukkit.getPlayerExact(entity.getWorld().getManager(PlayerManager.class).getPlayer(entity));
        if (p == null) {
            entity.deleteFromWorld();
            return;
        }
        int newSlot = p.getInventory().getHeldItemSlot();
        if (newSlot != wh.lastSlot) {
            wh.currentlyUsing = null;
            wh.lastSlot = newSlot;
        }
        if (wh.currentlyUsing != null && wh.currentlyUsing.timer <= 0)
            wh.currentlyUsing = null;

        for (Weapon w : wh.weaponList) {
            if (w != null) {
                w.tick();
                Health h = entity.getComponent(Health.class);
                if (h != null && h.getHealth() <= 0)
                    w.finishReloading();

            }
        }

    }

    public void fireWeapon(WeaponHolder wh, Weapon gun) {
        if (gun.timer <= 0) {
            wh.currentlyUsing = gun;
            gun.firing = true;
        }
    }

    public void toggleAim(Entity e, Player p) {
        Aim aim = e.getComponent(Aim.class);
        if (aim == null) return;
        aim.isAiming = !aim.isAiming;
        if (aim.isAiming)
            p.addPotionEffect(new PotionEffect(
                    PotionEffectType.SLOW, 12000, 4));
        else
            p.removePotionEffect(PotionEffectType.SLOW);

    }

    public void giveWeapon(Inventory i, Entity e, String wp) {
        if (i == null || e == null) return;
        WeaponStats w = weapons.get(wp);
        if (w != null)
            giveWeapon(i, e, w);
    }


    public void giveWeapon(Inventory i, Entity e, WeaponStats wp) {
        i.addItem(wp.gun.newItemStack(1));
        i.addItem(wp.amm.newItemStack(64));
        weaponHolderMapper.get(e).weaponList.add(new Weapon(wp, e));
    }


    public void handleClick(Player player, boolean right) {
        ItemStack inHand = player.getItemInHand();
        if (inHand == null || inHand.getType() == Material.AIR) return;
        IScenario is = AWorldUtils.getScenarioByBukkitEntity(player);
        if (is == null)
            return;
        Entity e = AWorldUtils.getEntityByBukktiPlayer(is.getWorld(), player);
        if (e == null) return;

        WeaponHolder wh = weaponHolderMapper.get(e);
        if (wh == null) return;
        for (Weapon w : wh.weaponList)
            if (w.stats.gun.matches(inHand)) {
                wh.currentlyUsing = w;
                if (w.stats.canClickRight && right) {
                    if (!w.stats.canAimRight) {
                        w.heldDownTicks++;
                        w.lastFired = 0;
                        if (!w.firing)
                            fireWeapon(wh, w);
                    } else
                        toggleAim(e, player);
                } else if (w.stats.canClickLeft && !right) {
                    if (!w.stats.canAimLeft) {
                        w.heldDownTicks++;
                        w.lastFired = 0;
                        if (!w.firing)
                            fireWeapon(wh, w);
                    } else
                        toggleAim(e, player);
                }
                return;
            }
    }


    /*
        Player Events
     */
    @EventHandler
    public void onPlayerDropItem(PlayerDropItemEvent event) {
        if (event.isCancelled()) return;
        ItemStack inHand = event.getItemDrop().getItemStack();
        if (inHand == null || inHand.getType() == Material.AIR) return;
        IScenario is = AWorldUtils.getScenarioByBukkitEntity(event.getPlayer());
        if (is == null)
            return;

        Entity e = AWorldUtils.getEntityByBukktiPlayer(is.getWorld(), event.getPlayer());
        if (e == null) return;

        WeaponHolder wh = weaponHolderMapper.get(e);
        if (wh == null) return;

        for (Weapon w : wh.weaponList)
            if (w.stats.gun.matches(inHand) && w.stats.reloadGunOnDrop) {
                w.reloadGun();
                event.setCancelled(true);
                return;
            }
    }

    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent event) {
        if (event.getAction() == Action.RIGHT_CLICK_AIR || event.getAction() == Action.RIGHT_CLICK_BLOCK)
            handleClick(event.getPlayer(), true);
        else if (event.getAction() == Action.LEFT_CLICK_AIR || event.getAction() == Action.LEFT_CLICK_BLOCK)
            handleClick(event.getPlayer(), false);

    }

    @EventHandler
    public void onAS(PlayerInteractAtEntityEvent e) {
        if (e.getRightClicked() instanceof ArmorStand)
            e.setCancelled(true);
        handleClick(e.getPlayer(), true);
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onEntityDamageByEntity(EntityDamageByEntityEvent event) {

        if (event.isCancelled())
            return;
        if (event.getEntity() instanceof ArmorStand) {
            IScenario is = AWorldUtils.getScenarioByBukkitEntity(event.getEntity());
            if (is != null && AWorldUtils.getEntityByBukktiEntity(is.getWorld(), event.getEntity()) != null)
                event.setCancelled(true);
        }


    }

}
