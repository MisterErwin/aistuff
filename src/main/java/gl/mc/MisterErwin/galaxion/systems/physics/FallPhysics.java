package gl.mc.mistererwin.galaxion.systems.physics;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.annotations.Wire;
import com.artemis.systems.EntityProcessingSystem;
import gl.mc.mistererwin.galaxion.components.generic.Located;
import gl.mc.mistererwin.galaxion.implementation.generic.Fallable;
import org.bukkit.Location;

/**
 * Created by mistererwin on 15.07.2015.
 * In case you need it, ask me ;)
 */
@Wire
public class FallPhysics extends EntityProcessingSystem {

    ComponentMapper<Fallable> fallableComponentMapper;
    ComponentMapper<Located> locatedComponentMapper;

    public FallPhysics() {
        super(Aspect.getAspectForAll(Fallable.class));
    }

    @Override
    protected void process(Entity entity) {

        Location orig = locatedComponentMapper.get(entity).location, location = new Location(orig.getWorld(),orig.getX(),orig.getY()-0.91,orig.getZ());
        if (!location.getBlock().getType().isSolid())
            locatedComponentMapper.get(entity).transform.getTranslation().subtractLocal(0,fallableComponentMapper.get(entity).fallSpeed,0);
    }
}
