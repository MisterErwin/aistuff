package gl.mc.mistererwin.galaxion.systems.physics;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.annotations.Wire;
import com.artemis.systems.IntervalEntityProcessingSystem;
import gl.mc.mistererwin.galaxion.AIStuff;
import gl.mc.mistererwin.galaxion.components.animation.Animated;
import gl.mc.mistererwin.galaxion.components.animation.Animations;
import gl.mc.mistererwin.galaxion.components.character.AniPart;
import gl.mc.mistererwin.galaxion.components.character.Parted;
import gl.mc.mistererwin.galaxion.components.generic.DespawnBehavior;
import gl.mc.mistererwin.galaxion.components.generic.Located;
import gl.mc.mistererwin.galaxion.implementation.animations.AnimChannel;
import gl.mc.mistererwin.galaxion.implementation.animations.Animation;
import gl.mc.mistererwin.galaxion.packetStuff.PacketUtils;
import gl.mc.mistererwin.galaxion.utils.TempVars;
import gl.mc.mistererwin.galaxion.utils.math.FastMath;
import gl.mc.mistererwin.galaxion.utils.math.Quaternion;
import gl.mc.mistererwin.galaxion.utils.math.Vector3f;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.ArmorStand;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.EulerAngle;


/**
 * Created by mistererwin on 30.06.2015.
 * In case you need it, ask me ;)
 */
@Wire
public class PartSystem extends IntervalEntityProcessingSystem {
    ComponentMapper<Parted> partedComponentMapper;
    ComponentMapper<Located> locMapper;
    ComponentMapper<DespawnBehavior> despawnBehaviorComponentMapper;
    ComponentMapper<Animations> animationsComponentMapper;
    ComponentMapper<Animated> animatedComponentMapper;

    private final static double bigrel = 1.42d;


    public PartSystem() {
        super(Aspect.getAspectForAll(Parted.class, Located.class), 1);
    }

    @Override
    protected void process(Entity entity) {
        TempVars tempVars = TempVars.get();
        Parted parted = partedComponentMapper.get(entity);
        Located loc = locMapper.get(entity);
        DespawnBehavior despawnBehavior = despawnBehaviorComponentMapper.get(entity);
        for (AniPart p : parted.partList) if (!p.userControlled) p.localTransform.set(p.initialTransform);

        Animated animated = animatedComponentMapper.get(entity);
        if (animated != null)
            for (AnimChannel animChannel : animated.channels)
                animChannel.update(1, tempVars, parted, this);

        if (parted.rootPart != null) {
            updateRoot(loc, parted.rootPart);
            update(parted.rootPart, loc, despawnBehavior, tempVars);
        }
        tempVars.release();
    }

    private void updateRoot(Located loc, AniPart root) {
        root.localTransform.set(loc.transform);
        loc.location.setX(loc.transform.getTranslation().getX());
        loc.location.setY(loc.transform.getTranslation().getY());
        loc.location.setZ(loc.transform.getTranslation().getZ());
    }

    private static Quaternion XAxis = new Quaternion().fromAngleAxis(FastMath.PI, Vector3f.UNIT_X);

    private void updateEntity(AniPart ap, Located located, DespawnBehavior despawnBehavior, TempVars tempVars) {
        if (despawnBehavior != null && despawnBehavior.isDespawned && !despawnBehavior.requestRespawn)
            return;
        Vector3f v = tempVars.vect1;
        Quaternion q = tempVars.quat1;
        ap.worldTransform.getTranslation(v);
        ap.worldTransform.getRotation(q);
//        ap.initialTransform.getRotation().inverse().mult(ap.worldTransform.getRotation(),q);
        Location n = getLoc(located, v);
        if (ap.as == null || ap.as.isDead()) {
            ap.as = spawnAtLoc(n, ap.head, ap.name);
        }
//        if (ap.inverse&&false)
//            q.multLocal(XAxis);
//            q = XAxis.mult(q,null);
        float[] euler = q.toAngles(null);
        EulerAngle ea = ap.as.getHeadPose();
//        if (ap.inverse)
//            euler[1] =FastMath.PI -euler[1];
        euler[2] = FastMath.PI-euler[2];
        if (ea.getX() != euler[0] || ea.getY() != euler[1] || ea.getZ() != euler[2])
            ap.as.setHeadPose(new EulerAngle(euler[0], euler[1], euler[2]));


//        n.setYaw(ap.localTransform.getRotation().toAngles(null)[1]);
//        n.setYaw(ap.localTransform.getRotation().mult(q.inverse()).toAngles(null)[1]);
        if (n.distanceSquared(ap.as.getLocation()) != 0) {
//            ap.as.teleport(n.clone().add(10,10,10));
            ap.as.teleport(n);
//            ParticleEffect.NOTE.display(0, 0, 0, 1, 1, n, 20);

//            PacketUtils.resendEntityLocation(ap.as);
        } else if (n.getYaw() != ap.as.getLocation().getY())
            ap.as.teleport(n);


    }

    private Location getLoc(Located loc, Vector3f vector3) {
        return new Location(loc.location.getWorld(), vector3.x, vector3.y - bigrel, vector3.z);//new Location(loc.location.getWorld(),loc.location.getX()vector3.x,vector3.y,vector3.z);
    }

    private ArmorStand spawnAtLoc(Location loc, ItemStack i, String name) {
        ArmorStand as = loc.getWorld().spawn(loc.clone().subtract(0, bigrel, 0), ArmorStand.class);
//        System.out.println("Spawning at " + loc.clone().subtract(0, bigrel, 0).toString());
//        as.teleport(loc)
        as.setGravity(false);
        as.setVisible(false);
        as.setCustomName(name);
        if (i != null)
            as.setHelmet(i);
        Bukkit.getScheduler().runTaskLater(AIStuff.instance, new Runnable() {
            @Override
            public void run() {
                PacketUtils.resendEntityLocation(as);
            }
        }, 10);
        return as;
    }

    private void updateWorldVectors(AniPart part) {
        if (part.currentWeightSum == 1f) { //Performance
            part.currentWeightSum = -1;
        } else if (part.currentWeightSum != -1f) {
            // Apply the weight to the local localTransform
            if (part.currentWeightSum == 0) {
                part.localTransform.set(part.initialTransform);
            } else {
                float invWeightSum = 1f - part.currentWeightSum;
                part.localTransform.getRotation().nlerp(part.initialTransform.getRotation(), invWeightSum);
//                localRot.nlerp(initialRot, invWeightSum);
                part.localTransform.getTranslation().interpolateLocal(part.initialTransform.getTranslation(), invWeightSum);
//                localPos.interpolateLocal(initialPos, invWeightSum);
//                localScale.interpolateLocal(initialScale, invWeightSum);
            }

            // Future invocations of localTransform blend will start over.
            part.currentWeightSum = -1;
        }

        if (part.parent != null) {
            //rotation
            part.parent.worldTransform.getRotation().mult(part.localTransform.getRotation(), part.worldTransform.getRotation());

//            parent.worldRot.mult(localRot, worldRot);

            //scale
            //For scale parent scale is not taken into account!
            // worldScale.set(localScale);
//            parent.worldScale.mult(localScale, worldScale);
//      //      part.parent.worldTransform.getScale().mult(part.localTransform.getScale(),part.worldTransform.getScale());

            //translation
            //scale and rotation of parent affect bone position
//            parent.worldRot.mult(localPos, worldPos);
//            Quaternion q2 = new Quaternion(part.parent.worldTransform.getRotation().getX(), -part.parent.worldTransform.getRotation().getY(), part.parent.worldTransform.getRotation().getZ(), -part.parent.worldTransform.getRotation().getW());
//            part.parent.worldTransform.getRotation()
//            q2.inverse()
            part.parent.worldTransform.getRotation().mult(part.localTransform.getTranslation(), part.worldTransform.getTranslation());
            part.worldTransform.getRotation().normalizeLocal();
            //Scale
//            worldPos.multLocal(parent.worldScale);
//      //      part.worldTransform.getTranslation().multLocal(part.parent.worldTransform.getScale());

//            worldPos.addLocal(parent.worldPos);
            part.worldTransform.getTranslation().addLocal(part.parent.worldTransform.getTranslation());
        } else {
            part.worldTransform.set(part.localTransform);
//            worldRot.set(localRot);
//            worldPos.set(localPos);
//            worldScale.set(localScale);
        }
    }

    private void update(AniPart part, Located located, DespawnBehavior despawnBehavior, TempVars tempVars) {
        updateWorldVectors(part);
        if (part.children != null)
            for (AniPart ip : part.children)
                update(ip, located, despawnBehavior, tempVars);
        updateEntity(part, located, despawnBehavior, tempVars);
    }

    /**
     * Blends the given animation transform onto the bone's local transform.
     * <p>
     * Subsequent calls of this method stack up, with the final transformation
     * of the bone computed at {@link () } which resets
     * the stack.
     * <p>
     * E.g. a single transform blend with weight = 0.5 followed by an
     * updateWorldVectors() call will result in final transform = transform * 0.5.
     * Two transform blends with weight = 0.5 each will result in the two
     * transforms blended together (nlerp) with blend = 0.5.
     *
     * @param part        The Anipart
     * @param translation The translation to blend in
     * @param rotation    The rotation to blend in
     * @param weight      The weight of the transform to apply. Set to 1.0 to prevent
     *                    any other transform from being applied until updateWorldVectors().
     */
    public void blendAnimTransforms(AniPart part, Vector3f translation, Quaternion rotation, float weight) {
//        if (part.userControl) {
//            return;
//        }

        if (weight == 0) {
            // Do not apply this transform at all.
            return;
        }

        if (part.currentWeightSum == 1) {
            return; // More than 2 transforms are being blended
        } else if (part.currentWeightSum == -1 || part.currentWeightSum == 0) {
            // Set the transform fully
            part.localTransform.getTranslation().set(part.initialTransform.getTranslation()).addLocal(translation);
            part.localTransform.getRotation().set(part.initialTransform.getRotation()).multLocal(rotation);
//            if (scale != null) {
//                localScale.set(initialScale).multLocal(scale);
//            }
            // Set the weight. It will be applied in updateWorldVectors().
            part.currentWeightSum = weight;
        } else {
            // The weight is already set.
            // Blend in the new transform.
            TempVars vars = TempVars.get();

            Vector3f tmpV = vars.vect1;
            Vector3f tmpV2 = vars.vect2;
            Quaternion tmpQ = vars.quat1;

            tmpV.set(part.initialTransform.getTranslation()).addLocal(translation);
            part.localTransform.getTranslation().interpolateLocal(tmpV, weight);

            tmpQ.set(part.initialTransform.getRotation()).multLocal(rotation);
            part.localTransform.getRotation().nlerp(tmpQ, weight);

//            if (scale != null) {
//                tmpV2.set(initialScale).multLocal(scale);
//                localScale.interpolateLocal(tmpV2, weight);
//            }

            // Ensures no new weights will be blended in the future.
            part.currentWeightSum = 1;

            vars.release();
        }
    }

    /**
     * Adds an animation to be available for playing to this
     * <code>AnimControl</code>.
     *
     * @param animations The @see{@link Animations} component .
     * @param anim       The animation to add.
     */
    public void addAnim(Animation anim, Animations animations) {
        animations.animationMap.put(anim.getName(), anim);
    }

    public void addAnim(Animation anim, Entity entity) {
        addAnim(anim, animationsComponentMapper.get(entity));
    }

    /**
     * Remove an animation so that it is no longer available for playing.
     *
     * @param anim       The animation to remove.
     * @param animations The @see{@link Animations} component .
     */
    public void removeAnim(Animation anim, Animations animations) {
        if (!animations.animationMap.containsKey(anim.getName())) {
            throw new IllegalArgumentException("Given animation does not exist "
                    + "in this Animations component");
        }

        animations.animationMap.remove(anim.getName());
    }

    public void removeAnim(Animation anim, Entity entity) {
        removeAnim(anim, animationsComponentMapper.get(entity));
    }


    /**
     * Create a new animation channel, by default assigned to all bones
     * in the skeleton.
     *
     * @param entity The @see{@link Entity} itself .
     * @param parted The @see{@link Parted} component .
     * @return A new animation channel for this <code>AnimControl</code>.
     */
    public AnimChannel createChannel(Parted parted, Entity entity) {
        AnimChannel channel = new AnimChannel(parted, entity);
        animatedComponentMapper.get(entity).channels.add(channel);
        return channel;
    }

    public AnimChannel createChannel(Entity entity) {
        return createChannel(partedComponentMapper.get(entity), entity);
    }

    /**
     * Clears all the channels that were created.
     *
     * @param animated The @see{@link Animated} component .
     * @see PartSystem#createChannel(Parted, Entity)
     */
    public void clearChannels(Animated animated) {
//        for (AnimChannel animChannel : channels) {
//            for (AnimEventListener list : listeners) {
//                list.onAnimCycleDone(this, animChannel, animChannel.getAnimationName());
//            }
//        }
        animated.channels.clear();
    }

    public void clearChannels(Entity entity) {
        clearChannels(animatedComponentMapper.get(entity));
    }


}
