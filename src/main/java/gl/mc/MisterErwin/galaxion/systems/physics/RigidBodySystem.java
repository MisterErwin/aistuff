package gl.mc.mistererwin.galaxion.systems.physics;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.annotations.Wire;
import com.artemis.systems.IntervalEntityProcessingSystem;
import gl.mc.mistererwin.galaxion.components.character.RigidEntity;
import gl.mc.mistererwin.galaxion.implementation.generic.RigidBody;
import gl.mc.mistererwin.galaxion.utils.math.Matrix4;
import gl.mc.mistererwin.galaxion.utils.math.Quaternion2;
import gl.mc.mistererwin.galaxion.utils.math.Transform;
import gl.mc.mistererwin.galaxion.utils.math.Vector3;

/**
 * Created by mistererwin on 21.06.2015.
 * In case you need it, ask me ;)
 */
@Wire
public class RigidBodySystem extends IntervalEntityProcessingSystem {

    public final static double sleepEpsilon = 0.3;

    ComponentMapper<RigidEntity> rigidBody;

    public RigidBodySystem() {
        super(Aspect.getAspectForAll(RigidEntity.class), 1);
    }


    /**
     * Integrates the rigid body forward in time by the given amount.
     * This function uses a Newton-Euler integration method, which is a
     * linear approximation to the correct integral. For this reason it
     * may be inaccurate in some cases.
     */
    @Override
    protected void process(Entity entity) {
        double duration = 1;
        RigidEntity re = rigidBody.get(entity);
        for (RigidBody rb : re.parts)
            process(rb, duration);
    }

    private void process(RigidBody rb, double duration) {
        calculateDerivedData(rb);
        /* No physics ;)
        if (!rb.isAwake)
            return;
        // Calculate linear acceleration from force inputs.
        rb.lastFrameAcceleration = rb.acceleration.clone();
        rb.lastFrameAcceleration.addScaledVector(rb.forceAccum, rb.inverseMass);

        // Calculate angular acceleration from torque inputs.
        Vector3 angularAcceleration =
                rb.inverseInertiaTensorWorld.localTransform(rb.torqueAccum);

        // Adjust velocities
        // Update linear velocity from both acceleration and impulse.
        rb.velocity.addScaledVector(rb.lastFrameAcceleration, duration);

        // Update angular velocity from both acceleration and impulse.
        rb.rotation.addScaledVector(angularAcceleration, duration);

        // Impose drag.
        rb.velocity.mult(Math.pow(rb.linearDamping, duration));
        rb.rotation.mult(Math.pow(rb.angularDamping, duration));

        // Adjust positions
        // Update linear position.
        rb.position.addScaledVector(rb.velocity, duration);

        // Update angular position.
        rb.orientation.addScaledVector(rb.rotation, duration);

        // Normalise the orientation, and update the matrices with the new
        // position and orientation
        calculateDerivedData(rb);

        // Clear accumulators.
        clearAccumulators(rb);

        // Update the kinetic energy store, and possibly put the body to
        // sleep.
        if (rb.canSleep) {
            double currentMotion = rb.velocity.scalarProduct(rb.velocity) +
                    rb.rotation.scalarProduct(rb.rotation);

            double bias = Math.pow(0.5, duration);
            rb.motion = bias * rb.motion + (1 - bias) * currentMotion;

            if (rb.motion < sleepEpsilon) setAwake(rb, false);
            else if (rb.motion > 10 * sleepEpsilon) rb.motion = 10 * sleepEpsilon;
        }
        */

    }

    private void setAwake(RigidBody rb, boolean awake) {
        if (awake) {
            rb.isAwake = true;

            // Add a bit of motion to avoid it falling asleep immediately.
            rb.motion = sleepEpsilon * 2.0f;
        } else {
            rb.isAwake = false;
            rb.velocity.clear();
            rb.rotation.clear();
        }
    }

    private void clearAccumulators(RigidBody rb) {
        rb.forceAccum.clear();
        rb.torqueAccum.clear();
    }

    /**
     * Calculates internal data from state data. This should be called
     * after the body's state is altered directly (it is called
     * automatically during integration). If you change the body's state
     * and then intend to integrate before querying any data (such as
     * the localTransform matrix), then you can ommit this step.
     */
    void calculateDerivedData(RigidBody b) {
        b.orientation.normalize();
        //Calculate tje localTransform matrix of the body
        _calculateTransformMatrix(b.transformMatrix, b.position, b.orientation);
    }


    void _calculateTransformMatrix(Matrix4 transformMatrix,
                                   Vector3 position,
                                   Quaternion2 orientation) {
        transformMatrix.data[0] = 1 - 2 * orientation.j * orientation.j -
                2 * orientation.k * orientation.k;
        transformMatrix.data[1] = 2 * orientation.i * orientation.j -
                2 * orientation.r * orientation.k;
        transformMatrix.data[2] = 2 * orientation.i * orientation.k +
                2 * orientation.r * orientation.j;
        transformMatrix.data[3] = position.x;

        transformMatrix.data[4] = 2 * orientation.i * orientation.j +
                2 * orientation.r * orientation.k;
        transformMatrix.data[5] = 1 - 2 * orientation.i * orientation.i -
                2 * orientation.k * orientation.k;
        transformMatrix.data[6] = 2 * orientation.j * orientation.k -
                2 * orientation.r * orientation.i;
        transformMatrix.data[7] = position.y;

        transformMatrix.data[8] = 2 * orientation.i * orientation.k -
                2 * orientation.r * orientation.j;
        transformMatrix.data[9] = 2 * orientation.j * orientation.k +
                2 * orientation.r * orientation.i;
        transformMatrix.data[10] = 1 - 2 * orientation.i * orientation.i -
                2 * orientation.j * orientation.j;
        transformMatrix.data[11] = position.z;
    }


    /////////////////////////////////////////////////////////////////

    public void addForceAtBodyPoint(RigidBody rb, Vector3 force,
                                    Vector3 point) {
        // Convert to coordinates relative to center of mass.
        Vector3 pt = getPointInWorldSpace(rb, point);
        addForceAtPoint(rb, force, pt);
    }

    public void addForceAtPoint(RigidBody rb, Vector3 force,
                                Vector3 point) {
        // Convert to coordinates relative to center of mass.
        Vector3 pt = point.clone();
        pt.sub(rb.position);

        rb.forceAccum.add(force);
        rb.torqueAccum.add(pt.vectorProductCopy(force));//pt % force;

        rb.isAwake = true;
    }

    public void addTorque(RigidBody rb, Vector3 torque) {
        rb.torqueAccum.add(torque);
        rb.isAwake = true;
    }


    public static Vector3 getPointInLocalSpace(RigidBody rb, Vector3 point) {
        Transform t;
        return rb.transformMatrix.transformInverse(point);
    }

    public static Vector3 getPointInWorldSpace(RigidBody rb, Vector3 point) {
        return rb.transformMatrix.transform(point);
    }

    public static Vector3 getDirectionInLocalSpace(RigidBody rb, Vector3 direction) {
        return rb.transformMatrix.transformInverseDirection(direction);
    }

    public static Vector3 getDirectionInWorldSpace(RigidBody rb, Vector3 direction) {
        return rb.transformMatrix.transformDirection(direction);
    }

    public static void rotateBy(RigidBody rb, Vector3 v) {
        rb.orientation.rotateByVector(v);
    }
}
