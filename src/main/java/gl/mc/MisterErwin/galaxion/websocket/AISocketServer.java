package gl.mc.mistererwin.galaxion.websocket;

import com.artemis.Entity;
import gl.mc.mistererwin.galaxion.AIStuff;
import gl.mc.mistererwin.galaxion.components.animation.AS_Animatable;
import gl.mc.mistererwin.galaxion.components.animation.AS_Animations;
import gl.mc.mistererwin.galaxion.components.web.WatchableEntity;
import gl.mc.mistererwin.galaxion.implementation.animations.AniObject;
import gl.mc.mistererwin.galaxion.manager.EntityTracker;
import gl.mc.mistererwin.galaxion.scenarios.ScenarioManager;
import gl.mc.mistererwin.galaxion.systems.WatcherSystem;
import org.bukkit.Bukkit;
import org.java_websocket.WebSocket;
import org.java_websocket.handshake.ClientHandshake;
import org.java_websocket.server.WebSocketServer;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.util.*;

/**
 * Created by mistererwin on 06.06.2015.
 * In case you need it, ask me ;)
 */
public class AISocketServer extends WebSocketServer {

    final AIStuff plugin;

    JSONParser parser = new JSONParser();
    private boolean isOpen = false;

    public AISocketServer(AIStuff pPlugin) {
        super(new InetSocketAddress(getAvaiblePort(8080)));
        this.plugin = pPlugin;
        isOpen = true;
        if (getPort() != 8080)
            plugin.getLogger().info("Port 8080 is already in use");
        plugin.getLogger().info("Going to use " + getPort() + " + for the WebWatcher");

        start();
    }


    @Override
    public void onOpen(WebSocket conn, ClientHandshake handshake) {
        System.out.println(conn.getRemoteSocketAddress().getAddress().getHostAddress() + " entered the watcher!");
    }

    @Override
    public void onClose(WebSocket conn, int code, String reason, boolean remote) {
        if (!isOpen) return;
        plugin.getScenarioManager().getScenarios().stream().filter(is -> is.isRunning() && is.getWorld().getSystem(WatcherSystem.class) != null).forEach(is -> {
            EntityTracker et = is.getWorld().getManager(EntityTracker.class);
            if (et != null) {
                for (WatchableEntity c : et.getComWithComponent(WatchableEntity.class)) {
                    c.watcher.remove(conn);
                }
            }
        });
        System.out.println(conn + " has left the watcher!");
    }

    @Override
    public void onError(WebSocket conn, Exception ex) {
        ex.printStackTrace();
    }

    public void stop() {
        isOpen = false;
        try {
            connections().forEach(org.java_websocket.WebSocket::close);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            super.stop();

            while (connections().iterator().hasNext() && connections().iterator().next().isOpen()) {
                System.out.println("Waiting...");
                Thread.sleep(100);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onMessage(WebSocket conn, String message) {
//        System.out.println(conn + ": " + message);
        JSONObject o;
        try {
            o = (JSONObject) parser.parse(message);
        } catch (ParseException e) {
            e.printStackTrace();
            return;
        }
        switch (o.get("action").toString()) {
            case "gp":
                GamePadHelper.onInput(conn, o, plugin);
                break;
            case "watch":
                startWatching(conn, o);
                break;
            case "scen":
                if (o.containsKey("list"))
                    listScen(conn);
                else if (o.containsKey("listE"))
                    listE(conn, o);
                else if (o.containsKey("setTickRate"))
                    setTickRate(conn, o);
                break;
            case "setPose":
                setPose(conn, o);
                break;
            case "ani":
                handleAni(conn, o);
                break;
        }
    }


    void setTickRate(WebSocket conn, JSONObject o) {
        String name = (String) o.get("name");
        plugin.getScenarioManager().getScenarios().stream().filter(is -> is.getUniqueName().equals(name)).forEach(is -> {
            is.getWorld().getSystem(ScenarioManager.ScenarioSystem.class).tickrate = Integer.valueOf(o.get("setTickRate").toString());
            Bukkit.broadcastMessage(is.getUniqueName() + "has a tickrate of " + Integer.valueOf(o.get("setTickRate").toString()));
        });
    }


    void listScen(WebSocket conn) {
        Map<String, Object> m = new LinkedHashMap<>();
        m.put("hide", "listScen");
        List<Map> l = new ArrayList<>();
        plugin.getScenarioManager().getScenarios().stream().filter(is -> is.getWorld().getSystem(ScenarioManager.ScenarioSystem.class) != null).forEach(is -> {
            Map<String, Object> m2 = new LinkedHashMap<>();
            m2.put("name", is.getUniqueName());
            m2.put("running", is.isRunning());
            m2.put("player", is.getPlayers().size());
            m2.put("tickrate", is.getWorld().getSystem(ScenarioManager.ScenarioSystem.class).tickrate);
            l.add(m2);
        });
        m.put("list", l);

        conn.send(JSONObject.toJSONString(m));
    }

    void listE(WebSocket conn, JSONObject o) {
        String sname = (String) o.get("listE");
        LinkedList<Map<String, Object>> list = new LinkedList<>();
        plugin.getScenarioManager().getScenarios().stream().filter(is -> is.getUniqueName().equals(sname) && is.getWorld().getSystem(WatcherSystem.class) != null).forEach(is -> {
            EntityTracker et = is.getWorld().getManager(EntityTracker.class);
            if (et != null) {
                for (AbstractMap.Entry<Entity, WatchableEntity> e : et.getBothWithComponent(WatchableEntity.class)) {
                    Map<String, Object> map2 = new LinkedHashMap<String, Object>();
                    map2.put("type", e.getValue().type);
                    map2.put("uuid", e.getKey().getUuid().toString());
                    list.add(map2);
                }
            }
        });
        Map<String, Object> map = new LinkedHashMap<>();
        map.put("list", list);
        map.put("hide", "listEntities");

        conn.send(JSONObject.toJSONString(map));
    }


    void startWatching(WebSocket conn, JSONObject o) {
        UUID uuid = UUID.fromString(o.get("uuid").toString());
        plugin.getScenarioManager().getScenarios().stream().filter(is -> is.isRunning() && is.getWorld().getSystem(WatcherSystem.class) != null).forEach(is -> {
            EntityTracker et = is.getWorld().getManager(EntityTracker.class);
            if (et != null) {
                for (AbstractMap.SimpleEntry<Entity, WatchableEntity> e : et.getBothWithComponent(WatchableEntity.class)) {
                    if (e.getKey().getUuid().equals(uuid)) {
                        e.getValue().watcher.add(conn);
                        e.getValue().lastV.clear();
                    } else e.getValue().watcher.remove(conn);

                }
            }
        });
    }

    void handleAni(WebSocket conn, JSONObject o) {
        UUID uuid = UUID.fromString(o.get("uuid").toString());
        String c = (String) o.get("cmd");
        plugin.getScenarioManager().getScenarios().stream().filter(is -> is.isRunning() && is.getWorld().getSystem(WatcherSystem.class) != null).forEach(is -> {
            EntityTracker et = is.getWorld().getManager(EntityTracker.class);
            if (et != null)
                for (AbstractMap.SimpleEntry<Entity, AS_Animations> e : et.getBothWithComponent(AS_Animations.class)) {
                    if (!e.getKey().getUuid().equals(uuid))
                        continue;
                    if (c.equals("removeall"))
                        e.getValue().anis.clear();
                    else if (c.equals("reset")) {
                        AS_Animations.resetAnimation(e.getValue(), (String) o.get("animation"));
                    } else if (c.equals("add")) {
                        if (o.containsKey("force"))
                            if (o.containsKey("con"))
                                AS_Animations.addForceConAnimation(e.getValue(), (String) o.get("animation"));
                            else
                                AS_Animations.addForceAnimation(e.getValue(), (String) o.get("animation"));
                        else if (o.containsKey("con"))
                            AS_Animations.addConAnimation(e.getValue(), (String) o.get("animation"));
                        else
                            AS_Animations.addAnimation(e.getValue(), (String) o.get("animation"));
                    } else if (c.equals("remove"))
                        AS_Animations.stopAnimation(e.getValue(), (String) o.get("animation"));
                }

        });
    }


    void setPose(WebSocket conn, JSONObject o) {
        UUID uuid = UUID.fromString(o.get("uuid").toString());
        String part = (String) o.get("part");
        plugin.getScenarioManager().getScenarios().stream().filter(is -> is.isRunning() && is.getWorld().getSystem(WatcherSystem.class) != null).forEach(is -> {
            EntityTracker et = is.getWorld().getManager(EntityTracker.class);
            if (et != null) {
                for (AbstractMap.SimpleEntry<Entity, AS_Animatable> e : et.getBothWithComponent(AS_Animatable.class)) {
                    if (e.getKey().getUuid().equals(uuid))
                        if (e.getValue().objects.containsKey(part)) {
                            AniObject a = e.getValue().objects.get(part);
                            if (o.containsKey("x"))
                                a.setTarget(a.getTarget().setX(Double.valueOf(o.get("x").toString())));
                            if (o.containsKey("y"))
                                a.setTarget(a.getTarget().setY(Double.valueOf(o.get("y").toString())));
                            if (o.containsKey("z"))
                                a.setTarget(a.getTarget().setZ(Double.valueOf(o.get("z").toString())));

                            return;
                        }

                }
            }
        });
    }


    public static int getAvaiblePort(int start) {
        while (!available(start))
            start++;
        return start;
    }


    private static boolean available(int port) {
        if (port < 0 || port > 0xFFFF) {
            throw new IllegalArgumentException("Invalid start port: " + port);
        }

        ServerSocket ss = null;
        DatagramSocket ds = null;
        try {
            ss = new ServerSocket(port);
            ss.setReuseAddress(true);
            ds = new DatagramSocket(port);
            ds.setReuseAddress(true);
            return true;
        } catch (IOException ignored) {
        } finally {
            if (ds != null) {
                ds.close();
            }

            if (ss != null) {
                try {
                    ss.close();
                } catch (IOException e) {
                /* should not be thrown */
                }
            }
        }

        return false;
    }


}
