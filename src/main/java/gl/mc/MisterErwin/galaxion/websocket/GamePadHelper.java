package gl.mc.mistererwin.galaxion.websocket;

import com.artemis.Entity;
import gl.mc.mistererwin.galaxion.AIStuff;
import gl.mc.mistererwin.galaxion.components.ai.AI;
import gl.mc.mistererwin.galaxion.components.animation.AS_Animatable;
import gl.mc.mistererwin.galaxion.components.animation.AS_Animations;
import gl.mc.mistererwin.galaxion.components.animation.Animated;
import gl.mc.mistererwin.galaxion.components.animation.Animations;
import gl.mc.mistererwin.galaxion.components.character.AniPart;
import gl.mc.mistererwin.galaxion.components.character.Parted;
import gl.mc.mistererwin.galaxion.components.generic.Located;
import gl.mc.mistererwin.galaxion.components.web.WatchableEntity;
import gl.mc.mistererwin.galaxion.implementation.ai.goals.Goal_MoveToPosition;
import gl.mc.mistererwin.galaxion.implementation.animations.LoopMode;
import gl.mc.mistererwin.galaxion.manager.EntityTracker;
import gl.mc.mistererwin.galaxion.packetStuff.Title;
import gl.mc.mistererwin.galaxion.scenarios.IScenario;
import gl.mc.mistererwin.galaxion.utils.AWorldUtils;
import gl.mc.mistererwin.galaxion.utils.math.Quaternion;
import gl.mc.mistererwin.galaxion.utils.math.Vector3f;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.EulerAngle;
import org.bukkit.util.Vector;
import org.java_websocket.WebSocket;
import org.json.simple.JSONObject;

import java.util.*;

/**
 * Created by mistererwin on 18.06.2015.
 * In case you need it, ask me ;)
 */
public class GamePadHelper {

    private static final float step = 0.01745329251f;

    static Quaternion x = new Quaternion(new float[]{step, 0, 0});
    static Quaternion y = new Quaternion(new float[]{0, step, 0});
    static Quaternion z = new Quaternion(new float[]{0, 0, step});
    private static boolean doAnims=false;


    private enum GPMode {
        ENTITY_SELECTION,
        ENTITY_MOVING,
        POSE_EDITING,
        ANIMATION_SELECTION;

        public GPMode next() {
            return values()[(this.ordinal() + 1) % values().length];
        }
    }

    private static Map<Integer, Entity> watched = new LinkedHashMap<>();
    private static Map<Integer, GPMode> mode = new LinkedHashMap<>();
    private static Map<Integer, UUID> player = new LinkedHashMap<>();
    private static Title t = new Title("LOADING", "", 1, 2, 1);


    private static Map<Integer, ArmorStand> pos = new LinkedHashMap<>();
    private static Map<Integer, String> pose = new LinkedHashMap<>();


    public static void onConnect(WebSocket conn, JSONObject o, AIStuff plugin) {
        Player p = Bukkit.getPlayerExact((String) o.get("player"));
        int hash = conn.getRemoteSocketAddress().hashCode();
        player.put(hash, p.getUniqueId());
        t.setTitle("Gamepad Connected");
        t.setTitleColor(ChatColor.GREEN);
        t.send(p);
        startSelection(p, hash, plugin);
    }

    public static void onDisconnect(WebSocket conn, JSONObject o, AIStuff plugin) {
        int hash = conn.getRemoteSocketAddress().hashCode();
        Player p = Bukkit.getPlayer(player.get(hash));
        t.setTitle("Gamepad Disconnected");
        t.setTitleColor(ChatColor.RED);
        rT(p);
        endMode(p, hash, mode.get(hash), plugin);
        watched.remove(hash);
        mode.remove(hash);
        player.remove(hash);
        pos.remove(hash);
        pose.remove(hash);
    }

    private static void next(WebSocket conn, int hash, Player p, AIStuff plugin) {
        GPMode o = mode.get(hash);
        GPMode m = o.next();
        while (o != m && !nextTry(hash, m))
            m = m.next();
        if (m == o) {
            t.setTitle("Nothing possible");
            t.setTitleColor(ChatColor.RED);
            rT(p);
            return;
        }
        switch (m) {
            case ANIMATION_SELECTION:
                startAni(p, hash, plugin);
                break;
            case ENTITY_MOVING:
                startEntityMoving(p, hash, plugin);
                break;
            case POSE_EDITING:
                startPoseEditing(p, hash, plugin);
                break;
        }
    }

    private static boolean nextTry(int hash, GPMode next) {
        switch (next) {
            case ENTITY_MOVING:
                return (watched.get(hash).getComponent(AI.class) != null);
            case POSE_EDITING:
                return (watched.get(hash).getComponent(AS_Animatable.class) != null);
            case ANIMATION_SELECTION:
                return (watched.get(hash).getComponent(AS_Animations.class) != null);
            default:
                return false;

        }
    }

    private static int[] cpart = new int[]{};

    private static void rotate(JSONObject axes, JSONObject buttons, AIStuff plugin) {
        if (axes.isEmpty() && buttons.isEmpty()) return;
        IScenario s = plugin.getScenarioManager().getScenarios().get(0);
        EntityTracker et = s.getWorld().getManager(EntityTracker.class);

        List<Quaternion> list = new LinkedList<>();

        //AXES
        if (!axes.isEmpty()) {
            Object d = axes.get(Integer.toString(0));
            Double dd;

            if (cpart.length == 0)
                et.getWithComponents(Parted.class).forEach(ex -> list.add(ex.getComponent(Located.class).transform.getRotation()));
            else
                et.getComWithComponent(Parted.class).forEach(ex -> addToList(list, ex.rootPart, 0));

            if (d != null) {
                dd = getDL(d);
                if (dd > 0.2)
                    list.forEach(ex -> rotateBy(x, ex));
                else if (dd < -0.2)
                    list.forEach(ex -> rotateBy(x.inverse(), ex));
            }
            d = axes.get(Integer.toString(1));
            if (d != null) {
                dd = getDL(d);
                if (dd > 0.2)
                    list.forEach(ex -> rotateBy(y, ex));
                else if (dd < -0.2)
                    list.forEach(ex -> rotateBy(y.inverse(), ex));
            }
            d = axes.get(Integer.toString(2));
            if (d != null) {
                dd = getDL(d);
                if (dd > 0.2)
                    list.forEach(ex -> rotateBy(z, ex));
                else if (dd < -0.2)
                    list.forEach(ex -> rotateBy(z.inverse(), ex));
            }
        }
        if (isPressed(buttons, 0) && cpart.length > 0) //A-Userc
        {
            List<AniPart> partList = new LinkedList<>();
            et.getComWithComponent(Parted.class).forEach(ex -> addToPartList(partList, ex.rootPart, 0));
            partList.forEach(ex -> ex.userControlled = !ex.userControlled);
        }

        if (isPressed(buttons, 2)) //X-Reset
        {
            if (list.isEmpty())
                if (cpart.length == 0)
                    et.getWithComponents(Parted.class).forEach(ex -> list.add(ex.getComponent(Located.class).transform.getRotation()));
                else
                    et.getComWithComponent(Parted.class).forEach(ex -> addToList(list, ex.rootPart, 0));
            list.forEach(ex -> ex.set(0, 0, 0, 1));
        }


        if (isPressed(buttons, 14) && cpart.length > 0) { //D-pad left
            if (cpart[cpart.length - 1] == 0)
                t.setTitle("0 is 0").setTitleColor(ChatColor.RED).broadcast();
            else {
                cpart[cpart.length - 1]--;
                showCPart();
            }
        }
        if (isPressed(buttons, 15) && cpart.length > 0) { //D-pad right
            cpart[cpart.length - 1]++;
            showCPart();
        }

        if (isPressed(buttons, 12)) { //D-Pad top
            if (cpart.length == 0)
                t.setTitle("already @ root").setTitleColor(ChatColor.RED).broadcast();
            else {
                cpart = Arrays.copyOf(cpart, cpart.length - 1);
                showCPart();
            }
        }
        if (isPressed(buttons, 13)) { //D-Pad bottom
            cpart = Arrays.copyOf(cpart, cpart.length + 1);
            cpart[cpart.length - 1] = 0;
            showCPart();
        }

    }

    private static void showCPart() {
        StringBuilder sbStr = new StringBuilder();
        for (int i = 0, il = cpart.length; i < il; i++) {
            if (i > 0)
                sbStr.append("->");
            sbStr.append(String.valueOf(cpart[i]));
        }
        t.setTitle(sbStr.toString()).setTitleColor(ChatColor.AQUA).broadcast();
    }

    private static void addToList(List<Quaternion> returnList, AniPart part, int index) {
        if (index >= cpart.length) return;
//        System.out.println(cpart[index] + ">>" + part.name + ">>" + index + "/" + cpart.length + " ((" + ((part.children != null) ? part.children.length : "null"));
        if (cpart.length == index + 1)
            returnList.add(part.localTransform.getRotation());
        else if (part.children != null && ++index < cpart.length && cpart[index] < part.children.length)
            addToList(returnList, part.children[cpart[index]], index);

    }

    private static void addToPartList(List<AniPart> returnList, AniPart part, int index) {
        if (index >= cpart.length) return;
//        System.out.println(cpart[index] + ">>" + part.name + ">>" + index + "/" + cpart.length + " ((" + ((part.children != null) ? part.children.length : "null"));
        if (cpart.length == index + 1)
            returnList.add(part);
        else if (part.children != null && ++index < cpart.length && cpart[index] < part.children.length)
            addToPartList(returnList, part.children[cpart[index]], index);

    }


    public static void onInput(WebSocket conn, JSONObject o, AIStuff plugin) {
        Object type = o.get("type");
        if (type != null && type.equals("controllerDisconnect")) {
            onDisconnect(conn, o, plugin);
            return;
        } else if (type != null && type.equals("controllerConnect")) {
            onConnect(conn, o, plugin);
            return;
        }
        int hash = conn.getRemoteSocketAddress().hashCode();
        Player p = Bukkit.getPlayer(player.get(hash));

        JSONObject buttons = (JSONObject) o.get("button");
        JSONObject axes = (JSONObject) o.get("axes");

        if (isPressed(buttons,9)){
            doAnims=!doAnims;
            t.setTitle((doAnims)?"Animations":"Rotation").setTitleColor(ChatColor.BLUE);
            rT(p);
            return;
        }

        if (doAnims)
            doAnim(axes, buttons, p, plugin);
        else
            rotate(axes, buttons, plugin);

        if (true) return;
        if (!buttons.isEmpty())
            if (isPressed(buttons, 8)) { // GO back
                System.out.println("out");
                endMode(p, hash, mode.get(hash), plugin);
                startSelection(p, hash, plugin);
                return;
            } else if (mode.get(hash) != GPMode.ENTITY_SELECTION && isPressed(buttons, 9)) { //Next
                next(conn, hash, p, plugin);
                return;
            }
        switch (mode.get(hash)) {
            case ENTITY_SELECTION:
                Entity w = watched.get(hash);
                if (w == null) { //Maybe an entity was added
                    startSelection(p, hash, plugin);
                    return;
                }
                if (isPressed(buttons, 4) || isPressed(buttons, 6)) { //back
                    IScenario s = plugin.getScenarioManager().getScenarios().get(0);
//                    Entity[] es = s.getWorld().getManager(EntityTracker.class).getEntities();
                    EntityTracker et = s.getWorld().getManager(EntityTracker.class);
                    if (et.getEntity(0).equals(w))
                        watched.put(hash, et.getEntity(et.size() - 1));
                    else {
                        Entity t = et.getEntity(0);
                        for (int i = 1; i < et.size(); i++) {
                            if (et.getEntity(i).equals(w)) {
                                watched.put(hash, t);
                                break;
                            }
                            t = et.getEntity(i);//es[i];
                        }
                    }
                    t.setTitle("Selected:");
                    WatchableEntity e = watched.get(hash).getComponent(WatchableEntity.class);
                    t.setSubtitle(watched.get(hash).getUuid().toString() + ((e != null) ? (e.type) : ""));
                    rT(p);
                } else if (isPressed(buttons, 5) || isPressed(buttons, 7)) { //next
                    IScenario s = plugin.getScenarioManager().getScenarios().get(0);
//                    Entity[] es = s.getWorld().getManager(EntityTracker.class).getEntities();
                    EntityTracker et = s.getWorld().getManager(EntityTracker.class);

                    if (et.getEntity(et.size() - 1).equals(w))
                        watched.put(hash, et.getEntity(0));
                    else {
                        for (int i = 1; i < et.size(); i++) {
                            if (et.getEntity(i).equals(w)) {
                                watched.put(hash, et.getEntity(i + 1));
                                break;
                            }
                        }
                    }
                    t.setTitle("Selected:");
                    WatchableEntity e = watched.get(hash).getComponent(WatchableEntity.class);
                    t.setSubtitle(watched.get(hash).getUuid().toString() + ((e != null) ? (e.type) : ""));
                    rT(p);
                } else if (isPressed(buttons, 0) || isPressed(buttons, 9) || isPressed(buttons, 10) || isPressed(buttons, 11)) {
                    t.setTitle("Entity selected!");
                    WatchableEntity e = watched.get(hash).getComponent(WatchableEntity.class);
                    t.setSubtitle(watched.get(hash).getUuid().toString() + ((e != null) ? (e.type) : ""));
                    rT(p);
                    next(conn, hash, p, plugin);
                }
                break;
            case ENTITY_MOVING:
                if (isPressed(buttons, 10)) {
                    Entity e = watched.get(hash);
                    e.getComponent(AI.class).getGoal().addSubgoal(new Goal_MoveToPosition(e, pos.get(hash).getLocation().add(0, -1, 0)));
                    t.setSubtitle("Walking...");
                    rT(p);
                    return;
                }

                Vector v = new Vector(0, 0, 0);
                if (!axes.isEmpty()) {
                    Object d = axes.get(Integer.toString(0));
                    if (d != null)
                        v.setX(getDL(d));
                    d = axes.get(Integer.toString(1));
                    if (d != null)
                        v.setZ(getDL(d));
                }
                if (isPressed(buttons, 0))
                    v.setY(3.5);
                if (isPressed(buttons, 2))
                    v.multiply(2);
                if (isPressed(buttons, 1))
                    Bukkit.getScheduler().runTask(plugin, () -> pos.get(hash).teleport(AWorldUtils.getLocation(watched.get(hash))));

                if (v.lengthSquared() != 0) {
                    final Vector x = v;
                    Bukkit.getScheduler().runTask(plugin, () -> pos.get(hash).setVelocity(x.multiply(0.1)));
                }
                break;
            case POSE_EDITING:
                AS_Animatable a = watched.get(hash).getComponent(AS_Animatable.class);
                if (isPressed(buttons, 4) || isPressed(buttons, 6)) {
                    String c = pose.get(hash);
                    String prev = null;
                    Iterator<String> i = a.objects.keySet().iterator();
                    while (i.hasNext()) {
                        String x = i.next();
                        if (x.equals(c))
                            break;
                        prev = x;
                    }
                    if (prev == null) {
                        i = a.objects.keySet().iterator();
                        while (i.hasNext())
                            prev = i.next();
                    }

                    t.setTitle("Pose: " + prev);
                    rT(p);
                    pose.put(hash, prev);

                } else if (isPressed(buttons, 5) || isPressed(buttons, 7)) {
                    String past = pose.get(hash);
                    Iterator<String> i = a.objects.keySet().iterator();
                    while (i.hasNext()) {
                        if (i.next().equals(past)) {
                            past = (i.hasNext()) ? i.next() : a.objects.keySet().iterator().next();
                            break;
                        }
                    }
                    t.setTitle("Pose: " + past);
                    rT(p);
                    pose.put(hash, past);
                } else if (isPressed(buttons, 1)) {
                    watched.get(hash).getComponent(AS_Animatable.class).objects.get(pose.get(hash)).setTarget(new EulerAngle(0, 0, 0));
                }

                if (!axes.isEmpty()) {
                    EulerAngle e = watched.get(hash).getComponent(AS_Animatable.class).objects.get(pose.get(hash)).getTarget();
                    Object d = axes.get(Integer.toString(0));
                    if (d != null)
                        e = e.add(Math.PI * getDL(d) * 0.01, 0, 0);
                    axes.get(Integer.toString(1));
                    if (d != null)
                        e = e.add(0, Math.PI * getDL(d) * 0.01, 0);
                    axes.get(Integer.toString(2));
                    if (d != null)
                        e = e.add(0, 0, Math.PI * getDL(d) * 0.01);
                    watched.get(hash).getComponent(AS_Animatable.class).objects.get(pose.get(hash)).setTarget(e);
                }
                break;
        }
    }

    private static Vector3f speed = new Vector3f(0,0,0.1f);

    private static void doAnim(JSONObject axes, JSONObject buttons, Player player, AIStuff plugin) {
        IScenario s = plugin.getScenarioManager().getScenarios().get(0);
        EntityTracker et = s.getWorld().getManager(EntityTracker.class);

        if (!axes.isEmpty()){
            Object d = axes.get(Integer.toString(1));
            Double dd;

            if (d != null) {
                dd = getDL(d);
                if (dd > 0.2)
                    for (Located located:et.getComWithComponent(Located.class))
                        located.transform.getTranslation().addLocal(located.transform.getRotation().mult(speed));
                else if (dd < -0.2)
                    for (Located located:et.getComWithComponent(Located.class))
                        located.transform.getTranslation().subtractLocal(located.transform.getRotation().mult(speed));
            }

             d = axes.get(Integer.toString(0));
            if (d != null) {
                dd = getDL(d);
                if (dd > 0.2)
                    for (Located located:et.getComWithComponent(Located.class))
                        rotateBy(y,located.transform.getRotation());
                else if (dd < -0.2)
                    for (Located located:et.getComWithComponent(Located.class))
                        rotateBy(y.inverse(),located.transform.getRotation());
            }
        }
        if (isPressed(buttons,0))
            for (Located located:et.getComWithComponent(Located.class))
                located.transform.getTranslation().addLocal(0,1.4f,0);
        if (isPressed(buttons,6))
            for (Entity entity:et.getWithComponent(Animations.class)) {
                entity.getComponent(Animated.class).channels.get(0).setAnim("duck", entity.getComponent(Animations.class));
                entity.getComponent(Animated.class).channels.get(0).setLoopMode(LoopMode.DontLoop);
            }

    }

    private static void rotateBy(Quaternion quaternion, Located value) {
        quaternion.mult(value.transform.getRotation(), value.transform.getRotation());
    }

    private static void rotateBy(Quaternion quaternion, Quaternion quaternion2) {
        quaternion.mult(quaternion2, quaternion2);
    }

    private static double getDL(Object d) {
        return (d instanceof Double) ? (Double) d : (Long) d;
    }

    private static void rT(Player p) {
        t.send(p);
        t.setSubtitle("");
        t.setTitle("");
        t.setTitleColor(ChatColor.WHITE);
        t.setSubtitleColor(ChatColor.WHITE);
    }

    private static void endMode(Player p, int hash, GPMode old, AIStuff plugin) {
        if (old == null) return;
        if (old == GPMode.ENTITY_MOVING) {
            pos.get(hash).remove();
            pos.remove(hash);
        }
    }

    private static void startEntityMoving(Player p, int hash, AIStuff plugin) {
        Bukkit.getScheduler().runTask(plugin, new Runnable() {
            @Override
            public void run() {
                endMode(p, hash, mode.get(hash), plugin);
                mode.put(hash, GPMode.ENTITY_MOVING);

                Location loc = AWorldUtils.getLocation(watched.get(hash));
                ArmorStand as = (ArmorStand) loc.getWorld().spawnEntity(loc, EntityType.ARMOR_STAND);
                as.setHelmet(new ItemStack(Material.GOLD_BLOCK));
                as.setCustomName("Walk here");
                as.setCustomNameVisible(true);
                pos.put(hash, as);
                t.setTitle("Moving");
                t.setSubtitle("LStick Axe and Button");
                rT(p);
            }
        });
    }

    private static void startPoseEditing(Player p, int hash, AIStuff plugin) {
        endMode(p, hash, mode.get(hash), plugin);
        mode.put(hash, GPMode.POSE_EDITING);
        watched.get(hash).getComponent(AS_Animations.class).anis.clear();

        pose.put(hash, watched.get(hash).getComponent(AS_Animatable.class).objects.keySet().iterator().next());
        t.setSubtitle("L1/2 last <-[Analog] next R1/2");
        rT(p);
    }

    private static void startAni(Player p, int hash, AIStuff plugin) {
        endMode(p, hash, mode.get(hash), plugin);
        mode.put(hash, GPMode.ANIMATION_SELECTION);
        t.setSubtitle("Ani - soon");
        rT(p);
    }


    private static void startSelection(Player p, int hash, AIStuff plugin) {
        endMode(p, hash, mode.get(hash), plugin);
        mode.put(hash, GPMode.ENTITY_SELECTION);

        IScenario s = plugin.getScenarioManager().getScenarios().get(0);
        EntityTracker et = s.getWorld().getManager(EntityTracker.class);
        if (et.size() == 0) {
            t.setSubtitle("No entities to select!");
            rT(p);

            watched.put(hash, null);
            return;
        }
        t.setSubtitle("L1/2 last <-Start-> next R1/2");
        rT(p);

        watched.put(hash, et.getEntity(0));//es[0]);
    }

    private static boolean isPressed(JSONObject o, int button) {
        Object b = o.get(Integer.toString(button));
//        for (Object e : o.entrySet())
//            System.out.println(((Map.Entry<String,Object>)e).getKey() + "==>" + ((Map.Entry<String,Object>)e).getValue());

//        System.out.println(button + ":" + (b != null && (boolean) b) + "(" + o);
        return (b != null && (boolean) b);
    }


}
