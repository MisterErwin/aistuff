/**
 * Created by L�pges on 06.06.2015.
 */
var socket;
var port = 8080;
var last;
var uuid;
function onMessage(txt) {
    //console.log(txt);

    try {
        var obj = jQuery.parseJSON(txt);
    } catch (e) {
        console.log(txt);
        console.log(e);
        return;
    }

    if ("hide" in obj) {
        if (obj["hide"] == "listScen") {
            var select = $("#scenarioSelect");
            select.empty();

            $.each(obj["list"], function (name, b) {
                $("<option/>", {
                    value: b["name"],
                    text: (b["name"] + " (" + b["player"] + ")"),
                    style: {
                        'background-color': (b["running"]) ? "green" : "gray"
                    }
                }).appendTo(select);
            });

            select.attr('size', $('option', select).length);

            $(".breadcrumb li").slice(1).remove();
            $(".breadcrumb").append($("<li/>").append($("<a/>", {text: 'Scenarios'})).on("click", function () {
                socket.send('{"action":"scen","list":true}');
                //LOADING
            }));

            select.prop('disabled', false);
            $("[data-page='selectScen'] button").prop('disabled', false).on("click", function (e) {
                e.preventDefault();
                $(this).off("click");
                select.prop('disabled', '[data-page="selectScen"] button,select', true);

                socket.send('{"action":"scen","listE":"' + $("#scenarioSelect").val() + '"}');

            });
            showPage("selectScen");
        } else {
            if (obj["hide"] == "listEntities") {
                var select = $("#entitySelect");
                select.empty();

                $.each(obj["list"], function (name, b) {
                    $("<option/>", {
                        value: b["uuid"],
                        text: (b["type"] + " (" + b["uuid"] + ")")
                    }).appendTo(select);
                });
                select.prop('disabled', false);
                select.attr('size', $('option', select).length);


                $(".breadcrumb li").slice(2).remove();
                $(".breadcrumb").append($("<li/>").append($("<a/>", {text: 'Entities'})).on("click", function () {
                    socket.send('{"action":"scen","listE":"' + $("#scenarioSelect").val() + '"}');
                    //LOADING
                }));

                $("[data-page='selectEntity'] button").prop('disabled', false).on("click", function (e) {
                    e.preventDefault();
                    $(this).off("click");
                    select.prop('disabled', '[data-page="selectEntity"] button,select', true);
                    showPage("showEntityData");
                    uuid = $("#entitySelect").val();

                    socket.send('{"action":"watch","uuid":"' + uuid + '"}');
                    $(".breadcrumb li").slice(3).remove();
                    $(".breadcrumb").append($("<li/>").append($("<a/>", {text: 'Watcher'})));

                });
                showPage("selectEntity");
            }
        }
        return;
    }


    if ("hide" in obj) {
        console.log(obj);
        return;
    }
    if ($("#updateRealtimeData").prop("checked"))
        it("data", "data", obj, $("#content"), false);

    if ("components" in obj) {
        var e = $("#components").empty();


        $("button[needs-comp]").prop("disabled", true);


        $.each(obj["components"], function (i, o) {

            e.append($("<span/>", {class: 'badge badge-default', text: o}));
            //<span class="label label-default">Default</span>
            var n = $("button[needs-comp='" + o + "']").prop("disabled", false);
        });
    }

    last = obj;

    updatePath(obj);
    updatePose(obj);
    updateAni(obj);
}

function it(name, rn, obj, parent, panels) {
    if (name.length > 30)return;
    var o = $("[data-key='" + name + "']");

    if (o.length == 0) {
        if (panels) {


            o = $("<div/>", {
                'data-key': name,
                'class': 'panel-body'
            });
            $("<div/>", {'class': 'panel panel-default'}).append(
                $("<div/>", {
                    'class': 'panel-heading',
                    'text': rn
                })
            ).append(o).appendTo(parent);
        } else {
            o = $("<div/>", {
                'data-key': name
            });
            o.append($("<div/>", {class: 'rowtitle', text: rn + ":"}))
            o.appendTo(parent);
        }
    }
    if ($.isArray(obj))
        o.text(rn + ":" + obj);
    else if (typeof obj === 'object' && obj != null) {
        $.each(obj, function (n, ob) {
            it(name + "." + n, n, ob, o);
        });
    } else {
        if (panels)
            o.text(rn + ":" + obj);
        else
            o.html("<span class='rowtitle'>" + rn + "</span>:" + obj);

    }


}

var onceConnected = false;

function sub() {
    if ('WebSocket' in window) {
        socket = new WebSocket('ws://localhost:' + port);
        socket.onopen = function () {
            clearTimeout(closer);
            onceConnected = true;
            $(".progress-bar.progress-bar-striped.active").removeClass("progress-bar-warning").removeClass("progress-bar-info").addClass("progress-bar-success").css({'width': '75%'}).text("Fetching data");
            console.log('Connected to WebSocket.');
            socket.send('{"action":"scen","list":true}');

        };
        socket.onmessage = function (msg) {
            onMessage(msg.data);
        };
        socket.onerror = function (msg) {
            console.log('Sorry but there was an error.');
        };
        socket.onclose = function () {
            clearTimeout(closer);
            if (onceConnected) {
                alert("Server went ofline");
                $(".progress-bar.progress-bar-striped.active").
                    removeClass("progress-bar-info").addClass("progress-bar-danger").css({'width': '100%'}).text("Server closed!");
            } else {
                var o = $(".progress-bar.progress-bar-striped.active");
                var p = (o.width() < o.offsetParent().width() / 2) ? o.width() : 0;
                port = port + 1;
                $(".progress-bar.progress-bar-striped.active").
                    removeClass("progress-bar-info").addClass("progress-bar-warning").width(p + (o.offsetParent().width() / 50)).text("Retrying on next port (" + port + ")...");
                window.setTimeout(sub, 500);
            }
        };
        closer = setTimeout(function () {
            socket.close();
        }, 4000);

    } else {
        alert('Your browser does not support HTML5 WebSockets.');
    }
}

var closer;

$(function () {
    var hash = window.location.hash;
    if (hash.length > 0)
        port = parseInt(hash.substr(1))

    sub();

});

var pN = 0;

function nextPage() {
    var x = $(".page");
    if (pN++ >= x.length - 1)
        pN = 0;
    x.hide().removeClass("page-first");
    x.eq(pN).fadeIn();
}

function showPage(n) {
    var x = $(".page[data-page='" + n + "']");
    var i = $(".page");
    pN = 0;
    $(".page").each(function (ind) {
        if ($(this).attr("data-page") == n) {
            pN = ind;
            return false;
        }
    });
    i.hide().removeClass("page-first");
    x.fadeIn();
}


var drawPath = false;
var pathCanvasCTX;
var pathCanvasHeight = 0;
var pathCanvasWidth = 0;

function canvasButton() {
    drawPath = !drawPath;
    if (!drawPath) {
        $('#pathCollapse').collapse("hide");
    } else {
        $('#pathCollapse').collapse("show");
        pathCanvasHeight = $("#pathCanvas").height();
        pathCanvasWidth = $("#pathCanvas").width();
        setTimeout(function () {
            pathCanvasHeight = $("#pathCanvas").height();
            pathCanvasWidth = $("#pathCanvas").width();
        }, 500);
        pathCanvasCTX = $("#pathCanvas")[0].getContext("2d");
    }
}

var mult = 5;

function pathChangeMult(val) {
    mult = Math.max(1, Math.min(parseInt(val), 50));
}

function updatePath(obj) {
    if (drawPath && "path" in obj && "path" in obj["path"]) {
        var path = obj["path"]["path"];
        if (path == null) {
        } else {

            pathCanvasCTX.strokeStyle = '#000';
            pathCanvasCTX.clearRect(0, 0, pathCanvasWidth, pathCanvasHeight);
            pathCanvasCTX.beginPath();
            pathCanvasCTX.arc(pathCanvasWidth / 2, pathCanvasHeight / 2, 3, 0, 2 * Math.PI);
            pathCanvasCTX.stroke();
            var x = 0, z = 0;
            pathCanvasCTX.moveTo(pathCanvasWidth / 2 + x * mult, pathCanvasHeight / 2 + z * mult);

            $.each(path, function (i, p) {
                var c = p.split("|");
                pathCanvasCTX.beginPath();
                pathCanvasCTX.moveTo(pathCanvasWidth / 2 + x * mult, pathCanvasHeight / 2 + z * mult);
                x += parseInt(c[0]);
                z += parseInt(c[2]);

                pathCanvasCTX.lineTo(pathCanvasWidth / 2 + x * mult, pathCanvasHeight / 2 + z * mult);
                if (c[1] == 1)
                    pathCanvasCTX.strokeStyle = '#ff0000';
                else if (c[1] == -1)
                    pathCanvasCTX.strokeStyle = '#0000ff';

                pathCanvasCTX.stroke();
            })
        }
    }
}

var updatePoseB = undefined;
var currentPosePart;
var allParts = {};

function poseButton() {
    updatePoseB = !updatePoseB;
    if (!updatePoseB) {
        $('#poseCollapse').collapse("hide");
    } else {
        $('#poseCollapse').collapse("show");
    }
}


function changePose(key, val) {
    socket.send('{"action":"setPose", "uuid":"' + uuid + '", "part": "' + currentPosePart + '","' + key + '":' + (val * (Math.PI / 180)) + '}')
}
var PoseSliderX, PoseSliderY, PoseSliderZ;
$(function () {
    $("#posePart").on("input", function () {
        currentPosePart = $(this).val();
        $("#poseX").val(allParts[currentPosePart][0])
        $("#poseY").val(allParts[currentPosePart][1])
        $("#poseZ").val(allParts[currentPosePart][2])

    });
    $("#poseX").on("input", function () {
        changePose("x", Math.max(0, Math.min($(this).val(), 359)));
    });
    $("#poseY").on("input", function () {
        changePose("y", Math.max(0, Math.min($(this).val(), 359)));
    });
    $("#poseZ").on("input", function () {
        changePose("z", Math.max(0, Math.min($(this).val(), 359)));
    });
});
function updatePose(obj) {
    if ((updatePoseB || updatePoseB == undefined) && "ani" in obj) {
        if (updatePoseB == undefined)
            updatePoseB = false;
        $.each(obj["ani"], function (k, o) {
            if (!(k in allParts)) {
                if ($("#posePart option").length == 0)
                    currentPosePart = k;
                $("#posePart").append($("<option/>", {value: k, text: k}));
            } else if (k == currentPosePart) {
                //var target = o["target"];
                //PoseSliderX.setValue(target[0]);
                //PoseSliderY.setValue(target[1]);
                //PoseSliderZ.setValue(target[2]);
            }
            if ("target" in o)
                allParts[k] = o["target"];

        });

    }
}
var anis = {}

function aniButton() {
    $('#anisCollapse').collapse("toggle");

}
function updateAni(obj) {
    if (!("anis" in obj))return;
    var an = jQuery.extend(true, {}, anis);

    $.each(obj["anis"], function (name, o) {
        if (!(name in anis)) {
            $("#anisTable").append(
                $("<tr/>", {'data-anis-name': name})
                    .append($("<td/>", {text: name}))
                    .append($("<td/>", {'a-active': true, text: o['active']}))
                    .append($("<td/>", {'a-status': true, text: o['status']}))
                    .append($("<td/>")
                        .append($("<span/>", {
                            class: 'glyphicon glyphicon-remove',
                            'aria-hidden': "true",
                            title: 'Remove'
                        }))
                        .append($("<span/>", {
                            class: 'glyphicon glyphicon-hand-left',
                            'aria-hidden': "true",
                            title: 'Reset'
                        })
                    )
                )
            );
        } else {
            $("tr[data-anis-name='" + name + "']>td[a-active]").text(o['active']);
            $("tr[data-anis-name='" + name + "']>td[a-status]").text(o['status']);
        }
        anis[name] = true;
        delete an[name];
    });
    $.each(an, function (name, o) {
        $("tr[data-anis-name='" + name + "']").hide("slow", function () {
            $(this).remove()
        });
    });

}

function anisAdd(){
    var name = $("#anisName");
    if (name.val().length < 2)
        return false;
    var f = ($("#anisForce").prop("checked"))?',"force":"true"':"";
    var c = ($("#anisCon")  .prop("checked"))?',"con":"true"':"";
    socket.send('{"action":"ani","uuid":"' + uuid + '","cmd":"add","animation":"'.concat(name.val()).concat('"').concat(f).concat(c).concat('}'));
    name.val("");
    return false;
}

$(function () {
    $("#anisTable").on("click", "tr>td>span[title='Remove']", function () {
        socket.send('{"action":"ani","uuid":"' + uuid + '","cmd":"remove","animation":"' + $(this).parent().parent().attr("data-anis-name") + '" }');
    });
    $("#anisTable").on("click", "tr>td>span[title='Reset']", function () {
        socket.send('{"action":"ani","uuid":"' + uuid + '","cmd":"reset","animation":"' + $(this).parent().parent().attr("data-anis-name") + '" }');
    });
    var $input = $('#anisName');
    $input.typeahead({
        minLength: 0,
        source: ["idle", "jump", "preJump", "run", "walk"]
    });

});